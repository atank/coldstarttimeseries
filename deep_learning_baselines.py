from pylab import *
from IPython import embed
from time import time
import scipy.sparse
import scipy.sparse.linalg
import sys, os
import pandas as pd
import pickle

import tensorflow as tf

from generate_series import *
from util import *

""" WARNING: This code is messy... 

    Note: This code works with TensorFlow version 0.11:
    https://www.tensorflow.org/versions/r0.11/get_started/os_setup#using_pip

"""


def generate_single_cold_start_plot(Y_cold_start, phi_cold_start, tf_objects, pure_or_OOS, test_articles_indices, index_to_article, indices=None):

    # Grab TF objects
    sess, output, sparse_input_indices, sparse_input_values, y_true = tf_objects

    T, N = Y_cold_start.shape

    print "Articles: "
    for index, article in zip(indices, [index_to_article[test_articles_indices[i]] for i in indices]):
        print str(index) + '\t' + article

    for fig_num, i in enumerate(indices):
        article = index_to_article[test_articles_indices[i]]

        fig = figure(fig_num+1)
        fig.suptitle(article)

        # subplot(2, 1, 1)

        # Plot the true time series
        plot(Y_cold_start[:,i], color='magenta') # or red

        # Plot the neural network prediction
        row_inds, col_inds, sp_vals = scipy.sparse.find(phi_cold_start[:,[i]].T) 
        sp_ind = np.vstack((row_inds, col_inds)).T
        prediction = sess.run(output, feed_dict = {sparse_input_indices : sp_ind,
                                                                          sparse_input_values : sp_vals,
                                                                          y_true : Y_cold_start[:,[i]].T})
        prediction = prediction[0, :] # line above returns a (1, 365) matrix

        plot( prediction, linewidth=1.5, color='green', alpha=0.8 )
        # ylim([-1, 1])
        xlim([0, 365])
        locator_params(axis='y',nbins=3)
        locator_params(axis='x',nbins=5)


        # subplot(2, 1, 2)
        # plot( prediction, linewidth=1.25, color='green' )
        # xlim([0, 365])
        # locator_params(axis='y',nbins=3)
        # locator_params(axis='x',nbins=5)


        subplots_adjust(hspace=0.2)

        savefig('Images/Real Data Experiments/{0}_FFMF/'.format(pure_or_OOS) + article + '.pdf', bbox_inches='tight')

        close(fig)

    # show()

def generate_cold_start_plot(Y_cold_start, phi_cold_start, tf_objects, pure_or_OOS, test_articles_indices, index_to_article, indices=None):

    np.random.seed(int(time()))

    # Grab TF objects
    sess, output, sparse_input_indices, sparse_input_values, y_true = tf_objects

    # Select a random subset of the cold start time series to plot
    N = Y_cold_start.shape[1]
    if indices is None:
        indices = np.random.choice(N, 6, replace=False)

    print "Articles: "
    for index, article in zip(indices, [index_to_article[test_articles_indices[i]] for i in indices]):
        print str(index) + '\t' + article

    # Plot cold-start
    fig = figure(3)
    clf()
    fig.suptitle('Cold Start Projections: {0}_FFMF'.format(pure_or_OOS))

    subplot(211)
    for i in indices:
        plot(Y_cold_start[:,i])
    title('true cold-start Y')
    ylim([-1, 1])

    subplot(212)
    for i in indices:
        # Plot the neural network prediction
        # NEED TO MAKE phi_cold_start[:,i] AND Y_cold_start[:,i] A 2D ARRAY
        row_inds, col_inds, sp_vals = scipy.sparse.find(phi_cold_start[:,[i]].T) 
        sp_ind = np.vstack((row_inds, col_inds)).T
        prediction = sess.run(output, feed_dict = {sparse_input_indices : sp_ind,
                                                                          sparse_input_values : sp_vals,
                                                                          y_true : Y_cold_start[:,[i]].T})
        prediction = prediction[0, :] # line above returns a (1, 365) matrix
        if len(indices) == 1:
            subplot(211)
            plot( prediction, linewidth=1.25, color='green', alpha=0.8 )
            ylim([-1, 1])
            subplot(212)
            plot( prediction, linewidth=1.25, color='green' )
            title('Zoomed in: {0}'.format(index_to_article[test_articles_indices[i]]))    
        else:
            plot( prediction )
            title('Estimated cold-start Y')

    subplots_adjust(hspace=0.4)
    # savefig(directory + '/' + file_prefix + 'Cold_Start_Reconstruction.pdf', bbox_inches='tight')
    show()




def build_feedforward_multi_output_regression_network(T, m, k, N, NN_step_size, use_MF=True):

    ### ARCHITECTURE: 2 hidden layers, 1 output layer of dimension T

    #Build computation graph
    tf.reset_default_graph()
    tf.set_random_seed(1)

    # Input
    y_true = tf.placeholder(tf.float32, [None, T])
    current_minibatch_size = tf.shape(y_true, out_type=tf.int64)[0]

    sparse_input_indices = tf.placeholder(tf.int64, [None, 2]) # None is not for minibatch, but rather for unknown number of values in sparse minibatch
    sparse_input_values = tf.placeholder(tf.float32, [None])
    x = tf.SparseTensor(indices=sparse_input_indices, values=sparse_input_values, shape=[current_minibatch_size, m]) # WORKS!!!

    # Hidden Layer 1
    h1_dim = 100
    W_1 = tf.Variable( tf.truncated_normal([m, h1_dim], stddev=1.0 / sqrt(float(m))), name='W1' )
    b_1 = tf.Variable( tf.zeros([h1_dim]), name='b1' )
    h1 = tf.nn.relu( tf.sparse_tensor_dense_matmul(x, W_1) + b_1 )

    # Hidden Layer 2
    h2_dim = T
    W_2 = tf.Variable( tf.truncated_normal([h1_dim, h2_dim], stddev=1.0 / sqrt(float(h1_dim))), name='W2' )
    b_2 = tf.Variable( tf.zeros([h2_dim]), name='b2' )
    h2 = tf.nn.relu( tf.matmul(h1, W_2) + b_2 )

    # Output Layer
    output_dim = T
    W_output = tf.Variable( tf.truncated_normal([h2_dim, output_dim], stddev=1.0 / sqrt(float(h2_dim))), name='Wo' )
    b_output = tf.Variable( tf.zeros([output_dim]), name='bo' )
    output = tf.matmul(h2, W_output) + b_output

    # Matrix Factorization portion
    L = tf.Variable( tf.truncated_normal([k, T], stddev=1.0 / sqrt(float(T))), name='L' ) # transpose of what I'm used to
    R = tf.Variable( tf.truncated_normal([N, k], stddev=1.0 / sqrt(float(N))), name='R' ) # transpose of what I'm used to
    sgd_indices = tf.placeholder(tf.int64, [None])
    R_indices = tf.gather(R, sgd_indices) # tf.gather() gets rows
    mf_portion = tf.matmul(R_indices, L)

    # Loss
    curr_missing_data_mask = tf.placeholder(tf.float32, [None, T])
    loss_without_mf = 0.5 * tf.reduce_mean(tf.reduce_sum(tf.mul(tf.square(y_true - output), curr_missing_data_mask), 1))
    loss_with_mf = 0.5 * tf.reduce_mean(tf.reduce_sum(tf.mul(tf.square(y_true - output - mf_portion), curr_missing_data_mask), 1))
    if use_MF:
        train_step = tf.train.GradientDescentOptimizer(NN_step_size).minimize(loss_with_mf)
    else:
        train_step = tf.train.GradientDescentOptimizer(NN_step_size).minimize(loss_without_mf)

    return sparse_input_indices, sparse_input_values, sgd_indices, y_true, curr_missing_data_mask, train_step, loss_without_mf, loss_with_mf, output

def train_feedforward_multi_output_regression_network(num_epochs, minibatch_size, Y_train, phi_train, Y_cold_start, phi_cold_start, 
                                                      missing_data_mask, validation_missing_data_mask, validation_cold_start_mask, missing_data_experiments,
                                                      sparse_input_indices, sparse_input_values, sgd_indices, y_true, curr_missing_data_mask, train_step, loss_without_mf, loss_with_mf, use_MF=True,
                                                      save_model=False, model_name=None):

    # Grab some parameters
    N_train = Y_train.shape[1]
    num_iters_per_epoch = int(ceil(N_train / minibatch_size))

    obj_values = []
    validation_values = []

    start_time = time()
    with tf.Session() as sess:

        # Re-initialize all variables and re-run training
        tf.set_random_seed(1)
        init = tf.initialize_all_variables()
        sess.run(init)

        for epoch in range(num_epochs):
            
            print "Starting Epoch {0}...".format(epoch + 1)
            new_batch = True
            
            for iteration in range(num_iters_per_epoch):

                # Take care of minibatch chunking here
                if new_batch:
                    shuffle_order = np.random.permutation(N_train)
                    new_batch = False
                    chunk = minibatch_size
                    chunk_start = 0; chunk_end = minibatch_size
                indices = shuffle_order[chunk_start : chunk_end]
                chunk_start = chunk_end
                chunk_end += minibatch_size

                # Get minibatch into subphi in the shape of [minibatch_size, m]
                minibatch_Y = Y_train[:, indices].T
                minibatch_phi = phi_train[:, indices].T
                minibatch_missing_data_mask = missing_data_mask[:, indices].T

                # Grab feed dict values
                row_inds, col_inds, sp_vals = scipy.sparse.find(minibatch_phi) # gives you values w.r.t. the indices above
                sp_ind = np.vstack((row_inds, col_inds)).T # gives you a batch_size x 2 array of indices, needed for input to SparseTensor

                # Run a step of gradient descent
                if use_MF:
                    loss = loss_with_mf
                else:
                    loss = loss_without_mf
                _, obj_val = sess.run([train_step, loss], feed_dict = {sparse_input_indices : sp_ind,
                                                                        sparse_input_values : sp_vals,
                                                                        y_true : minibatch_Y,
                                                                        curr_missing_data_mask : minibatch_missing_data_mask,
                                                                        sgd_indices : indices})
                obj_values.append(obj_val)

            if missing_data_experiments:
                # Calculate validation loss as well
                row_inds, col_inds, sp_vals = scipy.sparse.find(phi_train.T) 
                sp_ind = np.vstack((row_inds, col_inds)).T
                test_cold_start = sess.run(loss_with_mf, feed_dict = {sparse_input_indices : sp_ind,
                                                              sparse_input_values : sp_vals,
                                                              y_true : Y_train.T,
                                                              curr_missing_data_mask : validation_missing_data_mask.T,
                                                              sgd_indices : np.arange(N_train)})
                validation_values.append(test_cold_start)
            else: # cold start experiments
                # Calculate validation loss as well
                row_inds, col_inds, sp_vals = scipy.sparse.find(phi_cold_start.T) 
                sp_ind = np.vstack((row_inds, col_inds)).T
                test_cold_start = sess.run(loss_without_mf, feed_dict = {sparse_input_indices : sp_ind,
                                                                                  sparse_input_values : sp_vals,
                                                                                  y_true : Y_cold_start.T,
                                                                                  curr_missing_data_mask : validation_cold_start_mask.T})
                validation_values.append(test_cold_start)

        # Save model
        if save_model:
            saver = tf.train.Saver()
            saver.save(sess, "opt_dumps/" + model_name)


    end_time = time()
    print "Time taken to train feedforward network: {0} seconds".format(round(end_time - start_time, 3))

    return obj_values, validation_values


def synthetic_data_FFMF_baseline_uniform_random_missing_data_experiments():

    ###### Generate the synthetic data #######

    # Seed the randomness
    np.random.seed(1)
    # np.random.seed(int(time()))

    T = 300                  # Length of each univariate time series
    N = 5000                  # Number of generated time series (some will be save for cold start forecasting)
    m = 1000                   # Length of metadata vector
    k = 20                   # True number of latent factors
    Y, phi, L, R, H, U = gen_series(T,N,m,k, model='mf+reg_GP')
    phi = scipy.sparse.csc_matrix(phi)

    # This is the test set
    test_Omega = gen_uniformly_missing_test_set(T, N, .2)
    K = 5 # 5-fold cross validation
    CV_folds = missing_data_CV_folds(T, N, K, test_Omega)

    # Parameters
    k = 20
    minibatch_size = 200
    missing_data_experiments = True
    NN_step_size = 0.05
    directory = 'test/synthetic/cross_validation/FFMF_baseline_uniformly_missing'
    use_MF = True

    # Build network
    sparse_input_indices, sparse_input_values, sgd_indices, y_true, curr_missing_data_mask, train_step, loss_without_mf, loss_with_mf, output = build_feedforward_multi_output_regression_network(T, m, k, N, NN_step_size, use_MF=use_MF)



    ####### TRAIN AND EVALUATE ON TEST SET ########

    # Generate missing data masks
    missing_data_mask, validation_missing_data_mask, validation_cold_start_mask, Omega_train, completely_missing_Omega_split = construct_missing_data_masks(Y, None, CV_folds[0], test_Omega)

    num_epochs_for_val = 1000
    # Train the feedforward network with training and validation sets
    print "Training and looking at VALIDATION error..."
    obj_values, test_missing_data_values = train_feedforward_multi_output_regression_network(num_epochs_for_val, minibatch_size, Y, phi, None, None, 
                                                                                             missing_data_mask, validation_missing_data_mask, None, missing_data_experiments,
                                                                                             sparse_input_indices, sparse_input_values, sgd_indices, y_true, curr_missing_data_mask, train_step, loss_without_mf, loss_with_mf, use_MF=use_MF)
    ##### Now learn matrix factorization #####

    # Print stuff
    print "{0}: %10.10f".format("Error on training data") % (obj_values[-1])
    true_model_train_error = test_missing_data_error([L, R, H, U, np.zeros(T)], Y, phi, missing_data_mask, 'mf+LowRankReg')
    print "{0}: %10.10f".format("Error on training data using true model") % (true_model_train_error)
    print "{0}: %10.10f".format("Error on test data") % (test_missing_data_values[-1])
    true_model_test_error = test_missing_data_error([L, R, H, U, np.zeros(T)], Y, phi, validation_missing_data_mask, 'mf+LowRankReg')
    print "{0}: %10.10f".format("Error on test data using true model") % (true_model_test_error)
    
    # Plot stuff
    semilogy(np.arange(1, len(obj_values) + 1) , obj_values)
    L1 = 'Objective'
    num_iters_per_epoch = int(ceil(N / minibatch_size))
    semilogy(np.arange(1, len(test_missing_data_values) + 1) * num_iters_per_epoch, test_missing_data_values)
    L2 = 'Validation Error'

    legend([L1, L2])
    xlabel('Iteration Number')
    title('NN Objective Values and Errors, SGD. Num Epochs: {0}'.format(num_epochs_for_val))

    if not os.path.exists(directory):
        os.makedirs(directory)
    savefig(directory + '/Train_Val_Convergence.pdf')




    ####### TRAIN AND EVALUATE ON TEST SET ########

    num_epochs_for_test = int(raw_input("How many epochs for training on test data? "))
    print "Running {0} epochs on test data...".format(num_epochs_for_test)
    
    # Generate missing data masks for training and test
    missing_data_mask, validation_missing_data_mask, validation_cold_start_mask, Omega_train, completely_missing_Omega = construct_missing_data_masks(Y, None, test_Omega[0], None)

    # Train the feedforward network with training and validation sets
    print "Training and looking at TEST error..."
    obj_values, test_missing_data_values = train_feedforward_multi_output_regression_network(num_epochs_for_test, minibatch_size, Y, phi, None, None, 
                                                                                             missing_data_mask, validation_missing_data_mask, None, missing_data_experiments,
                                                                                             sparse_input_indices, sparse_input_values, sgd_indices, y_true, curr_missing_data_mask, train_step, loss_without_mf, loss_with_mf, use_MF=use_MF,
                                                                                             save_model=True, model_name='Synth unif_FF' + ('MF' if use_MF else ''))
    ##### Now learn matrix factorization #####

    # Print stuff
    print "{0}: %10.10f".format("Error on training data") % (obj_values[-1])
    # true_model_train_error = test_missing_data_error([L, R, H, U, np.zeros(T)], Y, phi, missing_data_mask, 'mf+LowRankReg')
    # print "{0}: %10.10f".format("Error on training data using true model") % (true_model_train_error)
    print "{0}: %10.10f".format("Error on test data") % (test_missing_data_values[-1])
    true_model_test_error = test_missing_data_with_splines_error([L, R, U, np.zeros(T)], H, Y, phi, validation_missing_data_mask, 'mf+reg')
    print "{0}: %10.10f".format("Error on test data using true model") % (true_model_test_error)
    
    # Plot stuff
    clf();
    semilogy(np.arange(1, len(obj_values) + 1) , obj_values)
    L1 = 'Objective'
    num_iters_per_epoch = int(ceil(N / minibatch_size))
    semilogy(np.arange(1, len(test_missing_data_values) + 1) * num_iters_per_epoch, test_missing_data_values)
    L2 = 'Validation Error'

    legend([L1, L2])
    xlabel('Iteration Number')
    title('NN Objective Values and Errors, SGD. Num Epochs: {0}'.format(num_epochs_for_val))

    if not os.path.exists(directory):
        os.makedirs(directory)
    savefig(directory + '/Train_Test_Convergence.pdf')



def synthetic_data_FFMF_baseline_OOS_cold_start_experiments():

    ###### Generate the synthetic data #######

    # Seed the randomness
    np.random.seed(1)
    # np.random.seed(int(time()))

    T = 300                  # Length of each univariate time series
    num_phis = 1000
    K = 5 # Used for cross validation, and also for how many time series per phi
    m = 1000                   # Length of metadata vector
    k = 20                   # True number of latent factors
    Y_train, phi_train, Y_cold_start, phi_cold_start, CV_folds, L, R, H, U = gen_OOS_cold_start(T, num_phis, K, m, k, model='mf+reg_GP')
    phi_train = scipy.sparse.csc_matrix(phi_train)
    phi_cold_start = scipy.sparse.csc_matrix(phi_cold_start)

    # Generate a completely missing Omega, which the algorithm will never get to see
    N_train = Y_train.shape[1]
    N_cold_start = Y_cold_start.shape[1]
    completely_missing_Omega = gen_completely_missing_Omega(T, N_train, N_cold_start, 0.2)

    # Parameters
    k = 20
    minibatch_size = 200
    missing_data_experiments = False
    NN_step_size = 0.1
    directory = 'test/synthetic/cross_validation/FFMF_baseline_OOS_cold_start'
    use_MF = False



    ####### TRAIN AND EVALUATE ON TEST SET ########

    # Separate Y_train into a training set and validation set
    Y_train_split = np.delete(Y_train, CV_folds[0], axis=1)
    Y_val_split = Y_train[:, CV_folds[0]]
    N_train_split = Y_train_split.shape[1]
    N_val_split = Y_val_split.shape[1]

    all_cols = xrange(phi_train.shape[1])
    mask = np.in1d(all_cols, CV_folds[0], invert=True)
    phi_train_split = phi_train[:, mask]
    phi_val_split = phi_train[:, CV_folds[0]]

    completely_missing_Omega_train_split = np.delete(completely_missing_Omega[0], CV_folds[0], axis=0)
    completely_missing_Omega_val_split = np.array(completely_missing_Omega[0])[CV_folds[0]]
    completely_missing_Omega_split = [completely_missing_Omega_train_split, completely_missing_Omega_val_split]

    # Generate missing data masks
    missing_data_mask, validation_missing_data_mask, validation_cold_start_mask, Omega_train, completely_missing_Omega_split = construct_missing_data_masks(Y_train_split, Y_val_split, None, completely_missing_Omega_split)

    # Build network
    sparse_input_indices, sparse_input_values, sgd_indices, y_true, curr_missing_data_mask, train_step, loss_without_mf, loss_with_mf, output = build_feedforward_multi_output_regression_network(T, m, k, N_train_split, NN_step_size, use_MF=use_MF)

    num_epochs_for_val = 1000
    # Train the feedforward network with training and validation sets
    print "Training and looking at VALIDATION error..."
    obj_values, test_cold_start_values = train_feedforward_multi_output_regression_network(num_epochs_for_val, minibatch_size, Y_train_split, phi_train_split, Y_val_split, phi_val_split, 
                                                                                           missing_data_mask, None, validation_cold_start_mask, missing_data_experiments,
                                                                                           sparse_input_indices, sparse_input_values, sgd_indices, y_true, curr_missing_data_mask, train_step, loss_without_mf, loss_with_mf, use_MF=use_MF)

    # Print stuff
    print "{0}: %10.10f".format("Error on training data") % (obj_values[-1])
    # true_model_train_error = test_cold_start_error([np.zeros((T, k)), np.zeros((k, N_train_split)), H, U, np.zeros(T)], Y_train_split, phi_train_split, missing_data_mask, 'mf+LowRankReg')
    # true_model_train_error = .5 / N_train_split * pow(np.linalg.norm(missing_data_mask.T * ( Y_train_split.T -  (L.dot(np.delete(R[:, :num_phis*K], CV_folds[0], axis=1))).T - phi_train_split.T.dot(U.T).dot(H.T) ), 'fro'), 2)
    # print "{0}: %10.10f".format("Error on training data using true model") % (true_model_train_error)
    print "{0}: %10.10f".format("Error on test data") % (test_cold_start_values[-1])
    true_model_test_error = test_cold_start_error([L, R, H, U, np.zeros(T)], Y_val_split, phi_val_split, validation_cold_start_mask, 'mf+LowRankReg')
    print "{0}: %10.10f".format("Error on test data using true model") % (true_model_test_error)
    
    # Plot stuff
    semilogy(np.arange(1, len(obj_values) + 1) , obj_values)
    L1 = 'Objective'
    num_iters_per_epoch = int(ceil(N_train_split / minibatch_size))
    semilogy(np.arange(1, len(test_cold_start_values) + 1) * num_iters_per_epoch, test_cold_start_values)
    L2 = 'Validation Error'

    legend([L1, L2])
    xlabel('Iteration Number')
    title('NN Objective Values and Errors, SGD. Num Epochs: {0}'.format(num_epochs_for_val))

    if not os.path.exists(directory):
        os.makedirs(directory)
    savefig(directory + '/Train_Val_Convergence.pdf')



    ####### TRAIN AND EVALUATE ON TEST SET ########

    num_epochs_for_test = int(raw_input("How many epochs for training on test data? "))
    print "Running {0} epochs on test data...".format(num_epochs_for_test)
    
    # Generate missing data masks for training and test
    missing_data_mask, validation_missing_data_mask, validation_cold_start_mask, Omega_train, completely_missing_Omega_split = construct_missing_data_masks(Y_train, Y_cold_start, None, completely_missing_Omega)

    # Build network
    sparse_input_indices, sparse_input_values, sgd_indices, y_true, curr_missing_data_mask, train_step, loss_without_mf, loss_with_mf, output = build_feedforward_multi_output_regression_network(T, m, k, N_train, NN_step_size, use_MF=use_MF)

    # Train the feedforward network with training and validation sets
    print "Training and looking at TEST error..."
    obj_values, test_cold_start_values = train_feedforward_multi_output_regression_network(num_epochs_for_test, minibatch_size, Y_train, phi_train, Y_cold_start, phi_cold_start, 
                                                                                           missing_data_mask, None, validation_cold_start_mask, missing_data_experiments,
                                                                                           sparse_input_indices, sparse_input_values, sgd_indices, y_true, curr_missing_data_mask, train_step, loss_without_mf, loss_with_mf, use_MF=use_MF,
                                                                                           save_model=True, model_name='Synth OOS_FF' + ('MF' if use_MF else ''))

    # Print stuff
    print "{0}: %10.10f".format("Error on training data") % (obj_values[-1])
    # true_model_train_error = test_cold_start_error([np.zeros((T, k)), np.zeros((k, N_train)), H, U, np.zeros(T)], Y_train, phi_train, missing_data_mask, 'mf+LowRankReg')
    # true_model_train_error = .5 / N_train * pow(np.linalg.norm(missing_data_mask.T * ( Y_train.T -  (L.dot(R[:, :num_phis*K])).T - phi_train.T.dot(U.T).dot(H.T) ), 'fro'), 2)
    # print "{0}: %10.10f".format("Error on training data using true model") % (true_model_train_error)
    print "{0}: %10.10f".format("Error on test data") % (test_cold_start_values[-1])
    true_model_test_error = test_cold_start_with_splines_error([L, R[:, :num_phis*K], U, np.zeros(T)], H, Y_cold_start, phi_cold_start, validation_cold_start_mask, 'mf+reg')
    print "{0}: %10.10f".format("Error on test data using true model") % (true_model_test_error)
    
    # Plot stuff
    clf();
    semilogy(np.arange(1, len(obj_values) + 1) , obj_values)
    L1 = 'Objective'
    num_iters_per_epoch = int(ceil(N_train / minibatch_size))
    semilogy(np.arange(1, len(test_cold_start_values) + 1) * num_iters_per_epoch, test_cold_start_values)
    L2 = 'Cold Start Error'

    legend([L1, L2])
    xlabel('Iteration Number')
    title('NN Objective Values and Errors, SGD. Num Epochs: {0}'.format(num_epochs_for_test))

    if not os.path.exists(directory):
        os.makedirs(directory)
    savefig(directory + '/Train_Test_Convergence.pdf')


def synthetic_data_FFMF_baseline_pure_cold_start_experiments():

    ###### Generate the synthetic data #######

    # Seed the randomness
    np.random.seed(1)
    # np.random.seed(int(time()))

    T = 300                  # Length of each univariate time series
    N_train = 5000            # Number of generated time series (some will be save for cold start forecasting)
    N_cold_start = 1000
    m = 1000                   # Length of metadata vector
    k = 20                   # True number of latent factors
    Y_train, phi_train, Y_cold_start, phi_cold_start, L, R, H, U = gen_pure_cold_start(T, N_train, N_cold_start, m, k, model='mf+reg_GP')
    phi_train = scipy.sparse.csc_matrix(phi_train)
    phi_cold_start = scipy.sparse.csc_matrix(phi_cold_start)

    # Generate a completely missing Omega, which the algorithm will never get to see
    completely_missing_Omega = gen_completely_missing_Omega(T, N_train, N_cold_start, 0.2)
    K = 5 # 5-fold cross validation
    CV_folds = cold_start_CV_folds(T, N_train, K)

    # Parameters
    k = 20
    minibatch_size = 200
    missing_data_experiments = False
    NN_step_size = 0.05
    directory = 'test/synthetic/cross_validation/FFMF_baseline_pure_cold_start'
    use_MF = True




    ####### TRAIN AND EVALUATE ON TEST SET ########

    # Separate Y_train into a training set and validation set
    Y_train_split = np.delete(Y_train, CV_folds[0], axis=1)
    Y_val_split = Y_train[:, CV_folds[0]]
    N_train_split = Y_train_split.shape[1]
    N_val_split = Y_val_split.shape[1]

    all_cols = xrange(phi_train.shape[1])
    mask = np.in1d(all_cols, CV_folds[0], invert=True)
    phi_train_split = phi_train[:, mask]
    phi_val_split = phi_train[:, CV_folds[0]]

    completely_missing_Omega_train_split = np.delete(completely_missing_Omega[0], CV_folds[0], axis=0)
    completely_missing_Omega_val_split = np.array(completely_missing_Omega[0])[CV_folds[0]]
    completely_missing_Omega_split = [completely_missing_Omega_train_split, completely_missing_Omega_val_split]

    # Generate missing data masks
    missing_data_mask, validation_missing_data_mask, validation_cold_start_mask, Omega_train, completely_missing_Omega_split = construct_missing_data_masks(Y_train_split, Y_val_split, None, completely_missing_Omega_split)

    # Build network
    sparse_input_indices, sparse_input_values, sgd_indices, y_true, curr_missing_data_mask, train_step, loss_without_mf, loss_with_mf, output = build_feedforward_multi_output_regression_network(T, m, k, N_train_split, NN_step_size, use_MF=use_MF)

    num_epochs_for_val = 1000
    # Train the feedforward network with training and validation sets
    print "Training and looking at VALIDATION error..."
    obj_values, test_cold_start_values = train_feedforward_multi_output_regression_network(num_epochs_for_val, minibatch_size, Y_train_split, phi_train_split, Y_val_split, phi_val_split, 
                                                                                           missing_data_mask, None, validation_cold_start_mask, missing_data_experiments,
                                                                                           sparse_input_indices, sparse_input_values, sgd_indices, y_true, curr_missing_data_mask, train_step, loss_without_mf, loss_with_mf, use_MF=use_MF)

    # Print stuff
    print "{0}: %10.10f".format("Error on training data") % (obj_values[-1])
    # true_model_train_error = test_cold_start_error([np.zeros((T, k)), np.zeros((k, N_train_split)), H, U, np.zeros(T)], Y_train_split, phi_train_split, missing_data_mask, 'mf+LowRankReg')
    # true_model_train_error = .5 / N_train_split * pow(np.linalg.norm(missing_data_mask.T * ( Y_train_split.T -  (L.dot(np.delete(R[:, :N_train], CV_folds[0], axis=1))).T - phi_train_split.T.dot(U.T).dot(H.T) ), 'fro'), 2)
    # print "{0}: %10.10f".format("Error on training data using true model") % (true_model_train_error)
    print "{0}: %10.10f".format("Error on test data") % (test_cold_start_values[-1])
    # true_model_test_error = test_cold_start_error([L, R, H, U, np.zeros(T)], Y_val_split, phi_val_split, validation_cold_start_mask, 'mf+LowRankReg')
    # print "{0}: %10.10f".format("Error on test data using true model") % (true_model_test_error)
    
    # Plot stuff
    semilogy(np.arange(1, len(obj_values) + 1) , obj_values)
    L1 = 'Objective'
    num_iters_per_epoch = int(ceil(N_train_split / minibatch_size))
    semilogy(np.arange(1, len(test_cold_start_values) + 1) * num_iters_per_epoch, test_cold_start_values)
    L2 = 'Validation Error'

    legend([L1, L2])
    xlabel('Iteration Number')
    title('NN Objective Values and Errors, SGD. Num Epochs: {0}'.format(num_epochs_for_val))

    if not os.path.exists(directory):
        os.makedirs(directory)
    savefig(directory + '/Train_Val_Convergence.pdf')



    ####### TRAIN AND EVALUATE ON TEST SET ########

    num_epochs_for_test = int(raw_input("How many epochs for training on test data? "))
    print "Running {0} epochs on test data...".format(num_epochs_for_test)
    
    # Generate missing data masks for training and test
    missing_data_mask, validation_missing_data_mask, validation_cold_start_mask, Omega_train, completely_missing_Omega_split = construct_missing_data_masks(Y_train, Y_cold_start, None, completely_missing_Omega)

    # Build network
    sparse_input_indices, sparse_input_values, sgd_indices, y_true, curr_missing_data_mask, train_step, loss_without_mf, loss_with_mf, output = build_feedforward_multi_output_regression_network(T, m, k, N_train, NN_step_size, use_MF=use_MF)

    # Train the feedforward network with training and validation sets
    print "Training and looking at TEST error..."
    obj_values, test_cold_start_values = train_feedforward_multi_output_regression_network(num_epochs_for_test, minibatch_size, Y_train, phi_train, Y_cold_start, phi_cold_start, 
                                                                                           missing_data_mask, None, validation_cold_start_mask, missing_data_experiments,
                                                                                           sparse_input_indices, sparse_input_values, sgd_indices, y_true, curr_missing_data_mask, train_step, loss_without_mf, loss_with_mf, use_MF=use_MF,
                                                                                           save_model=True, model_name='Synth Pure_FF' + ('MF' if use_MF else ''))

    # Print stuff
    print "{0}: %10.10f".format("Error on training data") % (obj_values[-1])
    # true_model_train_error = test_cold_start_error([np.zeros((T, k)), np.zeros((k, N_train)), H, U, np.zeros(T)], Y_train, phi_train, missing_data_mask, 'mf+LowRankReg')
    # true_model_train_error = .5 / N_train * pow(np.linalg.norm(missing_data_mask.T * ( Y_train.T -  (L.dot(R[:, :N_train])).T - phi_train.T.dot(U.T).dot(H.T) ), 'fro'), 2)
    # print "{0}: %10.10f".format("Error on training data using true model") % (true_model_train_error)
    print "{0}: %10.10f".format("Error on test data") % (test_cold_start_values[-1])
    true_model_test_error = test_cold_start_with_splines_error([L, R[:, :N_train], U, np.zeros(T)], H, Y_cold_start, phi_cold_start, validation_cold_start_mask, 'mf+reg')
    print "{0}: %10.10f".format("Error on test data using true model") % (true_model_test_error)
    
    # Plot stuff
    clf();
    semilogy(np.arange(1, len(obj_values) + 1) , obj_values)
    L1 = 'Objective'
    num_iters_per_epoch = int(ceil(N_train / minibatch_size))
    semilogy(np.arange(1, len(test_cold_start_values) + 1) * num_iters_per_epoch, test_cold_start_values)
    L2 = 'Cold Start Error'

    legend([L1, L2])
    xlabel('Iteration Number')
    title('NN Objective Values and Errors, SGD. Num Epochs: {0}'.format(num_epochs_for_test))

    if not os.path.exists(directory):
        os.makedirs(directory)
    savefig(directory + '/Train_Test_Convergence.pdf')




def real_data_FFMF_large_chunk_missing_experiments():

    # Seed the randomness
    np.random.seed(1)
    # np.random.seed(int(time()))

    # Load the data
    print "Loading in data..."
    args = sys.argv[1:]
    Y_dataframe = pd.read_csv(args[0], index_col=0) # Y_dataframe is a pandas dataframe
    Y = Y_dataframe.values
    phi = pickle.load(open(args[1], 'rb')) # phi is a scipy.sparse matrix
    T, N = Y.shape
    print "Loaded in data!"

    # This is the test set
    completely_missing_Omega = gen_large_missing_chunks_test_set(T, N, .5)
    K = 5 # 5-fold cross validation
    CV_folds = missing_data_CV_folds(T, N, K, completely_missing_Omega, shuffle=False)

    # Parameters
    k = 5
    m = phi.shape[0]
    minibatch_size = 300
    missing_data_experiments = True
    NN_step_size = 1e-2
    directory = 'test/real_data/FFMF_large_chunk'
    use_MF = False



    # Build network
    sparse_input_indices, sparse_input_values, sgd_indices, y_true, curr_missing_data_mask, train_step, loss_without_mf, loss_with_mf, output = build_feedforward_multi_output_regression_network(T, m, k, N, NN_step_size, use_MF=use_MF)


    ####### TRAIN AND EVALUATE ON TEST SET ########

    # Generate missing data masks
    missing_data_mask, validation_missing_data_mask, validation_cold_start_mask, Omega_train, completely_missing_Omega_split = construct_missing_data_masks(Y, None, CV_folds[0], completely_missing_Omega)

    num_epochs_for_val = 20000
    # Train the feedforward network with training and validation sets
    print "Training and looking at VALIDATION error..."
    obj_values, test_missing_data_values = train_feedforward_multi_output_regression_network(num_epochs_for_val, minibatch_size, Y, phi, None, None, 
                                                                                             missing_data_mask, validation_missing_data_mask, None, missing_data_experiments,
                                                                                             sparse_input_indices, sparse_input_values, sgd_indices, y_true, curr_missing_data_mask, train_step, loss_without_mf, loss_with_mf, use_MF=use_MF)

    # Print stuff
    print "{0}: %10.10f".format("Error on training data") % (obj_values[-1])
    print "{0}: %10.10f".format("Error on test data") % (test_missing_data_values[-1])
    
    # Plot stuff
    semilogy(np.arange(1, len(obj_values) + 1) , obj_values)
    L1 = 'Objective'
    num_iters_per_epoch = int(ceil(N / minibatch_size))
    semilogy(np.arange(1, len(test_missing_data_values) + 1) * num_iters_per_epoch, test_missing_data_values)
    L2 = 'Validation Error'

    legend([L1, L2])
    xlabel('Iteration Number')
    title('NN Objective Values and Errors, SGD. Num Epochs: {0}'.format(num_epochs_for_val))

    if not os.path.exists(directory):
        os.makedirs(directory)
    savefig(directory + '/Train_Val_Convergence.png')




    ####### TRAIN AND EVALUATE ON TEST SET ########

    num_epochs_for_test = int(raw_input("How many epochs for training on test data? "))
    print "Running {0} epochs on test data...".format(num_epochs_for_test)
    
    # Generate missing data masks for training and test
    missing_data_mask, validation_missing_data_mask, validation_cold_start_mask, Omega_train, completely_missing_Omega = construct_missing_data_masks(Y, None, completely_missing_Omega[0], None)

    # Train the feedforward network with training and validation sets
    print "Training and looking at TEST error..."
    obj_values, test_missing_data_values = train_feedforward_multi_output_regression_network(num_epochs_for_test, minibatch_size, Y, phi, None, None, 
                                                                                             missing_data_mask, validation_missing_data_mask, None, missing_data_experiments,
                                                                                             sparse_input_indices, sparse_input_values, sgd_indices, y_true, curr_missing_data_mask, train_step, loss_without_mf, loss_with_mf, use_MF=use_MF,
                                                                                             save_model=True, model_name='unif_FF' + ('MF' if use_MF else ''))

    # Print stuff
    print "{0}: %10.10f".format("Error on training data") % (obj_values[-1])
    print "{0}: %10.10f".format("Error on test data") % (test_missing_data_values[-1])
    
    # Plot stuff
    clf();
    semilogy(np.arange(1, len(obj_values) + 1) , obj_values)
    L1 = 'Objective'
    num_iters_per_epoch = int(ceil(N / minibatch_size))
    semilogy(np.arange(1, len(test_missing_data_values) + 1) * num_iters_per_epoch, test_missing_data_values)
    L2 = 'Validation Error'

    legend([L1, L2])
    xlabel('Iteration Number')
    title('NN Objective Values and Errors, SGD. Num Epochs: {0}'.format(num_epochs_for_val))

    if not os.path.exists(directory):
        os.makedirs(directory)
    savefig(directory + '/Train_Test_Convergence.png')

    embed()




def real_data_FFMF_warm_start_missing_experiments():

    # Seed the randomness
    np.random.seed(1)
    # np.random.seed(int(time()))

    # Load the data
    print "Loading in data..."
    args = sys.argv[1:]
    Y_dataframe = pd.read_csv(args[0], index_col=0) # Y_dataframe is a pandas dataframe
    Y = Y_dataframe.values
    phi = pickle.load(open(args[1], 'rb')) # phi is a scipy.sparse matrix
    T, N = Y.shape
    print "Loaded in data!"

    # This is the test set
    K = 5 # 5-fold cross validation
    # Y_train, phi_train, Y_cold_start, phi_cold_start, CV_folds, train_articles_indices, test_articles_indices, index_to_article = segment_articles_into_OOS_CV_folds(Y_dataframe, phi, K)
    Y_train, phi_train, Y_cold_start, phi_cold_start, train_articles_indices, test_articles_indices, index_to_article = segment_articles_into_pure_cold_start_train_test(Y_dataframe, phi)
    N_train = Y_train.shape[1]; N_cold_start = Y_cold_start.shape[1]
    Y = np.concatenate((Y_train, Y_cold_start), axis=1)
    phi = scipy.sparse.hstack((phi_train, phi_cold_start))
    T, N = Y.shape
    completely_missing_Omega, CV_folds = gen_warm_start_test_set(T, N_train, N_cold_start, K, num_present=8)

    # Parameters
    k = 20
    m = phi.shape[0]
    minibatch_size = 500
    missing_data_experiments = True
    NN_step_size = 1e-2
    directory = 'test/real_data/FFMF_warm_start'
    use_MF = False



    # Build network
    sparse_input_indices, sparse_input_values, sgd_indices, y_true, curr_missing_data_mask, train_step, loss_without_mf, loss_with_mf, output = build_feedforward_multi_output_regression_network(T, m, k, N, NN_step_size, use_MF=use_MF)


    ####### TRAIN AND EVALUATE ON TEST SET ########

    # Generate missing data masks
    missing_data_mask, validation_missing_data_mask, validation_cold_start_mask, Omega_train, completely_missing_Omega_split = construct_missing_data_masks(Y_train, None, CV_folds[0], None)

    num_epochs_for_val = 1000
    # Train the feedforward network with training and validation sets
    print "Training and looking at VALIDATION error..."
    obj_values, test_missing_data_values = train_feedforward_multi_output_regression_network(num_epochs_for_val, minibatch_size, Y_train, phi_train, None, None, 
                                                                                             missing_data_mask, validation_missing_data_mask, None, missing_data_experiments,
                                                                                             sparse_input_indices, sparse_input_values, sgd_indices, y_true, curr_missing_data_mask, train_step, loss_without_mf, loss_with_mf, use_MF=use_MF)

    # Print stuff
    print "{0}: %10.10f".format("Error on training data") % (obj_values[-1])
    print "{0}: %10.10f".format("Error on test data") % (test_missing_data_values[-1])
    
    # Plot stuff
    semilogy(np.arange(1, len(obj_values) + 1) , obj_values)
    L1 = 'Objective'
    num_iters_per_epoch = int(ceil(N / minibatch_size))
    semilogy(np.arange(1, len(test_missing_data_values) + 1) * num_iters_per_epoch, test_missing_data_values)
    L2 = 'Validation Error'

    legend([L1, L2])
    xlabel('Iteration Number')
    title('NN Objective Values and Errors, SGD. Num Epochs: {0}'.format(num_epochs_for_val))

    if not os.path.exists(directory):
        os.makedirs(directory)
    savefig(directory + '/Train_Val_Convergence.png')




    ####### TRAIN AND EVALUATE ON TEST SET ########

    num_epochs_for_test = int(raw_input("How many epochs for training on test data? "))
    print "Running {0} epochs on test data...".format(num_epochs_for_test)
    
    # Generate missing data masks for training and test
    missing_data_mask, validation_missing_data_mask, validation_cold_start_mask, Omega_train, completely_missing_Omega = construct_missing_data_masks(Y, None, completely_missing_Omega[0], None)

    # Train the feedforward network with training and validation sets
    print "Training and looking at TEST error..."
    obj_values, test_missing_data_values = train_feedforward_multi_output_regression_network(num_epochs_for_test, minibatch_size, Y, phi, None, None, 
                                                                                             missing_data_mask, validation_missing_data_mask, None, missing_data_experiments,
                                                                                             sparse_input_indices, sparse_input_values, sgd_indices, y_true, curr_missing_data_mask, train_step, loss_without_mf, loss_with_mf, use_MF=use_MF,
                                                                                             save_model=True, model_name='WS_FF' + ('MF' if use_MF else ''))

    # Print stuff
    print "{0}: %10.10f".format("Error on training data") % (obj_values[-1])
    print "{0}: %10.10f".format("Error on test data") % (test_missing_data_values[-1])
    
    # Plot stuff
    clf();
    semilogy(np.arange(1, len(obj_values) + 1) , obj_values)
    L1 = 'Objective'
    num_iters_per_epoch = int(ceil(N / minibatch_size))
    semilogy(np.arange(1, len(test_missing_data_values) + 1) * num_iters_per_epoch, test_missing_data_values)
    L2 = 'Validation Error'

    legend([L1, L2])
    xlabel('Iteration Number')
    title('NN Objective Values and Errors, SGD. Num Epochs: {0}'.format(num_epochs_for_val))

    if not os.path.exists(directory):
        os.makedirs(directory)
    savefig(directory + '/Train_Test_Convergence.png')

    embed()




def real_data_FFMF_OOS_cold_start_experiments():

    ###### Generate the synthetic data #######

    # Seed the randomness
    np.random.seed(1)
    # np.random.seed(int(time()))

    # Load the data
    print "Loading in data..."
    args = sys.argv[1:]
    Y_dataframe = pd.read_csv(args[0], index_col=0) # Y_dataframe is a pandas dataframe
    phi = pickle.load(open(args[1], 'rb')) # phi is a scipy.sparse matrix
    print "Loaded in data!"

    # print "Removing spikes..."
    # spike_completely_missing_Omega, Y_dataframe = remove_spikes_and_rescale(Y_dataframe)
    # print "Removed spikes!"

    K = 5 # 5-fold Cross Validation
    Y_train, phi_train, Y_cold_start, phi_cold_start, CV_folds, train_articles_indices, test_articles_indices, index_to_article = segment_articles_into_OOS_CV_folds(Y_dataframe, phi, K)

    # Generate a completely missing Omega, which the algorithm will never get to see
    T, N_train = Y_train.shape
    N_cold_start = Y_cold_start.shape[1]
    completely_missing_Omega = gen_completely_missing_Omega(T, N_train, N_cold_start, 0.2)
    # Combine completely_missing_Omega with spike missing
    # permuted_spike_completely_missing_Omega = (np.array(spike_completely_missing_Omega)[train_articles_indices], np.array(spike_completely_missing_Omega)[test_articles_indices])
    # completely_missing_Omega = combine_CMOs(completely_missing_Omega, permuted_spike_completely_missing_Omega)
    
    # Parameters
    k = 20
    m = phi_train.shape[0]
    minibatch_size = 500
    missing_data_experiments = False
    NN_step_size = 0.1
    directory = 'test/real_data/FFMF_OOS_cold_start'
    use_MF = True




    ####### TRAIN AND EVALUATE ON TEST SET ########

    # Separate Y_train into a training set and validation set
    Y_train_split = np.delete(Y_train, CV_folds[0], axis=1)
    Y_val_split = Y_train[:, CV_folds[0]]
    N_train_split = Y_train_split.shape[1]
    N_val_split = Y_val_split.shape[1]

    all_cols = xrange(phi_train.shape[1])
    mask = np.in1d(all_cols, CV_folds[0], invert=True)
    phi_train_split = phi_train[:, mask]
    phi_val_split = phi_train[:, CV_folds[0]]

    completely_missing_Omega_train_split = np.delete(completely_missing_Omega[0], CV_folds[0], axis=0)
    completely_missing_Omega_val_split = np.array(completely_missing_Omega[0])[CV_folds[0]]
    completely_missing_Omega_split = [completely_missing_Omega_train_split, completely_missing_Omega_val_split]

    # Generate missing data masks
    missing_data_mask, validation_missing_data_mask, validation_cold_start_mask, Omega_train, completely_missing_Omega_split = construct_missing_data_masks(Y_train_split, Y_val_split, None, completely_missing_Omega_split)

    # Build network
    sparse_input_indices, sparse_input_values, sgd_indices, y_true, curr_missing_data_mask, train_step, loss_without_mf, loss_with_mf, output = build_feedforward_multi_output_regression_network(T, m, k, N_train_split, NN_step_size, use_MF=use_MF)

    num_epochs_for_val = 1000
    # Train the feedforward network with training and validation sets
    print "Training and looking at VALIDATION error..."
    obj_values, test_cold_start_values = train_feedforward_multi_output_regression_network(num_epochs_for_val, minibatch_size, Y_train_split, phi_train_split, Y_val_split, phi_val_split, 
                                                                                           missing_data_mask, None, validation_cold_start_mask, missing_data_experiments,
                                                                                           sparse_input_indices, sparse_input_values, sgd_indices, y_true, curr_missing_data_mask, train_step, loss_without_mf, loss_with_mf, use_MF=use_MF)

    # Print stuff
    print "{0}: %10.10f".format("Error on training data") % (obj_values[-1])
    print "{0}: %10.10f".format("Error on test data") % (test_cold_start_values[-1])
    
    # Plot stuff
    semilogy(np.arange(1, len(obj_values) + 1) , obj_values)
    L1 = 'Objective'
    num_iters_per_epoch = int(ceil(N_train_split / minibatch_size))
    semilogy(np.arange(1, len(test_cold_start_values) + 1) * num_iters_per_epoch, test_cold_start_values)
    L2 = 'Validation Error'

    legend([L1, L2])
    xlabel('Iteration Number')
    title('NN Objective Values and Errors, SGD. Num Epochs: {0}'.format(num_epochs_for_val))

    if not os.path.exists(directory):
        os.makedirs(directory)
    savefig(directory + '/Train_Val_Convergence.png')



    ####### TRAIN AND EVALUATE ON TEST SET ########

    num_epochs_for_test = int(raw_input("How many epochs for training on test data? "))
    print "Running {0} epochs on test data...".format(num_epochs_for_test)
    
    # Generate missing data masks for training and test
    missing_data_mask, validation_missing_data_mask, validation_cold_start_mask, Omega_train, completely_missing_Omega_split = construct_missing_data_masks(Y_train, Y_cold_start, None, completely_missing_Omega)

    # Build network
    sparse_input_indices, sparse_input_values, sgd_indices, y_true, curr_missing_data_mask, train_step, loss_without_mf, loss_with_mf, output = build_feedforward_multi_output_regression_network(T, m, k, N_train, NN_step_size, use_MF=use_MF)

    # Train the feedforward network with training and validation sets
    print "Training and looking at TEST error..."
    obj_values, test_cold_start_values = train_feedforward_multi_output_regression_network(num_epochs_for_test, minibatch_size, Y_train, phi_train, Y_cold_start, phi_cold_start, 
                                                                                           missing_data_mask, None, validation_cold_start_mask, missing_data_experiments,
                                                                                           sparse_input_indices, sparse_input_values, sgd_indices, y_true, curr_missing_data_mask, train_step, loss_without_mf, loss_with_mf, use_MF=use_MF,
                                                                                           save_model=True, model_name='OOS_FF' + ('MF' if use_MF else ''))

    # Print stuff
    print "{0}: %10.10f".format("Error on training data") % (obj_values[-1])
    print "{0}: %10.10f".format("Error on test data") % (test_cold_start_values[-1])
    
    # Plot stuff
    clf();
    semilogy(np.arange(1, len(obj_values) + 1) , obj_values)
    L1 = 'Objective'
    num_iters_per_epoch = int(ceil(N_train / minibatch_size))
    semilogy(np.arange(1, len(test_cold_start_values) + 1) * num_iters_per_epoch, test_cold_start_values)
    L2 = 'Cold Start Error'

    legend([L1, L2])
    xlabel('Iteration Number')
    title('NN Objective Values and Errors, SGD. Num Epochs: {0}'.format(num_epochs_for_test))

    if not os.path.exists(directory):
        os.makedirs(directory)
    savefig(directory + '/Train_Test_Convergence.png')

    embed()




def real_data_FFMF_pure_cold_start_experiments():

    ###### Generate the synthetic data #######

    # Seed the randomness
    np.random.seed(1)
    # np.random.seed(int(time()))

    # Load the data
    print "Loading in data..."
    args = sys.argv[1:]
    Y_dataframe = pd.read_csv(args[0], index_col=0) # Y_dataframe is a pandas dataframe
    phi = pickle.load(open(args[1], 'rb')) # phi is a scipy.sparse matrix
    print "Loaded in data!"

    # print "Removing spikes..."
    # spike_completely_missing_Omega, Y_dataframe = remove_spikes_and_rescale(Y_dataframe)
    # print "Removed spikes!"

    K = 5 # 5-fold Cross Validation
    Y_train, phi_train, Y_cold_start, phi_cold_start, train_articles_indices, test_articles_indices, index_to_article = segment_articles_into_pure_cold_start_train_test(Y_dataframe, phi)

    # Generate a completely missing Omega, which the algorithm will never get to see
    T, N_train = Y_train.shape
    N_cold_start = Y_cold_start.shape[1]
    completely_missing_Omega = gen_completely_missing_Omega(T, N_train, N_cold_start, 0.2)
    # Combine completely_missing_Omega with spike missing
    # permuted_spike_completely_missing_Omega = (np.array(spike_completely_missing_Omega)[train_articles_indices], np.array(spike_completely_missing_Omega)[test_articles_indices])
    # completely_missing_Omega = combine_CMOs(completely_missing_Omega, permuted_spike_completely_missing_Omega)

    # Create CV folds
    CV_folds = cold_start_CV_folds(T, N_train, K)

    # Parameters
    k = 20
    m = phi_train.shape[0]
    minibatch_size = 500
    missing_data_experiments = False
    NN_step_size = 0.1
    directory = 'test/real_data/FFMF_pure_cold_start'
    use_MF = True




    ####### TRAIN AND EVALUATE ON TEST SET ########

    # Separate Y_train into a training set and validation set
    Y_train_split = np.delete(Y_train, CV_folds[0], axis=1)
    Y_val_split = Y_train[:, CV_folds[0]]
    N_train_split = Y_train_split.shape[1]
    N_val_split = Y_val_split.shape[1]

    all_cols = xrange(phi_train.shape[1])
    mask = np.in1d(all_cols, CV_folds[0], invert=True)
    phi_train_split = phi_train[:, mask]
    phi_val_split = phi_train[:, CV_folds[0]]

    completely_missing_Omega_train_split = np.delete(completely_missing_Omega[0], CV_folds[0], axis=0)
    completely_missing_Omega_val_split = np.array(completely_missing_Omega[0])[CV_folds[0]]
    completely_missing_Omega_split = [completely_missing_Omega_train_split, completely_missing_Omega_val_split]

    # Generate missing data masks
    missing_data_mask, validation_missing_data_mask, validation_cold_start_mask, Omega_train, completely_missing_Omega_split = construct_missing_data_masks(Y_train_split, Y_val_split, None, completely_missing_Omega_split)

    # Build network
    sparse_input_indices, sparse_input_values, sgd_indices, y_true, curr_missing_data_mask, train_step, loss_without_mf, loss_with_mf, output = build_feedforward_multi_output_regression_network(T, m, k, N_train_split, NN_step_size, use_MF=use_MF)

    num_epochs_for_val = 1000
    # Train the feedforward network with training and validation sets
    print "Training and looking at VALIDATION error..."
    obj_values, test_cold_start_values = train_feedforward_multi_output_regression_network(num_epochs_for_val, minibatch_size, Y_train_split, phi_train_split, Y_val_split, phi_val_split, 
                                                                                           missing_data_mask, None, validation_cold_start_mask, missing_data_experiments,
                                                                                           sparse_input_indices, sparse_input_values, sgd_indices, y_true, curr_missing_data_mask, train_step, loss_without_mf, loss_with_mf, use_MF=use_MF)

    # Print stuff
    print "{0}: %10.10f".format("Error on training data") % (obj_values[-1])
    print "{0}: %10.10f".format("Error on test data") % (test_cold_start_values[-1])
    
    # Plot stuff
    semilogy(np.arange(1, len(obj_values) + 1) , obj_values)
    L1 = 'Objective'
    num_iters_per_epoch = int(ceil(N_train_split / minibatch_size))
    semilogy(np.arange(1, len(test_cold_start_values) + 1) * num_iters_per_epoch, test_cold_start_values)
    L2 = 'Validation Error'

    legend([L1, L2])
    xlabel('Iteration Number')
    title('NN Objective Values and Errors, SGD. Num Epochs: {0}'.format(num_epochs_for_val))

    if not os.path.exists(directory):
        os.makedirs(directory)
    savefig(directory + '/Train_Val_Convergence.png')



    ####### TRAIN AND EVALUATE ON TEST SET ########

    num_epochs_for_test = int(raw_input("How many epochs for training on test data? "))
    print "Running {0} epochs on test data...".format(num_epochs_for_test)
    
    # Generate missing data masks for training and test
    missing_data_mask, validation_missing_data_mask, validation_cold_start_mask, Omega_train, completely_missing_Omega_split = construct_missing_data_masks(Y_train, Y_cold_start, None, completely_missing_Omega)

    # Build network
    sparse_input_indices, sparse_input_values, sgd_indices, y_true, curr_missing_data_mask, train_step, loss_without_mf, loss_with_mf, output = build_feedforward_multi_output_regression_network(T, m, k, N_train, NN_step_size, use_MF=use_MF)

    # Train the feedforward network with training and validation sets
    print "Training and looking at TEST error..."
    obj_values, test_cold_start_values = train_feedforward_multi_output_regression_network(num_epochs_for_test, minibatch_size, Y_train, phi_train, Y_cold_start, phi_cold_start, 
                                                                                           missing_data_mask, None, validation_cold_start_mask, missing_data_experiments,
                                                                                           sparse_input_indices, sparse_input_values, sgd_indices, y_true, curr_missing_data_mask, train_step, loss_without_mf, loss_with_mf, use_MF=use_MF,
                                                                                           save_model=True, model_name='Pure_FF' + ('MF' if use_MF else ''))

    # Print stuff
    print "{0}: %10.10f".format("Error on training data") % (obj_values[-1])
    print "{0}: %10.10f".format("Error on test data") % (test_cold_start_values[-1])
    
    # Plot stuff
    clf();
    semilogy(np.arange(1, len(obj_values) + 1) , obj_values)
    L1 = 'Objective'
    num_iters_per_epoch = int(ceil(N_train / minibatch_size))
    semilogy(np.arange(1, len(test_cold_start_values) + 1) * num_iters_per_epoch, test_cold_start_values)
    L2 = 'Cold Start Error'

    legend([L1, L2])
    xlabel('Iteration Number')
    title('NN Objective Values and Errors, SGD. Num Epochs: {0}'.format(num_epochs_for_test))

    if not os.path.exists(directory):
        os.makedirs(directory)
    savefig(directory + '/Train_Test_Convergence.png')

    embed()


# How to run:
# $:~ python deep_learning_baselines.py <time_series_dataframe> <pickled_metadata_file> <model_name>
def plot_model_fits():

    # Seed the randomness
    np.random.seed(1)
    # np.random.seed(int(time()))

    # Load the data
    print "Loading in data..."
    args = sys.argv[1:]
    Y_dataframe = pd.read_csv(args[0], index_col=0) # Y_dataframe is a pandas dataframe
    phi = pickle.load(open(args[1], 'rb')) # phi is a scipy.sparse matrix
    print "Loaded in data!"

    # print "Removing spikes..."
    # spike_completely_missing_Omega, Y_dataframe = remove_spikes_and_rescale(Y_dataframe)
    # print "Removed spikes!"

    OOS = False # Use this to flip between Pure or OOS

    if OOS:
        K = 5
        Y_train, phi_train, Y_cold_start, phi_cold_start, CV_folds, train_articles_indices, test_articles_indices, index_to_article = segment_articles_into_OOS_CV_folds(Y_dataframe, phi, K)
        pure_or_OOS = "OOS"
    else:
        Y_train, phi_train, Y_cold_start, phi_cold_start, train_articles_indices, test_articles_indices, index_to_article = segment_articles_into_pure_cold_start_train_test(Y_dataframe, phi)
        pure_or_OOS = "Pure"

    # Parameters
    k = 20
    m = phi_train.shape[0]
    T, N_train = Y_train.shape
    NN_step_size = 0.1 # this doesn't matter
    use_MF = True # this doesn't matter either

    # Build network
    sparse_input_indices, sparse_input_values, sgd_indices, y_true, curr_missing_data_mask, train_step, loss_without_mf, loss_with_mf, output = build_feedforward_multi_output_regression_network(T, m, k, N_train, NN_step_size, use_MF=use_MF)

    # Open session, get a saver
    sess = tf.Session()
    saver = tf.train.Saver()
    saver.restore(sess, args[2])

    # Plot
    tf_objects = [sess, output, sparse_input_indices, sparse_input_values, y_true]
    if OOS:
        indices = [3662, 2213, 2729, 834, 1124, 2738, 2906, 3152, 443, 304, 250, 1274, 2947, 517, 3897, 2617, 689, 1737, 1084, 2427, 2893, 2665, 3377, 2876, 3406, 3757, 3475, 3211, 1956, 2610, 3244] # OOS
    else:
        indices = [317, 623, 746, 911, 910, 382, 338, 531, 887, 0, 48, 138, 238, 290, 85, 270, 420, 361, 271, 79, 660, 468, 187] # Pure
    # generate_cold_start_plot(Y_cold_start, phi_cold_start, tf_objects, pure_or_OOS, test_articles_indices, index_to_article, indices=None)
    generate_single_cold_start_plot(Y_cold_start, phi_cold_start, tf_objects, pure_or_OOS, test_articles_indices, index_to_article, indices=indices)

    embed()



if __name__ == '__main__':

    # synthetic_data_FFMF_baseline_uniform_random_missing_data_experiments()
    # synthetic_data_FFMF_baseline_OOS_cold_start_experiments()
    # synthetic_data_FFMF_baseline_pure_cold_start_experiments()
    # real_data_FFMF_large_chunk_missing_experiments()
    real_data_FFMF_warm_start_missing_experiments()
    # real_data_FFMF_OOS_cold_start_experiments()
    # real_data_FFMF_pure_cold_start_experiments()
    # plot_model_fits()

