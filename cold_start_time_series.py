from pylab import *
from IPython import embed
from time import time
import scipy.sparse
import scipy.sparse.linalg
import sys, os
import pandas as pd
import pickle

from gradients import *
from util import *

# Profiling:
# python -m cProfile -o mycode.prof experiments.py
# pyprof2calltree -k -i mycode.prof


# Possible models:
possible_models = {'reg', 'mf+reg', 'LowRankReg', 'mf+LowRankReg'}

# Global variables
obj_val_freq = 10
reg_step_size = 0.01

class Dump_Object(object):

    def __init__(self, opt_vars, obj_values, test_cold_start_values, test_missing_data_values):
        self.opt_vars = opt_vars
        self.obj_values = obj_values
        self.test_cold_start_values = test_cold_start_values
        self.test_missing_data_values = test_missing_data_values


def cold_start_learning(Y_train, phi_train, k_in, model, Y_cold_start=None, phi_cold_start=None, Omega_train=None, completely_missing_Omega=None, lambda1=1, lambda2=1,
                        initial_opt_vars=None, max_iters=100000, SGD=False, delay=None, minibatch_size=1, eps=1e-5, verbose=True, process_num=0, pick_up_where_we_left_off=False):
    """
    Gradient descent on the cold start time series model. This model is based on matrix factorization
    techniques and cold-start recommendation techniques.

    Parameters
    -----------
    Y_train : 2D numpy array
        T x N matrix of de-trended time series. Each univariate time series is length T, and there are N of them.

    phi_train : 2D numpy array
        m x N matrix of meta data. For each of the N univariate time series, there are m entries of metadata.

    k_in : int
        integer that determines the dimensionality of the latent factors

    model : string
        string that determines which model we are using
    
    Y_cold_start : 2D numpy array
        matrix of de-trended time series for testing. When cross validating, this is the validation set

    phi_cold_start : scipy.sparse.csc_matrix
        matrix of metadata for testing

    Omega_train : list of numpy arrays
        The length of this list is N. The i'th array in this list contains the missing data indices for time series i

    completely_missing_Omega : 2 lists of numpy arrays
        completely_missing_Omega[0] is used for Y_train
        completely_missing_Omega[1] is used for Y_cold_start
        Data that is completely missing. Used during cross validation. These values won't be counted for in the test error calculations (test set won't be touched)

    lambda1 : float
        Value of lambda_1

    lambda2 : float
        Value of lambda_2

    initial_opt_vars : list of 2D numpy arrays
        Initialization of optimization variables

    max_iters : int
        Maximum number of iterations of gradient descent

    SGD : bool
        Whether or not to use Stochastic Gradient Descent

    delay : list
        Details about SGD (e.g. fixed step size, decaying, etc)

    minibatch_size : int
        Minibatch size. Useful only if SGD = True

    eps : float
        Convergence criterion

    verbose : bool
        Bool that determines how much output the method produces

    process_num : int
        For debugging purposes

    pick_up_where_we_left_off : bool
        Used if training is interrupted


    Returns
    --------
    opt_vars : list of 2D numpy arrays
        Optimization variables

    obj_values : list of floats
        List of objective values

    test_cold_start_values : list of floats
        List of MSE values on cold start test set

    test_missing_data_values : list of floats
        List of MSE values on missing data test set
    """

    if model not in possible_models:
        print "ERROR: model {0} is not in possible models".format(model)
        print "Possible models:"
        print '\n'.join(possible_models)
        sys.exit(1)

    # Grab/Set parameters
    T, N = Y_train.shape
    m = phi_train.shape[0]
    k = k_in # Number of latent factors


    # Write intermediate results to a file
    write_freq = 1000 # Every _ iterations, write to file




    ##################### INITIALIZE OPTIMIZATION VARIABLES #####################

    largest_singular_val_phi_train = scipy.sparse.linalg.svds(phi_train, 1, return_singular_vectors=False)[0]
    if model == 'reg': # Order of opt_vars: [W]
        
        if initial_opt_vars is None:
            initial_W = .05 * np.random.normal(0, 1, size=(T,m))
            initial_b = .05 * np.random.normal(0, 1, size=T)
            initial_opt_vars = [initial_W, initial_b] # Initialize W, b
        lambda_W = lambda1
        reg_params = [lambda_W]

        # Find largest eigenvalue and set step size
        global reg_step_size
        reg_step_size = N / float(pow(largest_singular_val_phi_train, 2) + lambda_W)
        # print "Step size for regression: {0}".format(reg_step_size)

    elif model == 'mf+reg': # Order of opt_vars: [L, R, W]
        
        if initial_opt_vars is None:
            initial_L = .05 * np.random.normal(0, 1, size=(T,k))
            initial_R = .05 * np.random.normal(0, 1, size=(k,N))
            initial_W = .05 * np.random.normal(0, 1, size=(T,m))
            initial_b = .05 * np.random.normal(0, 1, size=T)
            initial_opt_vars = [initial_L, initial_R, initial_W, initial_b] # Initialize L, R, W, b
        lambda_L = lambda2
        lambda_R = lambda2
        lambda_W = lambda1
        reg_params = [lambda_L, lambda_R, lambda_W]
        
    elif model == 'LowRankReg': # Order of opt_vars: [H, U]

        if initial_opt_vars is None:
            initial_H = .05 * np.random.normal(0, 1, size=(T,k))
            initial_U = .05 * np.random.normal(0, 1, size=(k,m))
            initial_b = .05 * np.random.normal(0, 1, size=T)
            initial_opt_vars = [initial_H, initial_U, initial_b]
        lambda_H = lambda1
        lambda_U = lambda1
        reg_params = [lambda_H, lambda_U]

    elif model == 'mf+LowRankReg': # Order of opt_vars: [L, R, H, U]
        
        if initial_opt_vars is None:
            initial_L = .05 * np.random.normal(0, 1, size=(T,k))
            initial_R = .05 * np.random.normal(0, 1, size=(k,N))
            initial_H = .05 * np.random.normal(0, 1, size=(T,k))
            initial_U = .05 * np.random.normal(0, 1, size=(k,m))
            initial_b = .05 * np.random.normal(0, 1, size=T)
            initial_opt_vars = [initial_L, initial_R, initial_H, initial_U, initial_b]
        lambda_L = lambda2
        lambda_R = lambda2
        lambda_H = lambda1
        lambda_U = lambda1
        reg_params = [lambda_L, lambda_R, lambda_H, lambda_U]

    opt_vars = initial_opt_vars

    ##################### END: INITIALIZE OPTIMIZATION VARIABLES #####################




    # Gradient containers for speed ups
    gradient_containers = [item.copy() for item in initial_opt_vars]
    if SGD and model in ['mf+reg', 'mf+LowRankReg']:
        gradient_containers[1] = np.empty((k, minibatch_size))



    # Handle missing values and create missing data mask. Also handle cross validation missing data vs. test set missing data
    no_train_missing_values = Omega_train is None
    if no_train_missing_values:
        Omega_train = [np.array([], dtype=int64) for i in xrange(N)]

    if completely_missing_Omega is None:
        completely_missing_Omega = [0, 0]
        completely_missing_Omega[0] = [np.array([]) for i in xrange(N)]
        if Y_cold_start is not None:
            completely_missing_Omega[1] = [np.array([]) for i in xrange(Y_cold_start.shape[1])]

    # Masks for Y_train
    missing_data_mask = np.ones(Y_train.shape) # 0's where data is missing (used for objective calculation)
    validation_missing_data_mask = np.zeros(Y_train.shape) # 1's where validation missing data is (used for test_missing_data_error)
    for i in xrange(N):
        if len(Omega_train[i]) > 0:
            missing_data_mask[Omega_train[i], i] = 0
            validation_missing_data_mask[Omega_train[i], i] = 1
        if len(completely_missing_Omega[0][i]) > 0: # This data is part of the test set
            missing_data_mask[completely_missing_Omega[0][i], i] = 0 # Validation error won't touch this data
            Omega_train[i] = np.concatenate((Omega_train[i], completely_missing_Omega[0][i])) # Gradients won't touch this data. We are assuming Omega_train and completely_missing_Omega are disjoint

    # Masks for Y_cold_start
    validation_cold_start_mask = None
    if Y_cold_start is not None:
        validation_cold_start_mask = np.ones(Y_cold_start.shape) # 1's where validation cold data is (used for test_cold_start_error)
        for i in xrange(Y_cold_start.shape[1]):
            if len(completely_missing_Omega[1][i]) > 0:
                validation_cold_start_mask[completely_missing_Omega[1][i], i] = 0 # Won't be counted in the cold start error (as if data is actually missing)


    # Diagnostic stuff
    # H_gradient_norms = []
    # U_gradient_norms = []
    # L_gradient_norms = []
    # R_gradient_norms = []

    # H_norms = []
    # U_norms = []
    # L_norms = []
    # R_norms = []

    # diff_HU = []
    # diff_LR = []


    ##################### MAIN LOOP #####################

    old_obj = np.inf
    obj = full_objective(opt_vars, reg_params, Y_train, phi_train, missing_data_mask, model)
    obj_values = []

    test_missing_data_values = []
    test_cold_start_values = []
    t = 1
    new_batch = True

    if verbose:
        print "Objective: {0}".format(obj)

    if pick_up_where_we_left_off:
        do = pickle.load(open('opt_dumps/lambda1 {0}, lambda2 {1}, process_num {2}.pkl'.format(lambda1, lambda2, process_num), 'rb'))
        opt_vars = do.opt_vars
        obj_values = do.obj_values
        test_cold_start_values = do.test_cold_start_values
        test_missing_data_values = do.test_missing_data_values
        t = len(obj_values) * obj_val_freq

    while abs(old_obj - obj) >= eps and t <= max_iters:

        if verbose:
            print "Starting iteration {0}".format(t)
        
        # Take care of chunking here
        if new_batch:
            shuffle_order = np.random.permutation(N)
            new_batch = False
            chunk = minibatch_size
            chunk_start = 0; chunk_end = minibatch_size
        indices = shuffle_order[chunk_start : chunk_end]
        chunk_start = chunk_end
        chunk_end += minibatch_size
        if chunk_start >= N:
            new_batch = True

        # Take a stochastic gradient step
        if model == 'reg':
            
            # Extract variables
            W, b = extract_variables(opt_vars, model)
            lambda_W = extract_reg_parameters(reg_params, model)

            # Compute gradients and take a step
            if SGD:                
                # reg_stochastic_gradient_lazy(opt_vars, reg_params, Y_train, phi_train, model, indices, Omega_train, gradient_containers, t)
                compute_model_stochastic_gradients(opt_vars, reg_params, Y_train, phi_train, model, indices, Omega_train, gradient_containers)

                noisy_gradient_W = gradient_containers[0]
                noisy_gradient_b = gradient_containers[1]

                np.multiply( -step_size(t, model, SGD, delay), noisy_gradient_W, out=noisy_gradient_W); np.add(W, noisy_gradient_W, out=W)
                np.multiply( -step_size(t, model, SGD, delay), noisy_gradient_b, out=noisy_gradient_b); np.add(b, noisy_gradient_b, out=b)

            else:
                compute_model_gradients(opt_vars, reg_params, Y_train, phi_train, model, Omega_train, gradient_containers)

                gradient_W = gradient_containers[0]
                b = gradient_containers[1]

                np.multiply( -reg_step_size, gradient_W, out=gradient_W); np.add(W, gradient_W, out=W)

            # Put update back into opt_vars object
            opt_vars = [W, b]

        elif model == 'mf+reg':

            # Extract variables            
            L, R, W, b = extract_variables(opt_vars, model)
            lambda_L, lambda_R, lambda_W = extract_reg_parameters(reg_params, model)

            # Compute gradients and take a step
            if not SGD:# and t <= 100:
                # compute_model_gradients(opt_vars, reg_params, Y_train, phi_train, model, Omega_train, gradient_containers)

                # gradient_L = gradient_containers[0]
                # gradient_R = gradient_containers[1]
                # gradient_W = gradient_containers[2]
                # b = gradient_containers[3]

                # np.multiply( -step_size(t, model, SGD, delay), gradient_L, out=gradient_L); np.add(L, gradient_L, out=L)
                # np.multiply( -step_size(t, model, SGD, delay), gradient_R, out=gradient_R); np.add(R, gradient_R, out=R)
                # np.multiply( -step_size(t, model, SGD, delay), gradient_W, out=gradient_W); np.add(W, gradient_W, out=W)

                # ALTERNATING GRADIENT DESCENT
                # Joint step in W, L
                compute_reg_gradients_W_L(opt_vars, reg_params, Y_train, phi_train, model, Omega_train, gradient_containers)
                gradient_W = gradient_containers[2]
                gradient_L = gradient_containers[0]

                largest_singular_val_PR = scipy.sparse.linalg.svds(scipy.sparse.vstack([phi_train, R]), 1, return_singular_vectors=False)[0]
                best_step_size = N / float(pow(largest_singular_val_PR,2) + max(lambda_W, lambda_L))
                # if verbose:
                #     print 'Step size for W, L: {0}'.format(best_step_size)
                np.multiply( -best_step_size, gradient_L, out=gradient_L); np.add(L, gradient_L, out=L)
                np.multiply( -best_step_size, gradient_W, out=gradient_W); np.add(W, gradient_W, out=W)

                # Joint step in R, b
                compute_reg_gradients_R_b(opt_vars, reg_params, Y_train, phi_train, model, Omega_train, gradient_containers)
                gradient_R = gradient_containers[1]
                b = gradient_containers[3]

                largest_singular_val_L = np.linalg.svd(L, full_matrices=0, compute_uv=0)[0]
                best_step_size = N / float(pow(largest_singular_val_L,2) + lambda_R)
                # if verbose:
                #     print 'Step size for R: {0}'.format(best_step_size)
                np.multiply( -best_step_size, gradient_R, out=gradient_R); np.add(R, gradient_R, out=R)

            else:
                compute_model_stochastic_gradients(opt_vars, reg_params, Y_train, phi_train, model, indices, Omega_train, gradient_containers)

                noisy_gradient_L = gradient_containers[0]
                noisy_gradient_R_indices = gradient_containers[1]
                noisy_gradient_W = gradient_containers[2]
                noisy_gradient_b = gradient_containers[3]

                np.multiply( -step_size(t, model, True, delay), noisy_gradient_L, out=noisy_gradient_L); np.add(L, noisy_gradient_L, out=L)
                R[:,indices] = R[:,indices] - step_size(t, model, True, delay) * noisy_gradient_R_indices
                np.multiply( -step_size(t, model, True, delay), noisy_gradient_W, out=noisy_gradient_W); np.add(W, noisy_gradient_W, out=W)
                np.multiply( -step_size(t, model, True, delay), noisy_gradient_b, out=noisy_gradient_b); np.add(b, noisy_gradient_b, out=b)                


            # Put update back into opt_vars object
            opt_vars = [L, R, W, b]

        elif model == 'LowRankReg':

            # Extract variables
            H, U, b = extract_variables(opt_vars, model)
            lambda_H, lambda_U = extract_reg_parameters(reg_params, model)

            # Compute gradients and take a step
            if SGD:
                compute_model_stochastic_gradients(opt_vars, reg_params, Y_train, phi_train, model, indices, Omega_train, gradient_containers)

                noisy_gradient_H = gradient_containers[0]
                noisy_gradient_U = gradient_containers[1]
                noisy_gradient_b = gradient_containers[2]

                np.multiply( -step_size(t, model, SGD, delay), noisy_gradient_H, out=noisy_gradient_H); np.add(H, noisy_gradient_H, out=H)
                np.multiply( -step_size(t, model, SGD, delay), noisy_gradient_U, out=noisy_gradient_U); np.add(U, noisy_gradient_U, out=U)
                np.multiply( -step_size(t, model, SGD, delay), noisy_gradient_b, out=noisy_gradient_b); np.add(b, noisy_gradient_b, out=b)

            else:
                # compute_model_gradients(opt_vars, reg_params, Y_train, phi_train, model, Omega_train, gradient_containers)

                # gradient_H = gradient_containers[0]
                # gradient_U = gradient_containers[1]
                # b = gradient_containers[2]

                # np.multiply( -step_size(t, model, SGD, delay), gradient_H, out=gradient_H); np.add(H, gradient_H, out=H)
                # np.multiply( -step_size(t, model, SGD, delay), gradient_U, out=gradient_U); np.add(U, gradient_U, out=U)  


                # ALTERNATING GRADIENT DESCENT
                # Step in H
                compute_LowRankReg_gradients_H(opt_vars, reg_params, Y_train, phi_train, model, Omega_train, gradient_containers)
                gradient_H = gradient_containers[0]

                largest_singular_val_PU = np.linalg.svd(phi_train.T.dot(U.T).T, full_matrices=0, compute_uv=0)[0]
                best_step_size = N / float(pow(largest_singular_val_PU, 2) + lambda_H)
                np.multiply( -best_step_size, gradient_H, out=gradient_H); np.add(H, gradient_H, out=H)

                # Joint step in U, b
                compute_LowRankReg_gradients_U_b(opt_vars, reg_params, Y_train, phi_train, model, Omega_train, gradient_containers)
                gradient_U = gradient_containers[1]
                b = gradient_containers[2]

                largest_singular_val_PH = largest_singular_val_phi_train * (np.linalg.svd(H, full_matrices=0, compute_uv=0)[0])
                best_step_size = N / float(pow(largest_singular_val_PH, 2) + lambda_U)
                np.multiply( -best_step_size, gradient_H, out=gradient_H); np.add(H, gradient_H, out=H)



            # Put update back into opt_vars object
            opt_vars = [H, U, b]

        elif model == 'mf+LowRankReg':

            # Extract variables
            L, R, H, U, b = extract_variables(opt_vars, model)
            lambda_L, lambda_R, lambda_H, lambda_U = extract_reg_parameters(reg_params, model)

            # Compute gradients and take a step
            if SGD:
                compute_model_stochastic_gradients(opt_vars, reg_params, Y_train, phi_train, model, indices, Omega_train, gradient_containers)

                noisy_gradient_L = gradient_containers[0]
                noisy_gradient_R_indices = gradient_containers[1]
                noisy_gradient_H = gradient_containers[2]
                noisy_gradient_U = gradient_containers[3]
                noisy_gradient_b = gradient_containers[4]

                # Diagnostic stuff
                # old_HU = H.dot(U)
                # old_LR = L.dot(R)
                
                np.multiply( -step_size(t, model, SGD, delay), noisy_gradient_L, out=noisy_gradient_L); np.add(L, noisy_gradient_L, out=L)
                R[:,indices] = R[:,indices] - step_size(t, model, SGD, delay) * noisy_gradient_R_indices
                np.multiply( -step_size(t, model, SGD, delay), noisy_gradient_H, out=noisy_gradient_H); np.add(H, noisy_gradient_H, out=H)
                np.multiply( -step_size(t, model, SGD, delay), noisy_gradient_U, out=noisy_gradient_U); np.add(U, noisy_gradient_U, out=U)
                np.multiply( -step_size(t, model, SGD, delay), noisy_gradient_b, out=noisy_gradient_b); np.add(b, noisy_gradient_b, out=b)

                # Diagnostic stuff
                # H_gradient_norms.append(np.linalg.norm(noisy_gradient_H))
                # U_gradient_norms.append(np.linalg.norm(noisy_gradient_U))
                # L_gradient_norms.append(np.linalg.norm(noisy_gradient_L))
                # R_gradient_norms.append(np.linalg.norm(noisy_gradient_R_indices))

                # H_norms.append(np.linalg.norm(H))
                # U_norms.append(np.linalg.norm(U))
                # L_norms.append(np.linalg.norm(L))
                # R_norms.append(np.linalg.norm(R))

                # new_HU = H.dot(U)
                # new_LR = L.dot(R)
                # diff_HU.append(np.linalg.norm(old_HU - new_HU))
                # diff_LR.append(np.linalg.norm(old_LR - new_LR))

            else:
                # compute_model_gradients(opt_vars, reg_params, Y_train, phi_train, model, Omega_train, gradient_containers)

                # gradient_L = gradient_containers[0]
                # gradient_R = gradient_containers[1]
                # gradient_H = gradient_containers[2]
                # gradient_U = gradient_containers[3]
                # b = gradient_containers[4]

                # np.multiply( -step_size(t, model, SGD, delay), gradient_L, out=gradient_L); np.add(L, gradient_L, out=L)
                # np.multiply( -step_size(t, model, SGD, delay), gradient_R, out=gradient_R); np.add(R, gradient_R, out=R)
                # np.multiply( -step_size(t, model, SGD, delay), gradient_H, out=gradient_H); np.add(H, gradient_H, out=H)
                # np.multiply( -step_size(t, model, SGD, delay), gradient_U, out=gradient_U); np.add(U, gradient_U, out=U)   

                # ALTERNATING GRADIENT DESCENT
                # Joint step in H, L
                compute_mf_LowRankReg_gradients_H_L(opt_vars, reg_params, Y_train, phi_train, model, Omega_train, gradient_containers)
                gradient_H = gradient_containers[2]
                gradient_L = gradient_containers[0]

                largest_singular_val_PUR = np.linalg.svd(vstack((phi_train.T.dot(U.T).T, R)), full_matrices=0, compute_uv=0)[0]
                best_step_size = N / float(pow(largest_singular_val_PUR,2) + max(lambda_H, lambda_L))
                # if verbose:
                #     print 'Step size for H, L: {0}'.format(best_step_size)
                np.multiply( -best_step_size, gradient_H, out=gradient_H); np.add(H, gradient_H, out=H)
                np.multiply( -best_step_size, gradient_L, out=gradient_L); np.add(L, gradient_L, out=L)

                # Joint step in U, R, b
                compute_mf_LowRankReg_gradients_U_R_b(opt_vars, reg_params, Y_train, phi_train, model, Omega_train, gradient_containers)
                gradient_U = gradient_containers[3]
                gradient_R = gradient_containers[1]
                b = gradient_containers[4]

                upper_bound_on_largest_singular_val_PHL = largest_singular_val_phi_train * (np.linalg.svd(H, full_matrices=0, compute_uv=0)[0]) + \
                                                                                  np.linalg.svd(L, full_matrices=0, compute_uv=0)[0]
                best_step_size = N / float(pow(upper_bound_on_largest_singular_val_PHL,2) + max(lambda_U, lambda_R))
                # if verbose:
                #     print 'Step size for U, R: {0}'.format(best_step_size)
                np.multiply( -best_step_size, gradient_U, out=gradient_U); np.add(U, gradient_U, out=U)   
                np.multiply( -best_step_size, gradient_R, out=gradient_R); np.add(R, gradient_R, out=R)


            # Put update back into opt_vars object
            opt_vars = [L, R, H, U, b]



        if t % obj_val_freq == 0:

            # Calculate error for convergence detection
            old_obj = obj
            obj = full_objective(opt_vars, reg_params, Y_train, phi_train, missing_data_mask, model)

            # H_norms.append(np.linalg.norm(H))
            # U_norms.append(np.linalg.norm(U))
            # L_norms.append(np.linalg.norm(L))
            # R_norms.append(np.linalg.norm(R))

            # new_HU = H.dot(U)
            # new_LR = L.dot(R)
            # diff_HU.append(np.linalg.norm(new_HU, 'fro'))
            # diff_LR.append(np.linalg.norm(new_LR, 'fro'))

            if verbose:
                print "Objective: {0}".format(obj)
                print "Objective difference: {0}".format(abs(old_obj - obj))

            obj_values.append(obj)

            if Y_cold_start is not None:
                test_cold_start = test_cold_start_error(opt_vars, Y_cold_start, phi_cold_start, validation_cold_start_mask, model)
                test_cold_start_values.append(test_cold_start)
            else:
                test_cold_start_values.append(0)

            if not no_train_missing_values:
                test_missing_data = test_missing_data_error(opt_vars, Y_train, phi_train, validation_missing_data_mask, model)
                test_missing_data_values.append(test_missing_data)
            else:
                test_missing_data_values.append(0)

        if t % write_freq == 0 and process_num == 'Final Model': # Used for saving intermediate opt_vars
            pass

            # Things to dump: opt_vars, obj_values, test_missing_data_values, test_cold_start_values
            # do = Dump_Object(opt_vars, obj_values, test_cold_start_values, test_missing_data_values)
            # pickle.dump(do, open('opt_dumps/lambda1 {0}, lambda2 {1}, process_num {2}.pkl'.format(lambda1, lambda2, process_num), 'wb'))


        t += 1

    ##################### END: MAIN LOOP #####################

    print 'End of run. Obj value: {0}'.format(obj_values[-1])
    return opt_vars, obj_values, test_cold_start_values, test_missing_data_values


def smart_initialization_helper(Y_train, phi_train, k_in, model, lambda1, lambda2, Omega_train, completely_missing_Omega, init_opt_vars):

    T, N = Y_train.shape

    if model == 'reg':

        return init_opt_vars

    elif model == 'mf+reg':

        W, b = extract_variables(init_opt_vars, model)
        W_init = W
        b_init = b

        L_init = .05 * np.random.normal(0, 1, size=[T, k_in])
        R_init = .05 * np.random.normal(0, 1, size=[k_in, N])

        return [L_init, R_init, W_init, b_init]

    elif model == 'LowRankReg':

        return init_opt_vars

    elif model == 'mf+LowRankReg':

        H, U, b = extract_variables(init_opt_vars, 'LowRankReg')

        H_init = H
        U_init = U
        b_init = b

        # Perform a few steps of alternating gradient descent on L, R
        num_steps = 20

        L_init = .05 * np.random.normal(0, 1, size=[T, k_in])
        R_init = .05 * np.random.normal(0, 1, size=[k_in, N])

        # Replicating some code from cold_start_learning()
        no_train_missing_values = Omega_train is None
        if no_train_missing_values:
            Omega_train = [np.array([], dtype=int64) for i in xrange(N)]

        if completely_missing_Omega is None:
            completely_missing_Omega = [0, 0]
            completely_missing_Omega[0] = [np.array([]) for i in xrange(N)]
            if Y_cold_start is not None:
                completely_missing_Omega[1] = [np.array([]) for i in xrange(Y_cold_start.shape[1])]

        missing_data_mask = np.ones(Y_train.shape)
        Omega_train_copy = Omega_train[:]
        for i in xrange(N):
            if len(Omega_train[i]) > 0:
                missing_data_mask[Omega_train[i], i] = 0
            if len(completely_missing_Omega[0][i]) > 0: # This data is part of the test set
                missing_data_mask[completely_missing_Omega[0][i], i] = 0
                Omega_train_copy[i] = np.concatenate((Omega_train[i], completely_missing_Omega[0][i])) # Gradients won't touch this data. We are assuming Omega_train and completely_missing_Omega are disjoint


        opt_vars = [L_init, R_init, H_init, U_init, b_init]
        reg_params = [lambda2, lambda2, lambda1, lambda1]
        gradient_containers = [item.copy() for item in opt_vars]
        for step in range(num_steps):

            # Step in L
            compute_mf_LowRankReg_gradients_H_L(opt_vars, reg_params, Y_train, phi_train, model, Omega_train_copy, gradient_containers)
            gradient_L = gradient_containers[0]

            largest_singular_val_R = np.linalg.svd(R_init, full_matrices=0, compute_uv=0)[0]
            best_step_size = N / float(pow(largest_singular_val_R, 2) + lambda2)
            np.multiply( -best_step_size, gradient_L, out=gradient_L); np.add(L_init, gradient_L, out=L_init)

            # Joint step in U, R, b
            compute_mf_LowRankReg_gradients_U_R_b(opt_vars, reg_params, Y_train, phi_train, model, Omega_train_copy, gradient_containers)
            gradient_R = gradient_containers[1]

            largest_singular_val_L = np.linalg.svd(L_init, full_matrices=0, compute_uv=0)[0]
            best_step_size = N / float(pow(largest_singular_val_L, 2) + lambda2)
            np.multiply( -best_step_size, gradient_R, out=gradient_R); np.add(R_init, gradient_R, out=R_init)

            print "Objective: {0}".format(full_objective(opt_vars, reg_params, Y_train, phi_train, missing_data_mask, model))

        # return [L_init, R_init, H_init, U_init, b_init]  
        return opt_vars  

def smart_initialization(Y_train, phi_train, k_in, model, Omega_train=None, completely_missing_Omega=None, lambda1=1, lambda2=1, init_opt_vars=None, SGD=False, minibatch_size=1):

    # First we need to grab W if not given
    if model in ['reg', 'mf+reg'] and init_opt_vars is None:
        opt_vars, _blah, _blah_1, _blah_2 = cold_start_learning(Y_train, phi_train, k_in, 'reg', Omega_train=Omega_train, completely_missing_Omega=completely_missing_Omega, lambda1=lambda1, lambda2=lambda2, verbose=False, max_iters=200, SGD=SGD, minibatch_size=minibatch_size)
        W, b = extract_variables(opt_vars, 'reg')
        init_opt_vars = [W,b]
    elif model in ['LowRankReg', 'mf+LowRankReg'] and init_opt_vars is None:
        opt_vars, _blah, _blah_1, _blah_2 = cold_start_learning(Y_train, phi_train, k_in, 'LowRankReg', Omega_train=Omega_train, completely_missing_Omega=completely_missing_Omega, lambda1=lambda1, lambda2=lambda2, verbose=False, max_iters=200, SGD=SGD, minibatch_size=minibatch_size)
        H, U, b = extract_variables(opt_vars, 'LowRankReg')
        init_opt_vars = [H, U, b]

    return smart_initialization_helper(Y_train, phi_train, k_in, model, lambda1, lambda2, Omega_train, completely_missing_Omega, init_opt_vars)




