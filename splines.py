from pylab import *
from IPython import embed
from time import time

__author__ = 'Chris Xie'

""" This python file is just for me to try out some cubic spline fitting
"""

def generate_uniform_data(N, a, b):
    """ Sample N data points as follows:
        x_i ~ U[0,1]
        y_i | x_i ~ N(x_i, sigma^2)

        Return x, y, and f(x) (which is y without noise)
    """

    sigma = 1

    data = np.zeros((N, 3))
    for i in range(N):
        x = np.random.uniform(a, b)
        y = np.random.normal(0, pow(sigma,2))

        data[i,:] = [x, y, 0]

    return data

def uniform_knots(low, high, K):
    """ Sets K uniformly space knots from low to high. """
    return np.linspace(low, high, K+2)[1 : -1]

def linear_spline_basis_matrix(knots, x):
    """ Return a cubic spline basis matrix (using knots) evaluated at points x. """

    N = len(x)
    K = len(knots)
    p = K + 2
    B = np.zeros((N, p))

    # Standard linear basis
    B[:, 0] = 1             # 1
    B[:, 1] = x             # X

    # Truncated power basis
    for i in range(K):
        B[:,i + 2] = pow((x - knots[i]).clip(min=0), 1)

    return B

def quadratic_spline_basis_matrix(knots, x):
    """ Return a cubic spline basis matrix (using knots) evaluated at points x. """

    N = len(x)
    K = len(knots)
    p = K + 3
    B = np.zeros((N, p))

    # Standard quadratic basis
    B[:, 0] = 1             # 1
    B[:, 1] = x             # X
    B[:, 2] = pow(x, 2)     # X^2

    # Truncated power basis
    for i in range(K):
        B[:,i + 3] = pow((x - knots[i]).clip(min=0), 2)

    return B

def cubic_spline_basis_matrix(knots, x):
    """ Return a cubic spline basis matrix (using knots) evaluated at points x. """

    N = len(x)
    K = len(knots)
    p = K+4
    B = np.zeros((N, p))

    # Standard cubic basis
    B[:, 0] = 1             # 1
    B[:, 1] = x             # X
    B[:, 2] = pow(x, 2)     # X^2
    B[:, 3] = pow(x, 3)     # X^3

    # Truncated power basis
    for i in range(K):
        B[:,i + 4] = pow((x - knots[i]).clip(min=0), 3)

    return B


def generate_piecewise_polynomial_data(N, a, b):
    """ Generate data from a conditional distribution where the conditional mean
        is a piecewise cubic polynomial on [a, b]. y|x ~ N(x, sigma)
    """

    data = np.zeros((N, 3))
    data[:,0] = np.random.uniform(a, b, (N,)) # x's sampled uniformly from [0,1]

    # randomly generate a cubic spline
    generating_knots = uniform_knots(a, b, 2) # two knots
    generating_B = cubic_spline_basis_matrix(generating_knots, data[:,0])
    # generating_theta = np.random.normal(0, 500, (generating_B.shape[1]))
    generating_theta = np.array([1, 5, -2, -4, 0, 0])
    data[:,2] = generating_B.dot( generating_theta )

    # Now sample y's given x's
    sigma = 1
    for i in range(N):
        f_x = data[i, 2]
        y = np.random.normal(f_x, pow(sigma,2))

        data[i, 1] = y

    return data

def cubic_spline_regularization_matrix(knots, a, b):
    """ Returns Omega(x), where [Omega(x)]_{ij} = \int_a^b h_i''(x)h_j''(x) dx

        This is used to minimize ||Y - B(x)'theta||^2 + \gamma \int_a^b f''(x)^2 dx
        which penalizes the "wigglyness" of the function. The analytical answer is:

            theta_hat = (B(x)'B(x) + \gamma Omega(x))^{-1} * B(x)' Y

        Since Omega(x) is symmetric, only fill out the upper triangular portion, then compute
        Omega(x) + Omega(x)' and multiply each diagonal by 1/2
    """

    K = len(knots)
    p = K+4

    Omega = np.zeros((p, p))

    # This has to do with derivatives of standard cubic basis
    Omega[2, 2] = 4*(b-a)
    Omega[2, 3] = 6 * (pow(b,2) - pow(a,2))
    Omega[3, 3] = 12 * (pow(b,3) - pow(a,3))

    # This has to do with derivatives of standard cubic basis and truncated power basis
    for j in range(K):
        Omega[2, j+4] = 6 * (pow(b,2) - pow(knots[j], 2)) - 12 * knots[j] * (b - knots[j])
        Omega[3, j+4] = 12 * (pow(b,3) - pow(knots[j], 3)) - 18 * knots[j] * (pow(b, 2) - pow(knots[j], 2))

    # This has to do with derivatives of truncated power basis
    for i in range(K):
        for j in range(K):
            if j >= i:
                z = max(knots[i], knots[j])
                Omega[i+4,j+4] = 12 * (pow(b,3) - pow(z, 3)) \
                                 - 18 * (knots[i] + knots[j]) * (pow(b,2) - pow(z, 2)) \
                                 + 36 * knots[i] * knots[j] * (b - z)

    # Symmetrize, multiply diagonals by .5
    Omega = (np.ones((p,p)) - .5 * np.eye(p)) * (Omega + Omega.T)

    return Omega


def main():

    # Generate data
    N = 200
    a, b = -1, 1

    # data = generate_uniform_data(N)
    data = generate_piecewise_polynomial_data(N, a, b)
    data = data[ np.argsort(data[:,0]) ]    # Sort data based on x's
    x = data[:,0]; y = data[:,1]; f_x = data[:, 2]

    # Set knots uniformly spaced
    K = 10
    knots = uniform_knots(a, b, K)

    # Create basis matrix B(x)
    # B = linear_spline_basis_matrix(knots, x)
    # B = quadratic_spline_basis_matrix(knots, x)
    B = cubic_spline_basis_matrix(knots, x)

    # Second derivative regularization
    gamma = 0.01
    Omega = cubic_spline_regularization_matrix(knots, a, b)

    # Solve regression problem
    theta_hat_unreg = np.linalg.lstsq(B, y)[0] # Should be dimension p = K + 4
    theta_hat_reg = np.linalg.pinv(B.T.dot( B ) + gamma * Omega).dot( B.T.dot( y ) )

    # Evaluating spline at points x is simply the matrix multiplication:
    y_hat_unreg = B.dot( theta_hat_unreg )
    y_hat_reg = B.dot( theta_hat_reg )



    # Plot data points and spline
    plot(x, y, '.y', markersize=7)
    plot(x, f_x, linewidth=2.0, label='True function')     # True function
    plot(x, y_hat_unreg, linewidth=2.5, label='Linear Smoother Unregularized')   # Linear smoother
    plot(x, y_hat_reg, linewidth=2.5, label='Linear Smoother Regularized')   # Linear smoother
    legend(loc='lower right')
    title('Number of knots = {0}, gamma = {1}'.format(K, gamma))

    show()




if __name__ == '__main__':
    # Set random seed here
    # np.random.seed(0)
    np.random.seed(int(time()))

    main()


