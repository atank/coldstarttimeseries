import pandas as pd


def load_dataframes(data_file_names):
	""" Loads PANDAS dataframes from csv files with delimiter ','
	"""

	dataframes = []
	for file_name in data_file_names:
		dataframe = pd.read_csv(file_name, index_col=0)
		dataframes.append(dataframe)

	return dataframes

def extract_values(dataframes):
	""" Takes PANDAS dataframes and extracts the matrix of values
	"""

	matrices = []
	for dataframe in dataframes:
		matrices.append(dataframe.values)

	return matrices

def load_data(data_file_names):

	return extract_values( load_dataframes( data_file_names ) )