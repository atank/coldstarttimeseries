from pylab import *
from IPython import embed
from util import *
from time import time


W_access_times = 1


######### PER MODEL FULL GRADIENTS #########

def reg_gradient(opt_vars, reg_params, Y, phi, model, Omega, gradient_containers):
    N = Y.shape[1]

    # Extract the variables
    W, b = extract_variables(opt_vars, model)
    lambda_W = extract_reg_parameters(reg_params, model)

    # Extract containers
    gradient_W, b_star = gradient_containers

    # Precompute
    temp1 = phi.T.dot(W.T).T - Y
    temp = np.outer(b, np.ones(N)) + temp1

    # Handle missing data (multiplying by Pi_i as in my notebook)
    for i in xrange(N):
        if len(Omega[i]) > 0:
            temp[Omega[i], i] = 0
            temp1[Omega[i], i] = 0

    # Optimized calculation of gradients
    np.multiply(lambda_W / float(N), W, out=gradient_W); np.add(phi.dot( (temp).T ).T / float(N), gradient_W, out=gradient_W)
    # np.sum((-1 * temp + np.outer(b, np.ones(N)) ) / float(N), axis=1, out=b_star)
    np.sum((-1 * temp1) / float(N), axis=1, out=b_star)


    # Calculate gradients
    # gradient_W = phi.dot( (np.outer(b, np.ones(N)) + temp).T ).T / float(N) + lambda_W / float(N) * W
    # b_star = np.sum(-1 * temp, axis=1)/float(N)
    # return [gradient_W, b_star]

def mf_reg_gradient(opt_vars, reg_params, Y, phi, model, Omega, gradient_containers):
    N = Y.shape[1]

    # Extract the variables
    L, R, W, b = extract_variables(opt_vars, model)
    lambda_L, lambda_R, lambda_W = extract_reg_parameters(reg_params, model)

    # Extract containers
    gradient_L, gradient_R, gradient_W, b_star = gradient_containers

    # Precompute
    temp = np.outer(b, np.ones(N)) + phi.T.dot(W.T).T + L.dot(R) - Y

    # Handle missing data (multiplying by Pi_i as in my notebook)
    for i in xrange(N):
        if len(Omega[i]) > 0:
            temp[Omega[i], i] = 0

    # Optimized calculation of gradients
    np.multiply(lambda_L / float(N), L, out=gradient_L); np.add(temp.dot(R.T) / float(N), gradient_L, out=gradient_L)
    np.multiply(lambda_R / float(N), R, out=gradient_R); np.add(L.T.dot( temp ) / float(N), gradient_R, out=gradient_R)
    np.multiply(lambda_W / float(N), W, out=gradient_W); np.add(phi.dot( temp.T ).T / float(N), gradient_W, out=gradient_W)
    np.sum((-1 * temp + np.outer(b, np.ones(N)) ) / float(N), axis=1, out=b_star)

    # Calculate the gradients
    # gradient_L = temp.dot( R.T ) / float(N) + lambda_L / float(N) * L
    # gradient_R = L.T.dot( temp ) / float(N) + lambda_R / float(N) * R
    # gradient_W = phi.dot( temp.T ).T / float(N) + lambda_W / float(N) * W
    # b_star = np.sum(-1 * temp + np.outer(b, np.ones(N)), axis=1)/float(N)
    # return [gradient_L, gradient_R, gradient_W, b_star]

def compute_reg_gradients_W_L(opt_vars, reg_params, Y, phi, model, Omega, gradient_containers):
    N = Y.shape[1]

    # Extract the variables
    L, R, W, b = extract_variables(opt_vars, model)
    lambda_L, lambda_R, lambda_W = extract_reg_parameters(reg_params, model)

    # Extract containers
    gradient_L, gradient_R, gradient_W, b_star = gradient_containers

    # Precompute
    temp = np.outer(b, np.ones(N)) + phi.T.dot(W.T).T + L.dot(R) - Y

    # Handle missing data (multiplying by Pi_i as in my notebook)
    for i in xrange(N):
        if len(Omega[i]) > 0:
            temp[Omega[i], i] = 0

    # Optimized calculation of gradients
    np.multiply(lambda_L / float(N), L, out=gradient_L); np.add(temp.dot(R.T) / float(N), gradient_L, out=gradient_L)
    np.multiply(lambda_W / float(N), W, out=gradient_W); np.add(phi.dot( temp.T ).T / float(N), gradient_W, out=gradient_W)

def compute_reg_gradients_R_b(opt_vars, reg_params, Y, phi, model, Omega, gradient_containers):
    N = Y.shape[1]

    # Extract the variables
    L, R, W, b = extract_variables(opt_vars, model)
    lambda_L, lambda_R, lambda_W = extract_reg_parameters(reg_params, model)

    # Extract containers
    gradient_L, gradient_R, gradient_W, b_star = gradient_containers

    # Precompute
    temp = np.outer(b, np.ones(N)) + phi.T.dot(W.T).T + L.dot(R) - Y

    # Handle missing data (multiplying by Pi_i as in my notebook)
    for i in xrange(N):
        if len(Omega[i]) > 0:
            temp[Omega[i], i] = 0

    # Optimized calculation of gradients
    np.multiply(lambda_R / float(N), R, out=gradient_R); np.add(L.T.dot( temp ) / float(N), gradient_R, out=gradient_R)
    np.sum((-1 * temp + np.outer(b, np.ones(N)) ) / float(N), axis=1, out=b_star)

def LowRankReg_gradient(opt_vars, reg_params, Y, phi, model, Omega, gradient_containers):
    N = Y.shape[1]

    # Extract the variables
    H, U, b = extract_variables(opt_vars, model)
    lambda_H, lambda_U = extract_reg_parameters(reg_params, model)    

    # Extract containers
    gradient_H, gradient_U, b_star = gradient_containers

    # Precompute
    phi_T_U_T = phi.T.dot(U.T)
    temp = np.outer(b, np.ones(N)) + phi_T_U_T.dot(H.T).T - Y

    # Handle missing data (multiplying by Pi_i as in my notebook)
    for i in xrange(N):
        if len(Omega[i]) > 0:
            temp[Omega[i], i] = 0

    # Optimized calculation of gradients
    np.multiply(lambda_H / float(N), H, out=gradient_H); np.add(temp.dot( phi_T_U_T ) / float(N), gradient_H, out=gradient_H)
    np.multiply(lambda_U / float(N), U, out=gradient_U); np.add(phi.dot( temp.T.dot(H) ).T / float(N), gradient_U, out=gradient_U)
    np.sum((-1 * temp + np.outer(b, np.ones(N)) ) / float(N), axis=1, out=b_star)

    # Calculate the gradients
    # gradient_H = temp.dot( phi_T_U_T ) / float(N) + lambda_H / float(N) * H
    # gradient_U = phi.dot( temp.T.dot(H) ).T / float(N) + lambda_U / float(N) * U
    # b_star = np.sum(-1 * temp + np.outer(b, np.ones(N)), axis=1)/float(N)
    # return [gradient_H, gradient_U, b_star]

def compute_LowRankReg_gradients_H(opt_vars, reg_params, Y, phi, model, Omega, gradient_containers):
    N = Y.shape[1]

    # Extract the variables
    H, U, b = extract_variables(opt_vars, model)
    lambda_H, lambda_U = extract_reg_parameters(reg_params, model)    

    # Extract containers
    gradient_H, gradient_U, b_star = gradient_containers

    # Precompute
    phi_T_U_T = phi.T.dot(U.T)
    temp = np.outer(b, np.ones(N)) + phi_T_U_T.dot(H.T).T - Y

    # Handle missing data (multiplying by Pi_i as in my notebook)
    for i in xrange(N):
        if len(Omega[i]) > 0:
            temp[Omega[i], i] = 0

    # Optimized calculation of gradients
    np.multiply(lambda_H / float(N), H, out=gradient_H); np.add(temp.dot( phi_T_U_T ) / float(N), gradient_H, out=gradient_H)

def compute_LowRankReg_gradients_U_b(opt_vars, reg_params, Y, phi, model, Omega, gradient_containers):
    N = Y.shape[1]

    # Extract the variables
    H, U, b = extract_variables(opt_vars, model)
    lambda_H, lambda_U = extract_reg_parameters(reg_params, model)    

    # Extract containers
    gradient_H, gradient_U, b_star = gradient_containers

    # Precompute
    phi_T_U_T = phi.T.dot(U.T)
    temp = np.outer(b, np.ones(N)) + phi_T_U_T.dot(H.T).T - Y

    # Handle missing data (multiplying by Pi_i as in my notebook)
    for i in xrange(N):
        if len(Omega[i]) > 0:
            temp[Omega[i], i] = 0

    # Optimized calculation of gradients
    np.multiply(lambda_U / float(N), U, out=gradient_U); np.add(phi.dot( temp.T.dot(H) ).T / float(N), gradient_U, out=gradient_U)
    np.sum((-1 * temp + np.outer(b, np.ones(N)) ) / float(N), axis=1, out=b_star)


def mf_LowRankReg_gradient(opt_vars, reg_params, Y, phi, model, Omega, gradient_containers):
    N = Y.shape[1]

    # Extract the variables
    L, R, H, U, b = extract_variables(opt_vars, model)
    lambda_L, lambda_R, lambda_H, lambda_U = extract_reg_parameters(reg_params, model)

    # Extract containers
    gradient_L, gradient_R, gradient_H, gradient_U, b_star = gradient_containers

    # Precompute
    phi_T_U_T = phi.T.dot(U.T)    
    temp = np.outer(b, np.ones(N)) + phi_T_U_T.dot(H.T).T + L.dot(R) - Y

    # Handle missing data (multiplying by Pi_i as in my notebook)
    for i in xrange(N):
        if len(Omega[i]) > 0:
            temp[Omega[i], i] = 0
    
    # Optimized calculation of gradients
    np.multiply(lambda_L / float(N), L, out=gradient_L); np.add(temp.dot(R.T) / float(N), gradient_L, out=gradient_L)
    np.multiply(lambda_R / float(N), R, out=gradient_R); np.add(L.T.dot( temp ) / float(N), gradient_R, out=gradient_R)
    np.multiply(lambda_H / float(N), H, out=gradient_H); np.add(temp.dot( phi_T_U_T ) / float(N), gradient_H, out=gradient_H)
    np.multiply(lambda_U / float(N), U, out=gradient_U); np.add(phi.dot( temp.T.dot(H) ).T / float(N), gradient_U, out=gradient_U)
    np.sum((-1 * temp + np.outer(b, np.ones(N)) ) / float(N), axis=1, out=b_star)

    # Calculate the gradients
    # gradient_L = temp.dot( R.T ) / float(N) + lambda_L / float(N) * L
    # gradient_R = L.T.dot( temp ) / float(N) + lambda_R / float(N) * R
    # gradient_H = temp.dot( phi_T_U_T ) / float(N) + lambda_H / float(N) * H
    # gradient_U = phi.dot( temp.T.dot(H) ).T / float(N) + lambda_U / float(N) * U
    # b_star = np.sum(-1 * temp + np.outer(b, np.ones(N)), axis=1)/float(N)
    # return [gradient_L, gradient_R, gradient_H, gradient_U, b_star]

def compute_mf_LowRankReg_gradients_H_L(opt_vars, reg_params, Y, phi, model, Omega, gradient_containers):
    N = Y.shape[1]

    # Extract the variables
    L, R, H, U, b = extract_variables(opt_vars, model)
    lambda_L, lambda_R, lambda_H, lambda_U = extract_reg_parameters(reg_params, model)

    # Extract containers
    gradient_L, gradient_R, gradient_H, gradient_U, b_star = gradient_containers

    # Precompute
    phi_T_U_T = phi.T.dot(U.T)    
    temp = np.outer(b, np.ones(N)) + phi_T_U_T.dot(H.T).T + L.dot(R) - Y

    # Handle missing data (multiplying by Pi_i as in my notebook)
    for i in xrange(N):
        if len(Omega[i]) > 0:
            temp[Omega[i], i] = 0
    
    # Optimized calculation of gradients
    np.multiply(lambda_H / float(N), H, out=gradient_H); np.add(temp.dot( phi_T_U_T ) / float(N), gradient_H, out=gradient_H)
    np.multiply(lambda_L / float(N), L, out=gradient_L); np.add(temp.dot(R.T) / float(N), gradient_L, out=gradient_L)

def compute_mf_LowRankReg_gradients_U_R_b(opt_vars, reg_params, Y, phi, model, Omega, gradient_containers):
    N = Y.shape[1]

    # Extract the variables
    L, R, H, U, b = extract_variables(opt_vars, model)
    lambda_L, lambda_R, lambda_H, lambda_U = extract_reg_parameters(reg_params, model)

    # Extract containers
    gradient_L, gradient_R, gradient_H, gradient_U, b_star = gradient_containers

    # Precompute
    phi_T_U_T = phi.T.dot(U.T)    
    temp = np.outer(b, np.ones(N)) + phi_T_U_T.dot(H.T).T + L.dot(R) - Y

    # Handle missing data (multiplying by Pi_i as in my notebook)
    for i in xrange(N):
        if len(Omega[i]) > 0:
            temp[Omega[i], i] = 0
    
    # Optimized calculation of gradients
    np.multiply(lambda_U / float(N), U, out=gradient_U); np.add(phi.dot( temp.T.dot(H) ).T / float(N), gradient_U, out=gradient_U)
    np.multiply(lambda_R / float(N), R, out=gradient_R); np.add(L.T.dot( temp ) / float(N), gradient_R, out=gradient_R)
    np.sum((-1 * temp + np.outer(b, np.ones(N)) ) / float(N), axis=1, out=b_star)




def compute_model_gradients(opt_vars, reg_params, Y, phi, model, Omega, gradient_containers):

    # Returns gradients per model to save computation. Also returns optimal choice of b
    if model == 'reg':
        reg_gradient(opt_vars, reg_params, Y, phi, model, Omega, gradient_containers)
    elif model == 'mf+reg':
        mf_reg_gradient(opt_vars, reg_params, Y, phi, model, Omega, gradient_containers)
    elif model == 'LowRankReg':
        LowRankReg_gradient(opt_vars, reg_params, Y, phi, model, Omega, gradient_containers)
    elif model == 'mf+LowRankReg':
        mf_LowRankReg_gradient(opt_vars, reg_params, Y, phi, model, Omega, gradient_containers)








######### PER MODEL STOCHASTIC GRADIENTS #########

def initialize_access_time_matrix(opt_vars, model):
    """ This initializes an access time matrix for W. This is only used for models:

        REG, MF+REG

        The array W_access_times keeps track of when the last time each COLUMN was accessed. Delayed regularization is for COLUMNS
    """
    if model not in ['reg', 'mf+reg']:
        raise Exception('MODEL MUST BE IN {reg, mf+reg}')

    global W_access_times

    if model == 'reg':
        W, b = extract_variables(opt_vars, model)
    else:
        L, R, W, b = extract_variables(opt_vars, model)

    W_access_times = np.zeros(W.shape[1], dtype=int64) # Regularization last happened at time 0 (first iteration starts at t=1)


def reg_stochastic_gradient_lazy(opt_vars, reg_params, Y, phi, model, indices, Omega, gradient_containers, t):
    N = Y.shape[1]
    S = len(indices)

    # Extract the variables
    W, b = extract_variables(opt_vars, model)
    lambda_W = extract_reg_parameters(reg_params, model)

    # Perform delayed regularization (using access times and t) before computing gradients (this makes things correct)
    # All indices of phi that are nonzero are the indices of the columns of W that need to be updated
    set_of_nonzero_indices = set()
    for i in indices:
        set_of_nonzero_indices = set_of_nonzero_indices.union( set(np.nonzero(phi[:,i])[0]) )

    # Do the delayed regularization individually for each index
    print len(set_of_nonzero_indices)
    array_of_nonzero_indices = np.array(list(set_of_nonzero_indices))
    W[:, array_of_nonzero_indices] *= np.power(1 - step_size(t,model,SGD=True) * lambda_W / float(N) , t - W_access_times[array_of_nonzero_indices] - 1) # auto column multiplication
    W_access_times[array_of_nonzero_indices] = t-1
    # for index in set_of_nonzero_indices:
    #     mult_factor = 1
    #     for _t in range(W_access_times[index]+1, t): # We last accessed it at time W_access_times[index]
    #         mult_factor *= (1 - step_size(_t, model, SGD=True) * lambda_W / float(N))
    #     W[:, index] *= mult_factor
    #     W_access_times[index] = t-1


    # Gradient calculations
    # Precompute
    temp = np.outer(b, np.ones(S)) + phi[:,indices].T.dot(W.T).T - Y[:,indices]

    # Sanity Check
    # true_W, true_b = extract_variables(true_opt_vars, model)
    # true_temp = np.outer(true_b, np.ones(S)) + phi[:,indices].T.dot(true_W.T).T - Y[:,indices]
    # print np.all(np.isclose(temp, true_temp))
    # embed()

    # Handle missing data (multiplying by Pi_i as in my notebook)
    for idx, i in enumerate(indices):
        if len(Omega[i]) > 0:
            temp[Omega[i], idx] = 0

    # Can do in-place with W[update_indices[0], update_indices[1]] = W - step_size * ( lambda_W / N * W + phi[:, indices].dot(_temp.T).T / S )
    # Calculate gradient without regularization
    _temp = phi[:,indices].dot( temp.T ).T / float(S)
    W_column_update_indices = np.array(list(set(np.nonzero(_temp)[1])))

    # Perform gradient update (only on columns of W that will get updated)
    W[:, W_column_update_indices] *= 1 - step_size(t, model, SGD=True) * (lambda_W / float(N)) 
    W[:, W_column_update_indices] -= step_size(t, model, SGD=True) * _temp[:, W_column_update_indices]
    W_access_times[W_column_update_indices] = t
    b -= step_size(t, model, SGD=True) * np.sum(temp / float(S), axis=1)


def reg_stochastic_gradient(opt_vars, reg_params, Y, phi, model, indices, Omega, gradient_containers):
    N = Y.shape[1]
    S = len(indices)

    # Extract the variables
    W, b = extract_variables(opt_vars, model)
    lambda_W = extract_reg_parameters(reg_params, model)

    # Extract containers
    noisy_gradient_W, noisy_gradient_b = gradient_containers

    # Perform delayed regularization (using access times and t) before computing gradients (this makes things correct)
    # Need to figure out what parts of W needs to be regularized

    # Precompute
    temp = np.outer(b, np.ones(S)) + phi[:,indices].T.dot(W.T).T - Y[:,indices]

    # Handle missing data (multiplying by Pi_i as in my notebook)
    for idx, i in enumerate(indices):
        if len(Omega[i]) > 0:
            temp[Omega[i], idx] = 0

    # Optimized calculation of stochastic gradients
    np.multiply(lambda_W / float(N), W, out=noisy_gradient_W); np.add(phi[:,indices].dot( temp.T ).T / float(S), noisy_gradient_W, out=noisy_gradient_W)
    np.sum(temp / float(S), axis=1, out=noisy_gradient_b)

    # Calculate the stochastic gradients
    # noisy_gradient_W = phi[:,indices].dot( temp.T ).T / float(S) + lambda_W / float(N) * W
    # noisy_gradient_b = np.sum(temp, axis=1) / float(S)
    # return [noisy_gradient_W, noisy_gradient_b]

def mf_reg_stochastic_gradient(opt_vars, reg_params, Y, phi, model, indices, Omega, gradient_containers):
    N = Y.shape[1]
    S = len(indices)

    # Extract the variables
    L, R, W, b = extract_variables(opt_vars, model)
    R_indices = R[:, indices]
    lambda_L, lambda_R, lambda_W = extract_reg_parameters(reg_params, model)

    # Extract containers
    noisy_gradient_L, noisy_gradient_R_indices, noisy_gradient_W, noisy_gradient_b = gradient_containers
    if noisy_gradient_R_indices.shape[1] !=  S: # Handles last minibatch that is not same size
        noisy_gradient_R_indices = np.empty((noisy_gradient_R_indices.shape[0], S))
        gradient_containers[1] = noisy_gradient_R_indices

    # Precompute
    temp = np.outer(b, np.ones(S)) + phi[:,indices].T.dot(W.T).T + L.dot(R_indices) - Y[:,indices]

    # Handle missing data (multiplying by Pi_i as in my notebook)
    for idx, i in enumerate(indices):
        if len(Omega[i]) > 0:
            temp[Omega[i], idx] = 0

    # Optimized calculation of stochastic gradients
    np.multiply(lambda_L / float(N), L, out=noisy_gradient_L); np.add(temp.dot( R_indices.T ) / float(S), noisy_gradient_L, out=noisy_gradient_L)
    np.multiply(lambda_R / float(S), R_indices, out=noisy_gradient_R_indices); np.add(L.T.dot( temp ) / float(S), noisy_gradient_R_indices, out=noisy_gradient_R_indices)
    np.multiply(lambda_W / float(N), W, out=noisy_gradient_W); np.add(phi[:,indices].dot( temp.T ).T / float(S), noisy_gradient_W, out=noisy_gradient_W)
    np.sum(temp / float(S), axis=1, out=noisy_gradient_b)

    # Calculate the stochastic gradients
    # noisy_gradient_L = temp.dot( R_indices.T ) / float(S) + lambda_L / float(N) * L
    # noisy_gradient_R_indices = L.T.dot( temp ) / float(S) + lambda_R / float(S) * R_indices
    # noisy_gradient_W = phi[:,indices].dot( temp.T ).T / float(S) + lambda_W  / float(N) * W
    # noisy_gradient_b = np.sum(temp, axis=1) / float(S)
    # return [noisy_gradient_L, noisy_gradient_R_indices, noisy_gradient_W, noisy_gradient_b, indices]

def LowRankReg_stochastic_gradient(opt_vars, reg_params, Y, phi, model, indices, Omega, gradient_containers):
    N = Y.shape[1]
    S = len(indices)

    # Extract the variables
    H, U, b = extract_variables(opt_vars, model)
    lambda_H, lambda_U = extract_reg_parameters(reg_params, model)    

    # Extract containers
    noisy_gradient_H, noisy_gradient_U, noisy_gradient_b = gradient_containers

    # Precompute
    phi_i_T_U_T = phi[:,indices].T.dot(U.T)
    temp = np.outer(b, np.ones(S)) + phi_i_T_U_T.dot(H.T).T - Y[:,indices]

    # Handle missing data (multiplying by Pi_i as in my notebook)
    for idx, i in enumerate(indices):
        if len(Omega[i]) > 0:
            temp[Omega[i], idx] = 0

    # Optimized calculation of stochastic gradients
    np.multiply(lambda_H / float(N), H, out=noisy_gradient_H); np.add(temp.dot( phi_i_T_U_T ) / float(S), noisy_gradient_H, out=noisy_gradient_H)
    np.multiply(lambda_U / float(N), U, out=noisy_gradient_U); np.add(phi[:,indices].dot( temp.T.dot(H) ).T / float(S), noisy_gradient_U, out=noisy_gradient_U)
    np.sum(temp / float(S), axis=1, out=noisy_gradient_b)

    # Calculate the stochastic gradients
    # noisy_gradient_H = temp.dot( phi_i_T_U_T ) / float(S) + lambda_H / float(N) * H
    # noisy_gradient_U = phi[:,indices].dot( temp.T.dot(H) ).T / float(S) + lambda_U / float(N) * U
    # noisy_gradient_b = np.sum(temp, axis=1) / float(S)
    # return [noisy_gradient_H, noisy_gradient_U, noisy_gradient_b]

def mf_LowRankReg_stochastic_gradient(opt_vars, reg_params, Y, phi, model, indices, Omega, gradient_containers):
    N = Y.shape[1]
    S = len(indices)

    # Extract the variables
    L, R, H, U, b = extract_variables(opt_vars, model)
    R_indices = R[:, indices]
    lambda_L, lambda_R, lambda_H, lambda_U = extract_reg_parameters(reg_params, model)

    # Extract containers
    noisy_gradient_L, noisy_gradient_R_indices, noisy_gradient_H, noisy_gradient_U, noisy_gradient_b = gradient_containers
    if noisy_gradient_R_indices.shape[1] !=  S: # Handles last minibatch that is not same size
        noisy_gradient_R_indices = np.empty((noisy_gradient_R_indices.shape[0], S))
        gradient_containers[1] = noisy_gradient_R_indices

    # Precompute
    phi_i_T_U_T = phi[:,indices].T.dot(U.T)    
    temp = np.outer(b, np.ones(S)) + phi_i_T_U_T.dot(H.T).T + L.dot(R_indices) - Y[:,indices]

    # Handle missing data (multiplying by Pi_i as in my notebook)
    for idx, i in enumerate(indices):
        if len(Omega[i]) > 0:
            temp[Omega[i], idx] = 0

    # Optimized calculation of stochastic gradients
    np.multiply(lambda_L / float(N), L, out=noisy_gradient_L); np.add(temp.dot( R_indices.T ) / float(S), noisy_gradient_L, out=noisy_gradient_L)
    np.multiply(lambda_R / float(S), R_indices, out=noisy_gradient_R_indices); np.add(L.T.dot( temp ) / float(S), noisy_gradient_R_indices, out=noisy_gradient_R_indices)
    np.multiply(lambda_H / float(N), H, out=noisy_gradient_H); np.add(temp.dot( phi_i_T_U_T ) / float(S), noisy_gradient_H, out=noisy_gradient_H)
    np.multiply(lambda_U / float(N), U, out=noisy_gradient_U); np.add(phi[:,indices].dot( temp.T.dot(H) ).T / float(S), noisy_gradient_U, out=noisy_gradient_U)
    np.sum(temp / float(S), axis=1, out=noisy_gradient_b)

    # Calculate the stochastic gradients
    # noisy_gradient_L = temp.dot( R_indices.T ) / float(S) + lambda_L / float(N) * L
    # noisy_gradient_R_indices = L.T.dot( temp ) / float(S) + lambda_R / float(S) * R_indices
    # noisy_gradient_H = temp.dot( phi_i_T_U_T ) / float(S) + lambda_H / float(N) * H
    # noisy_gradient_U = phi[:,indices].dot( temp.T.dot(H) ).T / float(S) + lambda_U / float(N) * U
    # noisy_gradient_b = np.sum(temp, axis=1) / float(S)
    # return [noisy_gradient_L, noisy_gradient_R_indices, noisy_gradient_H, noisy_gradient_U, noisy_gradient_b, indices]


def compute_model_stochastic_gradients(opt_vars, reg_params, Y, phi, model, indices, Omega, gradient_containers):

    # Returns gradients per model to save computation
    if model == 'reg':
        reg_stochastic_gradient(opt_vars, reg_params, Y, phi, model, indices, Omega, gradient_containers)
    elif model == 'mf+reg':
        mf_reg_stochastic_gradient(opt_vars, reg_params, Y, phi, model, indices, Omega, gradient_containers)
    elif model == 'LowRankReg':
        LowRankReg_stochastic_gradient(opt_vars, reg_params, Y, phi, model, indices, Omega, gradient_containers)
    elif model == 'mf+LowRankReg':
        mf_LowRankReg_stochastic_gradient(opt_vars, reg_params, Y, phi, model, indices, Omega, gradient_containers)










################ GRADIENTS FOR SPLINE FORMULATION #################

def reg_with_splines_gradient(opt_vars, reg_params, B, Y, phi, model, Omega, gradient_containers):
    N = Y.shape[1]

    # Extract the variables
    Q, b = extract_variables(opt_vars, model)

    # Extract containers
    gradient_Q, b_star = gradient_containers

    # Precompute
    temp1 = phi.T.dot(Q.T).dot(B.T).T - Y
    temp = np.outer(b, np.ones(N)) + temp1

    # Handle missing data (multiplying by Pi_i as in my notebook)
    for i in xrange(N):
        if len(Omega[i]) > 0:
            temp[Omega[i], i] = 0
            temp1[Omega[i], i] = 0

    # Optimized calculation of gradients
    gradient_Q = phi.dot( temp.T.dot(B) ).T / float(N); gradient_containers[0] = gradient_Q
    np.sum((-1 * temp1) / float(N), axis=1, out=b_star)

def compute_model_with_splines_gradients(opt_vars, reg_params, B, Y, phi, model, Omega, gradient_containers):

    # Returns gradients per model to save computation. Also returns optimal choice of b
    if model == 'reg':
        reg_with_splines_gradient(opt_vars, reg_params, B, Y, phi, model, Omega, gradient_containers)
    else:
        raise Exception("Full Gradients not implemented for models other than regression")






def mf_reg_with_splines_stochastic_gradient(opt_vars, reg_params, B, Y, phi, model, indices, Omega, gradient_containers):
    N = Y.shape[1]
    S = len(indices)

    # Extract the variables
    L, R, Q, b = extract_variables(opt_vars, model)
    R_indices = R[:, indices]
    lambda_L, lambda_R = extract_reg_parameters_with_splines(reg_params, model)

    # Extract containers
    noisy_gradient_L, noisy_gradient_R_indices, noisy_gradient_Q, noisy_gradient_b = gradient_containers
    if noisy_gradient_R_indices.shape[1] !=  S: # Handles last minibatch that is not same size
        noisy_gradient_R_indices = np.empty((noisy_gradient_R_indices.shape[0], S))
        gradient_containers[1] = noisy_gradient_R_indices

    # Precompute
    temp = np.outer(b, np.ones(S)) + L.dot(R_indices) + phi[:,indices].T.dot(Q.T).dot(B.T).T - Y[:,indices]

    # Handle missing data (multiplying by Pi_i as in my notebook)
    for idx, i in enumerate(indices):
        if len(Omega[i]) > 0:
            temp[Omega[i], idx] = 0

    # Optimized calculation of stochastic gradients
    np.multiply(lambda_L / float(N), L, out=noisy_gradient_L); np.add(temp.dot( R_indices.T ) / float(S), noisy_gradient_L, out=noisy_gradient_L)
    np.multiply(lambda_R / float(S), R_indices, out=noisy_gradient_R_indices); np.add( L.T.dot( temp ) / float(S), noisy_gradient_R_indices, out=noisy_gradient_R_indices )
    noisy_gradient_Q = phi[:,indices].dot( temp.T.dot(B) ).T / float(S); gradient_containers[2] = noisy_gradient_Q
    np.sum(temp / float(S), axis=1, out=noisy_gradient_b)


def LowRankReg_with_splines_stochastic_gradient(opt_vars, reg_params, B, Y, phi, model, indices, Omega, gradient_containers):
    N = Y.shape[1]
    S = len(indices)

    # Extract the variables
    M, U, b = extract_variables(opt_vars, model)
    lambda_U = extract_reg_parameters_with_splines(reg_params, model)    

    # Extract containers
    noisy_gradient_M, noisy_gradient_U, noisy_gradient_b = gradient_containers

    # Precompute
    phi_i_T_U_T = phi[:,indices].T.dot(U.T)
    temp = np.outer(b, np.ones(S)) + phi_i_T_U_T.dot(M.T).dot(B.T).T - Y[:,indices]

    # Handle missing data (multiplying by Pi_i as in my notebook)
    for idx, i in enumerate(indices):
        if len(Omega[i]) > 0:
            temp[Omega[i], idx] = 0

    # Optimized calculation of stochastic gradients
    noisy_gradient_M = B.T.dot(temp).dot(phi_i_T_U_T) / float(S); gradient_containers[0] = noisy_gradient_M
    np.multiply(lambda_U / float(N), U, out=noisy_gradient_U); np.add(phi[:,indices].dot( temp.T.dot(B.dot(M)) ).T / float(S), noisy_gradient_U, out=noisy_gradient_U)
    np.sum(temp / float(S), axis=1, out=noisy_gradient_b)


def mf_LowRankReg_with_splines_stochastic_gradient(opt_vars, reg_params, B, Y, phi, model, indices, Omega, gradient_containers):
    N = Y.shape[1]
    S = len(indices)

    # Extract the variables
    L, R, M, U, b = extract_variables(opt_vars, model)
    R_indices = R[:, indices]
    lambda_L, lambda_R, lambda_U = extract_reg_parameters_with_splines(reg_params, model)    

    # Extract containers
    noisy_gradient_L, noisy_gradient_R_indices, noisy_gradient_M, noisy_gradient_U, noisy_gradient_b = gradient_containers
    if noisy_gradient_R_indices.shape[1] !=  S: # Handles last minibatch that is not same size
        noisy_gradient_R_indices = np.empty((noisy_gradient_R_indices.shape[0], S))
        gradient_containers[1] = noisy_gradient_R_indices

    # Precompute
    
    phi_i_T_U_T = phi[:,indices].T.dot(U.T)    
    # temp = np.outer(b, np.ones(S)) + phi_i_T_U_T.dot(H.T).T + L.dot(R_indices) - Y[:,indices]
    temp = np.outer(b, np.ones(S)) + L.dot(R_indices) + phi_i_T_U_T.dot(M.T).dot(B.T).T - Y[:,indices]

    # Handle missing data (multiplying by Pi_i as in my notebook)
    for idx, i in enumerate(indices):
        if len(Omega[i]) > 0:
            temp[Omega[i], idx] = 0

    # Optimized calculation of stochastic gradients
    np.multiply(lambda_L / float(N), L, out=noisy_gradient_L); np.add(temp.dot( R_indices.T ) / float(S), noisy_gradient_L, out=noisy_gradient_L)
    np.multiply(lambda_R / float(S), R_indices, out=noisy_gradient_R_indices); np.add( L.T.dot( temp ) / float(S), noisy_gradient_R_indices, out=noisy_gradient_R_indices )
    noisy_gradient_M = B.T.dot(temp).dot(phi_i_T_U_T) / float(S); gradient_containers[2] = noisy_gradient_M
    np.multiply(lambda_U / float(N), U, out=noisy_gradient_U); np.add(phi[:,indices].dot( temp.T.dot(B.dot(M)) ).T / float(S), noisy_gradient_U, out=noisy_gradient_U)
    np.sum(temp / float(S), axis=1, out=noisy_gradient_b)


def compute_model_with_splines_stochastic_gradients(opt_vars, reg_params, B, Y, phi, model, indices, Omega, gradient_containers):

    # Returns gradients per model to save computation
    if model == 'reg':
        raise Exception("Stochastic Gradients not implemented for regression")        
    elif model == 'mf+reg':
        mf_reg_with_splines_stochastic_gradient(opt_vars, reg_params, B, Y, phi, model, indices, Omega, gradient_containers)
    elif model == 'LowRankReg':
        LowRankReg_with_splines_stochastic_gradient(opt_vars, reg_params, B, Y, phi, model, indices, Omega, gradient_containers)
    elif model == 'mf+LowRankReg':
        mf_LowRankReg_with_splines_stochastic_gradient(opt_vars, reg_params, B, Y, phi, model, indices, Omega, gradient_containers)





