from pylab import *
from IPython import embed
from time import time
import scipy.sparse
import scipy.sparse.linalg
import sys, os
import pandas as pd
import pickle
# import sklearn
# import sklearn.decomposition
# from sklearn.decomposition import TruncatedSVD

from generate_series import *
from util import *
from cold_start_time_series import *

import logging
from gridmap import grid_map

# Profiling:
# python -m cProfile -o mycode.prof cold_start_time_series.py
# pyprof2calltree -k -i mycode.prof 

def plot_convergence(model, obj_values, test_cold_start_values=None, test_missing_data_values=None, SGD=False, directory='.', file_prefix=''):

    if not os.path.exists(directory):
        os.makedirs(directory)

    figure(1)
    clf()

    legend_list = []

    # Plot objective values
    semilogy(np.arange(1, len(obj_values) + 1) * obj_val_freq, obj_values)
    L1 = 'Objective'
    legend_list.append(L1)

    # Plot cold start error
    if test_cold_start_values is not None:
        semilogy(np.arange(1, len(test_cold_start_values) + 1) * obj_val_freq, test_cold_start_values)
        L2 = 'Cold Start Error'
        legend_list.append(L2)

    # Plot missing data error
    if test_missing_data_values is not None:
        semilogy(np.arange(1, len(test_missing_data_values) + 1) * obj_val_freq, test_missing_data_values)
        L3 = 'Missing Data Error'
        legend_list.append(L3)

    # Plot stuff
    legend(legend_list)
    xlabel('Iteration Number')
    title(model + ' Objective Values and Errors, ' + ('SGD' if SGD else 'Full GD'))

    savefig(directory + '/' + file_prefix + 'Objectives_and_Errors.pdf')

def plot_experiments(Y_train, phi_train, Y_cold_start, phi_cold_start, model, opt_vars, obj_values, Omega, completely_missing_Omega,
                     test_cold_start_values, test_missing_data_values, SGD=False, directory='.', file_prefix=''):

    plot_convergence(model, obj_values, test_cold_start_values, test_missing_data_values, SGD, directory, file_prefix)

    num_training_examples = Y_train.shape[1]
    num_cold_start_examples = Y_cold_start.shape[1]

    ###### Plots ######

    # Plot reconstructed training data (this includes missing data)
    fig = figure(2)
    clf()
    fig.suptitle('Reconstructed Training Data: {0}'.format(model))

    subplot(311)
    for i in range(num_training_examples):
        plot(Y_train[:,i])
    title('true Y')

    subplot(312)
    for i in range(num_training_examples):
        if model == 'reg':
            W, b = extract_variables(opt_vars, model)
            projection = W.dot( phi_train[:,i].toarray()[:,0] ) + b
        elif model == 'mf+reg':
            L, R, W, b = extract_variables(opt_vars, model)
            projection = L.dot(R[:,i]) + W.dot( phi_train[:,i].toarray()[:,0] ) + b
        elif model == 'LowRankReg':
            H, U, b = extract_variables(opt_vars, model)
            projection = H.dot(U).dot( phi_train[:,i].toarray()[:,0] ) + b
        elif model == 'mf+LowRankReg':
            L, R, H, U, b = extract_variables(opt_vars, model)
            projection = L.dot(R[:,i]) + H.dot(U).dot( phi_train[:,i].toarray()[:,0] ) + b
        plot(projection)
    title('Estimated Y')

    training_missing_data_mask = np.ones(Y_train.shape)
    test_missing_data_mask = np.zeros(Y_train.shape)
    for i in xrange(Y_train.shape[1]):
        if len(Omega[i]) > 0: # Omega contains test missing data
            training_missing_data_mask[Omega[i], i] = 0
            test_missing_data_mask[Omega[i], i] = 1
        if len(completely_missing_Omega[0][i]) > 0: # This data is completely missing
            training_missing_data_mask[completely_missing_Omega[0][i], i] = 0

    test_cold_start_mask = np.ones(Y_cold_start.shape)
    for i in xrange(Y_cold_start.shape[1]):
        if len(completely_missing_Omega[1][i]) > 0:
            test_cold_start_mask[completely_missing_Omega[1][i], i] = 0


    train_error = full_objective(opt_vars, [0 for _ in xrange(len(opt_vars) - 1)], Y_train, phi_train, training_missing_data_mask, model)
    print "Reconstruction Error of training data:", train_error

    subplot(313)
    for i in range(num_training_examples):
        if model == 'reg':
            W, b = extract_variables(opt_vars, model)
            projection = W.dot( phi_train[:,i].toarray()[:,0] ) + b
        elif model == 'mf+reg':
            L, R, W, b = extract_variables(opt_vars, model)
            projection = L.dot(R[:,i]) + W.dot( phi_train[:,i].toarray()[:,0] ) + b
        elif model == 'LowRankReg':
            H, U, b = extract_variables(opt_vars, model)
            projection = H.dot(U).dot( phi_train[:,i].toarray()[:,0] ) + b
        elif model == 'mf+LowRankReg':
            L, R, H, U, b = extract_variables(opt_vars, model)
            projection = L.dot(R[:,i]) + H.dot(U).dot( phi_train[:,i].toarray()[:,0] ) + b
        true = Y_train[:,i]
        plot( abs(true - projection) )
    title('Magnitude train error')

    subplots_adjust(hspace=0.4)
    savefig(directory + '/' + file_prefix + 'Reconstructed_Training_Data.pdf', bbox_inches='tight')



    # Plot cold-start
    fig = figure(3)
    clf()
    fig.suptitle('Cold Start Projections: {0}'.format(model))

    subplot(311)
    for i in range(num_cold_start_examples):
        plot(Y_cold_start[:,i])
    title('true cold-start Y')

    subplot(312)
    for i in range(num_cold_start_examples):
        if model == 'reg':
            W, b = extract_variables(opt_vars, model)
            projection = W.dot( phi_cold_start[:,i].toarray()[:,0] ) + b
        elif model == 'mf+reg':
            L, R, W, b = extract_variables(opt_vars, model)
            projection= W.dot( phi_cold_start[:,i].toarray()[:,0] ) + b
        elif model == 'LowRankReg':
            H, U, b = extract_variables(opt_vars, model)
            projection = H.dot(U).dot( phi_cold_start[:,i].toarray()[:,0] ) + b
        elif model == 'mf+LowRankReg':
            L, R, H, U, b = extract_variables(opt_vars, model)
            projection = H.dot(U).dot( phi_cold_start[:,i].toarray()[:,0] ) + b
        plot( projection )
    title('Estimated cold-start Y')

    test_cold_start = test_cold_start_error(opt_vars, Y_cold_start, phi_cold_start, test_cold_start_mask, model)
    test_missing_data = test_missing_data_error(opt_vars, Y_train, phi_train, test_missing_data_mask, model)

    subplot(313)
    for i in range(num_cold_start_examples):
        if model == 'reg':
            W, b = extract_variables(opt_vars, model)
            projection = W.dot( phi_cold_start[:,i].toarray()[:,0] ) + b
        elif model == 'mf+reg':
            L, R, W, b = extract_variables(opt_vars, model)
            projection = W.dot( phi_cold_start[:,i].toarray()[:,0] ) + b
        elif model == 'LowRankReg':
            H, U, b = extract_variables(opt_vars, model)
            projection = H.dot(U).dot( phi_cold_start[:,i].toarray()[:,0] ) + b
        elif model == 'mf+LowRankReg':
            L, R, H, U, b = extract_variables(opt_vars, model)
            projection = H.dot(U).dot( phi_cold_start[:,i].toarray()[:,0] ) + b   
        true = Y_cold_start[:,i]
        plot( abs(true - projection) )
    title('Magnitude test error')

    print "Cold Start Error Error of cold-start test data:", test_cold_start
    print "Missing Data Error Error of test data:", test_missing_data
    subplots_adjust(hspace=0.4)
    savefig(directory + '/' + file_prefix + 'Cold_Start_Reconstruction.pdf', bbox_inches='tight')

def generate_single_cold_start_plot(Y_cold_start, phi_cold_start, opt_vars, model, completely_missing_Omega, test_articles_indices, index_to_article, indices=None, pure_or_OOS="OOS"):

    T, N = Y_cold_start.shape
    # mask = np.zeros(T)
    # mask[completely_missing_Omega[1]] = 1

    print "Articles: "
    for index, article in zip(indices, [index_to_article[test_articles_indices[i]] for i in indices]):
        print str(index) + '\t' + article

    for fig_num, i in enumerate(indices):
        article = index_to_article[test_articles_indices[i]]

        fig = figure(fig_num+1)
        # fig.suptitle(article)

        # subplot(2, 1, 1)
        plot(Y_cold_start[:,i], color='magenta') # or red
        # title('Full time series')

        # subplot(4, 1, 2)
        # mask = np.zeros(T)
        # mask[completely_missing_Omega[1][i]] = 1
        # plot( ma.masked_array(Y_cold_start[:,i], mask=mask), color='red' )
        
        # title('Time series with missing data')
        # ylim([-1, 1])

        # subplot(4, 1, 3)
        if model == 'reg':
            W, b = extract_variables(opt_vars, model)
            projection = W.dot( phi_cold_start[:,i].toarray()[:,0] ) + b
        elif model == 'mf+reg':
            L, R, W, b = extract_variables(opt_vars, model)
            projection= W.dot( phi_cold_start[:,i].toarray()[:,0] ) + b
        elif model == 'LowRankReg':
            H, U, b = extract_variables(opt_vars, model)
            projection = H.dot(U).dot( phi_cold_start[:,i].toarray()[:,0] ) + b
        elif model == 'mf+LowRankReg':
            L, R, H, U, b = extract_variables(opt_vars, model)
            projection = H.dot(U).dot( phi_cold_start[:,i].toarray()[:,0] ) + b
            plot( projection, linewidth=1.5, color='green', alpha=0.8 )
            # ylim([-1, 1])
            # ylim([min(min(Y_cold_start[:,i]), min(projection)), max(max(Y_cold_start[:,i]), max(projection))])
        # title('Predicted time series')
        xlim([0, 365])
        small_eps = .2
        ylim([min(min(projection), min(Y_cold_start[:,i])) - small_eps, max(max(projection), max(Y_cold_start[:,i])) + small_eps])
        # if article == 'The Undertaker 2014': # hacked zoom in
        #     ylim([min(min(projection), min(Y_cold_start[:,i])) - small_eps, 5 + small_eps])
        locator_params(axis='y',nbins=3)
        locator_params(axis='x',nbins=5)

        # plot(Y_cold_start[:,i], color='magenta') # or red

        # subplot(2, 1, 2)
        # plot( projection, linewidth=1.25, color='green' )
        # title('Predicted time series zoomed in')
        # xlim([0, 365])
        # ylim([-.2, .2])
        locator_params(axis='y',nbins=3)
        locator_params(axis='x',nbins=5)


        subplots_adjust(hspace=0.2)

        savefig('Images/Real Data Experiments/Cold Start Predictions/{0}/'.format(pure_or_OOS) + article + '.pdf', bbox_inches='tight')

        close(fig)

    # show()

def generate_cold_start_plot(Y_cold_start, phi_cold_start, opt_vars, model, completely_missing_Omega, test_articles_indices, index_to_article, indices=None):

    np.random.seed(int(time()))

    # Select a random subset of the cold start time series to plot
    N = Y_cold_start.shape[1]
    if indices is None:
        indices = np.random.choice(N, 6, replace=False)

    print "Articles: "
    for index, article in zip(indices, [index_to_article[test_articles_indices[i]] for i in indices]):
        print str(index) + '\t' + article

    # Plot cold-start
    fig = figure(3)
    clf()
    fig.suptitle('Cold Start Projections: {0}'.format(model))

    subplot(211)
    for i in indices:
        plot(Y_cold_start[:,i])
    title('true cold-start Y')
    ylim([-1, 1])

    subplot(212)
    for i in indices:
        if model == 'reg':
            W, b = extract_variables(opt_vars, model)
            projection = W.dot( phi_cold_start[:,i].toarray()[:,0] ) + b
        elif model == 'mf+reg':
            L, R, W, b = extract_variables(opt_vars, model)
            projection= W.dot( phi_cold_start[:,i].toarray()[:,0] ) + b
        elif model == 'LowRankReg':
            H, U, b = extract_variables(opt_vars, model)
            projection = H.dot(U).dot( phi_cold_start[:,i].toarray()[:,0] ) + b
        elif model == 'mf+LowRankReg':
            L, R, H, U, b = extract_variables(opt_vars, model)
            projection = H.dot(U).dot( phi_cold_start[:,i].toarray()[:,0] ) + b
        if len(indices) == 1:
            subplot(211)
            plot( projection, linewidth=1.25, color='green', alpha=0.8 )
            ylim([-1, 1])
            subplot(212)
            plot( projection, linewidth=1.25, color='green' )
            title('Zoomed in: {0}'.format(index_to_article[test_articles_indices[i]]))    
        else:
            plot( projection )
            title('Estimated cold-start Y')

    """
    subplot(313)
    for i in indices:
        if model == 'reg':
            W, b = extract_variables(opt_vars, model)
            projection = W.dot( phi_cold_start[:,i].toarray()[:,0] ) + b
        elif model == 'mf+reg':
            L, R, W, b = extract_variables(opt_vars, model)
            projection = W.dot( phi_cold_start[:,i].toarray()[:,0] ) + b
        elif model == 'LowRankReg':
            H, U, b = extract_variables(opt_vars, model)
            projection = H.dot(U).dot( phi_cold_start[:,i].toarray()[:,0] ) + b
        elif model == 'mf+LowRankReg':
            L, R, H, U, b = extract_variables(opt_vars, model)
            projection = H.dot(U).dot( phi_cold_start[:,i].toarray()[:,0] ) + b   
        true = Y_cold_start[:,i]
        if len(indices) == 1:
            plot( abs(true - projection), color='red' )
        else:
            plot( abs(true - projection) )            
    title('Magnitude error')
    ylim([0, 1])
    """

    subplots_adjust(hspace=0.4)
    # savefig(directory + '/' + file_prefix + 'Cold_Start_Reconstruction.pdf', bbox_inches='tight')
    show()





def parallel_cold_start_learning(Y_train, phi_train, k_in, model, Y_cold_start, phi_cold_start, Omega_train, completely_missing_Omega, lambda1, lambda2, 
                                 initial_opt_vars, max_iters, SGD, delay, minibatch_size, eps, verbose, smart_init_bool, seed, process_num):

    print "Starting process: {0}".format(process_num)
    np.random.seed(seed)

    # Run smart init if specified
    if smart_init_bool:
        smart_init = smart_initialization(Y_train, phi_train, k_in, model, Omega_train=Omega_train, completely_missing_Omega=completely_missing_Omega, lambda1=lambda1, lambda2=lambda2, init_opt_vars=initial_opt_vars, SGD=SGD, minibatch_size=minibatch_size)
    else:
        smart_init = initial_opt_vars

    opt_vars, obj_values, test_cold_start_values, test_missing_data_values = cold_start_learning(Y_train, phi_train, k_in, model, Y_cold_start=Y_cold_start, phi_cold_start=phi_cold_start, 
                                                                                                 Omega_train=Omega_train, completely_missing_Omega=completely_missing_Omega, 
                                                                                                 lambda1=lambda1, lambda2=lambda2, initial_opt_vars=smart_init, max_iters=max_iters, 
                                                                                                 SGD=SGD, delay=delay, minibatch_size=minibatch_size, eps=eps, verbose=verbose, process_num=process_num)
    # Hit stopping criterion. Fill out rest of arrays with last value
    print "Finished process: {0}".format(process_num)
    if len(obj_values) * obj_val_freq < max_iters: 
        print "Process terminated early! Hit stopping criterion"
        obj_values += [obj_values[-1]] * ((max_iters - len(obj_values) * obj_val_freq) / obj_val_freq)
        test_cold_start_values += [test_cold_start_values[-1]] * ((max_iters - len(test_cold_start_values) * obj_val_freq) / obj_val_freq)
        test_missing_data_values += [test_missing_data_values[-1]] * ((max_iters - len(test_missing_data_values) * obj_val_freq) / obj_val_freq)
    return opt_vars, obj_values, test_cold_start_values, test_missing_data_values


def cold_start_CV_folds(T, N_train, K):
    """ Returns indices of cold start """

    shuffled_indices = np.random.permutation(N_train)
    return np.array_split(shuffled_indices, K)



def cross_validation(Y, phi, k_in, max_iters, minibatch_size, missing_data_experiments, CV_folds, test_Omega, directory='.', verbose=True, model_flags=[1,1,1,1]):
    """ K-fold cross validation on all four models 
        Use GD for regression, SGD with minibatches for all other models

        test_Omega: 
            - What is it? 2 lists of numpy arrays
            - for missing data experiments:
                - This is used so that the CV method never touches the test dataset.
            - for cold start experiments:
                - This is used so that the CV method never touches the COMPLETELY missing data

    """

    if verbose:
        print "Process starting cross validation..."

    # Set lambda values for cross validation
    lambda1_vals = np.logspace(-2, 3, 10)
    lambda2_vals = np.logspace(-2, 3, 10)

    # K = len(CV_folds) 
    K = 1 # for real data

    T, N = Y.shape




    ################## REG ##################

    if model_flags[0]:
        # Cross validate on lambda1 only
        model = 'reg'
        if verbose:
            print "Setting up arguments for cross validation on model: reg"
        cold_start_args = [0 for i in xrange(K * len(lambda1_vals))]
        index = 0
        for lambda1 in lambda1_vals:
            for k in range(K):

                if missing_data_experiments: # No need to split up training data into Y_train, Y_cold_start
                    Y_train = Y
                    phi_train = phi
                    Y_cold_start = None
                    phi_cold_start = None
                else: # cold start experiments
                    Y_train = np.delete(Y, CV_folds[k], axis=1)
                    Y_cold_start = Y[:, CV_folds[k]]

                    all_cols = xrange(phi.shape[1])
                    mask = np.in1d(all_cols, CV_folds[k], invert=True)
                    phi_train = phi[:, mask]
                    phi_cold_start = phi[:, CV_folds[k]]

                    completely_missing_Omega_train = np.delete(test_Omega[0], CV_folds[k], axis=0)
                    completely_missing_Omega_val = np.array(test_Omega[0])[CV_folds[k]]
                    completely_missing_Omega = [completely_missing_Omega_train, completely_missing_Omega_val]

                # Temporary container for cold start args
                temp = []

                # Args: Y_train, phi_train, k_in, model, Y_cold_start, phi_cold_start, Omega_train, completely_missing_Omega, lambda1, lambda2
                #       initial_opt_vars, max_iters, SGD, delay, minibatch_size, eps, verbose, smart_init_bool, seed, process_num
                temp.append(Y_train)        # Y_train
                temp.append(phi_train)      # phi_train
                temp.append(k_in)           # k_in
                temp.append(model)          # model
                temp.append(Y_cold_start)   # Y_cold_start
                temp.append(phi_cold_start) # phi_cold_start

                # Omega_train, completely missing Omega (for test set)
                if missing_data_experiments:
                    temp.append(CV_folds[k])
                    temp.append(test_Omega)    
                else: # cold start experiments
                    temp.append(None)
                    temp.append(completely_missing_Omega)

                temp.append(lambda1)        # lambda1
                temp.append(52)             # lambda2: doesn't matter for reg, set this to whatever
                temp.append(None)           # initial_opt_vars
                temp.append(1500)           # max_iters
                temp.append(False)          # SGD: don't use this for pure reg
                temp.append(None)           # delay
                temp.append(52)             # minibatch_size: doesn't matter for reg, set this to whatever
                temp.append(1e-3)           # eps
                temp.append(False)          # verbose
                temp.append(False)          # smart_init_bool
                temp.append(randint(1e7))   # thread seed
                temp.append(index+1)          # process_num

                # Add to cold start args
                cold_start_args[index] = temp
                index += 1

        # Parallelize over lambdas and folds
        all_runs_obj_values = grid_map(parallel_cold_start_learning, cold_start_args, quiet=False, max_processes=10, local=True, queue='all.q')

        # Find the lambda that gives best average generalization error
        best_cost = np.inf
        best_lambda1 = 0
        best_index = 0 # Not actually best index, just last index of best setting of lambdas
        index = 0
        for lambda1 in lambda1_vals:

            # Average over the lambda runs
            total_validation_MSE = 0
            for k in range(K):
            
                if missing_data_experiments:
                    total_validation_MSE += all_runs_obj_values[index][3][-1]
                else: # cold start experiments
                    total_validation_MSE += all_runs_obj_values[index][2][-1]
                index += 1
            
            total_validation_MSE /= float(K)
            if verbose:
                print "Average validation error for lambda1 = %10.5f, %15.7f" % (lambda1, total_validation_MSE)

            if total_validation_MSE < best_cost:
                best_cost = total_validation_MSE
                best_lambda1 = lambda1
                best_index = index - 1

        if verbose:
            print "Best lambda_1 found: {0}".format(best_lambda1)
            print "Best cost on validation MSE: {0}".format(best_cost)

        # Plot a objective value plot from this lambda
        # model, obj_values, test_cold_start_values=None, test_missing_data_values=None, SGD=False, directory='.', file_prefix=''
        if missing_data_experiments:
            plot_convergence(model, all_runs_obj_values[best_index][1], None, all_runs_obj_values[best_index][3], False, directory, file_prefix=model+'_')
        else:
            plot_convergence(model, all_runs_obj_values[best_index][1], all_runs_obj_values[best_index][2], None, False, directory, file_prefix=model+'_')

        reg_lambda_1 = best_lambda1




    ################## MF + REG ##################

    if model_flags[1]:
        # Grid search on lambda1 and lambda2
        model = 'mf+reg'
        if verbose:
            print "Setting up arguments for cross validation on model: mf+reg"
        # cold_start_args = [ 0 for i in xrange(K * len(lambda1_vals) * len(lambda2_vals)) ]
        cold_start_args = [ 0 for i in xrange(K * len(lambda2_vals)) ]
        index = 0
        # for lambda1 in lambda1_vals:
        for lambda2 in lambda2_vals:
            for k in range(K):
               
                if missing_data_experiments: # No need to split up training data into Y_train, Y_cold_start
                    Y_train = Y
                    phi_train = phi
                    Y_cold_start = None
                    phi_cold_start = None
                else: # cold start experiments
                    Y_train = np.delete(Y, CV_folds[k], axis=1)
                    Y_cold_start = Y[:, CV_folds[k]]

                    all_cols = xrange(phi.shape[1])
                    mask = np.in1d(all_cols, CV_folds[k], invert=True)
                    phi_train = phi[:, mask]
                    phi_cold_start = phi[:, CV_folds[k]]
                    
                    completely_missing_Omega_train = np.delete(test_Omega[0], CV_folds[k], axis=0)
                    completely_missing_Omega_val = np.array(test_Omega[0])[CV_folds[k]]
                    completely_missing_Omega = [completely_missing_Omega_train, completely_missing_Omega_val]

                # Temporary container for cold start args
                temp = []

                # Args: Y_train, phi_train, k_in, model, Y_cold_start, phi_cold_start, Omega_train, completely_missing_Omega, lambda1, lambda2
                #       initial_opt_vars, max_iters, SGD, delay, minibatch_size, eps, verbose, smart_init_bool, seed, process_num
                temp.append(Y_train)        # Y_train
                temp.append(phi_train)      # phi_train
                temp.append(k_in)           # k_in
                temp.append(model)          # model
                temp.append(Y_cold_start)   # Y_cold_start
                temp.append(phi_cold_start) # phi_cold_start

                # Omega_train, completely missing Omega (for test set)
                if missing_data_experiments:
                    temp.append(CV_folds[k])
                    temp.append(test_Omega)    
                else: # cold start experiments
                    temp.append(None)
                    temp.append(completely_missing_Omega)

                temp.append(reg_lambda_1)        # lambda1
                temp.append(lambda2)        # lambda2
                temp.append(None)           # initial_opt_vars
                temp.append(max_iters)      # max_iters
                temp.append(True)           # SGD 
                temp.append(['fixed', 0.1])           # delay
                temp.append(minibatch_size) # minibatch_size
                temp.append(1e-10)           # eps
                temp.append(False)          # verbose
                temp.append(False)           # smart_init_bool
                temp.append(randint(1e7))   # thread seed
                temp.append(index+1)          # process_num

                # Add to cold start args
                cold_start_args[index] = temp
                index += 1

        # Parallelize over lambdas and folds
        all_runs_obj_values = grid_map(parallel_cold_start_learning, cold_start_args, quiet=False, max_processes=10, local=True, queue='all.q')

        # Find the lambda that gives best average generalization error
        best_cost = np.inf
        best_lambda1 = 0
        best_lambda2 = 0
        best_index = 0
        index = 0
        # for lambda1 in lambda1_vals:
        for lambda2 in lambda2_vals:

            # Average over the lambda runs
            total_validation_MSE = 0
            
            for k in range(K):
            
                if missing_data_experiments:
                    total_validation_MSE += all_runs_obj_values[index][3][-1]
                else: # cold start experiments
                    total_validation_MSE += all_runs_obj_values[index][2][-1]
                index += 1

            total_validation_MSE /= float(K)
            if verbose:
                # print "Average validation error for lambda1 = %10.5f, lambda2 = %10.5f: %15.7f" % (lambda1, lambda2, total_validation_MSE)
                print "Average validation error for lambda1 = %10.5f, lambda2 = %10.5f: %15.7f" % (reg_lambda_1, lambda2, total_validation_MSE)

            if total_validation_MSE < best_cost:
                best_cost = total_validation_MSE
                best_lambda1 = reg_lambda_1 # lambda1
                best_lambda2 = lambda2
                best_index = index - 1

        if verbose:
            print "Best lambda_1 found: {0}".format(best_lambda1)
            print "Best lambda_2 found: {0}".format(best_lambda2)
            print "Best cost on validation MSE: {0}".format(best_cost)

        # Plot a objective value plot from this lambda
        # model, obj_values, test_cold_start_values=None, test_missing_data_values=None, SGD=False, directory='.', file_prefix=''
        if missing_data_experiments:
            plot_convergence(model, all_runs_obj_values[best_index][1], None, all_runs_obj_values[best_index][3], True, directory, file_prefix=model+'_')
        else:
            plot_convergence(model, all_runs_obj_values[best_index][1], all_runs_obj_values[best_index][2], None, True, directory, file_prefix=model+'_')

        mf_reg_lambda_1 = best_lambda1
        mf_reg_lambda_2 = best_lambda2




    ################## LOW RANK REG ##################

    if model_flags[2]:
        # Cross validate on lambda1 only
        model = 'LowRankReg'
        if verbose:
            print "Setting up arguments for cross validation on model: LowRankReg"
        cold_start_args = [0 for i in xrange(K * len(lambda1_vals))]
        index = 0
        for lambda1 in lambda1_vals:
            for k in range(K):
               
                if missing_data_experiments: # No need to split up training data into Y_train, Y_cold_start
                    Y_train = Y
                    phi_train = phi
                    Y_cold_start = None
                    phi_cold_start = None
                else: # cold start experiments
                    Y_train = np.delete(Y, CV_folds[k], axis=1)
                    Y_cold_start = Y[:, CV_folds[k]]

                    all_cols = xrange(phi.shape[1])
                    mask = np.in1d(all_cols, CV_folds[k], invert=True)
                    phi_train = phi[:, mask]
                    phi_cold_start = phi[:, CV_folds[k]]
                    
                    completely_missing_Omega_train = np.delete(test_Omega[0], CV_folds[k], axis=0)
                    completely_missing_Omega_val = np.array(test_Omega[0])[CV_folds[k]]
                    completely_missing_Omega = [completely_missing_Omega_train, completely_missing_Omega_val]
                # Temporary container for cold start args
                temp = []

                # Args: Y_train, phi_train, k_in, model, Y_cold_start, phi_cold_start, Omega_train, completely_missing_Omega, lambda1, lambda2
                #       initial_opt_vars, max_iters, SGD, delay, minibatch_size, eps, verbose, smart_init_bool, seed, process_num
                temp.append(Y_train)        # Y_train
                temp.append(phi_train)      # phi_train
                temp.append(k_in)           # k_in
                temp.append(model)          # model
                temp.append(Y_cold_start)   # Y_cold_start
                temp.append(phi_cold_start) # Y_cold_start

                # Omega_train, completely missing Omega (for test set)
                if missing_data_experiments:
                    temp.append(CV_folds[k])
                    temp.append(test_Omega)    
                else: # cold start experiments
                    temp.append(None)
                    temp.append(completely_missing_Omega)

                temp.append(lambda1)        # lambda1
                temp.append(52)             # lambda2: doesn't matter, set this to whatever
                temp.append(None)           # initial_opt_vars
                temp.append(max_iters)      # max_iters
                temp.append(True)           # SGD
                temp.append(['fixed', 0.15])           # delay
                temp.append(minibatch_size) # minibatch_size
                temp.append(1e-8)           # eps
                temp.append(False)          # verbose
                temp.append(False)          # smart_init_bool
                temp.append(randint(1e7))   # thread seed
                temp.append(index+1)          # process_num

                # Add to cold start args
                cold_start_args[index] = temp
                index += 1

        # Parallelize over lambdas and folds
        all_runs_obj_values = grid_map(parallel_cold_start_learning, cold_start_args, quiet=False, max_processes=10, local=True, queue='all.q')

        # Find the lambda that gives best average generalization error
        best_cost = np.inf
        best_lambda1 = 0
        best_index = 0
        index = 0
        for lambda1 in lambda1_vals:
            # Average over the lambda runs
            total_validation_MSE = 0
            for k in range(K):
            
                if missing_data_experiments:
                    total_validation_MSE += all_runs_obj_values[index][3][-1]
                else: # cold start experiments
                    total_validation_MSE += all_runs_obj_values[index][2][-1]
                index += 1

            total_validation_MSE /= float(K)
            if verbose:
                print "Average validation error for lambda1 = %10.5f, %15.7f" % (lambda1, total_validation_MSE)

            if total_validation_MSE < best_cost:
                best_cost = total_validation_MSE
                best_lambda1 = lambda1
                best_index = index - 1

        if verbose:
            print "Best lambda_1 found: {0}".format(best_lambda1)
            print "Best cost on validation MSE: {0}".format(best_cost)

        # Plot a objective value plot from this lambda
        # model, obj_values, test_cold_start_values=None, test_missing_data_values=None, SGD=False, directory='.', file_prefix=''
        if missing_data_experiments:
            plot_convergence(model, all_runs_obj_values[best_index][1], None, all_runs_obj_values[best_index][3], True, directory, file_prefix=model+'_')
        else:
            plot_convergence(model, all_runs_obj_values[best_index][1], all_runs_obj_values[best_index][2], None, True, directory, file_prefix=model+'_')

        lowrankreg_lambda_1 = best_lambda1




    ################## MF + LOW RANK REG ##################

    if model_flags[3]:
        # Cross validate on lambda2 only
        model = 'mf+LowRankReg'
        if verbose:
            print "Setting up arguments for cross validation on model: mf+LowRankReg"
        # cold_start_args = [ 0 for i in xrange(K * len(lambda1_vals) * len(lambda2_vals)) ]
        cold_start_args = [ 0 for i in xrange(K * len(lambda2_vals)) ]
        index = 0
        # for lambda1 in lambda1_vals:
        for lambda2 in lambda2_vals:
            for k in range(K):
               
                if missing_data_experiments: # No need to split up training data into Y_train, Y_cold_start
                    Y_train = Y
                    phi_train = phi
                    Y_cold_start = None
                    phi_cold_start = None
                else: # cold start experiments
                    Y_train = np.delete(Y, CV_folds[k], axis=1)
                    Y_cold_start = Y[:, CV_folds[k]]

                    all_cols = xrange(phi.shape[1])
                    mask = np.in1d(all_cols, CV_folds[k], invert=True)
                    phi_train = phi[:, mask]
                    phi_cold_start = phi[:, CV_folds[k]]
                    
                    completely_missing_Omega_train = np.delete(test_Omega[0], CV_folds[k], axis=0)
                    completely_missing_Omega_val = np.array(test_Omega[0])[CV_folds[k]]
                    completely_missing_Omega = [completely_missing_Omega_train, completely_missing_Omega_val]

                # Temporary container for cold start args
                temp = []

                # Args: Y_train, phi_train, k_in, model, Y_cold_start, phi_cold_start, Omega_train, completely_missing_Omega, lambda1, lambda2
                #       initial_opt_vars, max_iters, SGD, delay, minibatch_size, eps, verbose, smart_init_bool, seed, process_num
                temp.append(Y_train)        # Y_train
                temp.append(phi_train)      # phi_train
                temp.append(k_in)           # k_in
                temp.append(model)          # model
                temp.append(Y_cold_start)   # Y_cold_start
                temp.append(phi_cold_start) # phi_cold_start

                # Omega_train, completely missing Omega (for test set)
                if missing_data_experiments:
                    temp.append(CV_folds[k])
                    temp.append(test_Omega)    
                else: # cold start experiments
                    temp.append(None)
                    temp.append(completely_missing_Omega)

                temp.append(lowrankreg_lambda_1)        # lambda1
                temp.append(lambda2)        # lambda2
                temp.append(None)           # initial_opt_vars
                temp.append(max_iters)      # max_iters
                temp.append(True)           # SGD
                temp.append(['fixed', 0.15])           # delay
                temp.append(minibatch_size) # minibatch_size
                temp.append(1e-8)           # eps
                temp.append(False)          # verbose
                temp.append(False)           # smart_init_bool
                temp.append(randint(1e7))   # thread seed
                temp.append(index+1)          # process_num

                # Add to cold start args
                cold_start_args[index] = temp
                index += 1

        # Parallelize over lambdas and folds
        all_runs_obj_values = grid_map(parallel_cold_start_learning, cold_start_args, quiet=False, max_processes=10, local=True, queue='all.q')

        # Find the lambda that gives best average generalization error
        best_cost = np.inf
        best_lambda1 = 0
        best_lambda2 = 0
        best_index = 0
        index = 0
        # for lambda1 in lambda1_vals:
        for lambda2 in lambda2_vals:

            # Average over the lambda runs
            total_validation_MSE = 0            
            for k in range(K):
            
                if missing_data_experiments:
                    total_validation_MSE += all_runs_obj_values[index][3][-1]
                else: # cold start experiments
                    total_validation_MSE += all_runs_obj_values[index][2][-1]
                index += 1

            total_validation_MSE /= float(K)
            if verbose:
                # print "Average validation error for lambda1 = %10.5f, lambda2 = %10.5f: %15.7f" % (lambda1, lambda2, total_validation_MSE)
                print "Average validation error for lambda1 = %10.5f, lambda2 = %10.5f: %15.7f" % (lowrankreg_lambda_1, lambda2, total_validation_MSE)

            if total_validation_MSE < best_cost:
                best_cost = total_validation_MSE
                best_lambda1 = lowrankreg_lambda_1 # lambda1
                best_lambda2 = lambda2
                best_index = index - 1

        if verbose:
            print "Best lambda_1 found: {0}".format(best_lambda1)
            print "Best lambda_2 found: {0}".format(best_lambda2)
            print "Best cost on validation MSE: {0}".format(best_cost)

        # Plot a objective value plot from this lambda
        # model, obj_values, test_cold_start_values=None, test_missing_data_values=None, SGD=False, directory='.', file_prefix=''
        if missing_data_experiments:
            plot_convergence(model, all_runs_obj_values[best_index][1], None, all_runs_obj_values[best_index][3], True, directory, file_prefix=model+'_')
        else:
            plot_convergence(model, all_runs_obj_values[best_index][1], all_runs_obj_values[best_index][2], None, True, directory, file_prefix=model+'_')

        mf_lowrankreg_lambda_1 = best_lambda1
        mf_lowrankreg_lambda_2 = best_lambda2



    return_list = []
    return_list.append(reg_lambda_1 if model_flags[0] else 0)
    return_list.append((mf_reg_lambda_1, mf_reg_lambda_2) if model_flags[1] else (0,0))
    return_list.append(lowrankreg_lambda_1 if model_flags[2] else 0)
    return_list.append((mf_lowrankreg_lambda_1, mf_lowrankreg_lambda_2) if model_flags[3] else (0,0))

    return return_list

def synthetic_data_single_experiment():
    """ This function is for running a single experiment
    """

    args = sys.argv[1:]

    ###### Generate the synthetic data #######

    # Seed the randomness
    np.random.seed(5)
    # np.random.seed(int(time()))

    T = 300                 # Length of each univariate time series
    N = 5000                  # Number of generated time series (some will be save for cold start forecasting)
    m = 1000                  # Length of metadata vector
    k = 20                   # True number of latent factors
    Y, phi, L, R, H, U = gen_series(T,N,m,k)
    phi = scipy.sparse.csc_matrix(phi)

    # Eventually cross-validation
    num_series_used = N - 50    # Number of univariate time series used. N - num_series_used are saved for cold start forecasting
    print "Number of series used: {0}".format(num_series_used)
    rand_ind = np.random.permutation(N)
    ind_train = rand_ind[:num_series_used]
    ind_cold_start = rand_ind[num_series_used:]

    Y_train = Y[:, ind_train]
    Y_cold_start = Y[:, ind_cold_start]
    phi_train = phi[:, ind_train]
    phi_cold_start = phi[:, ind_cold_start]

    ###### Run algorithm ######
    start_time = time()
    k_in = k
    model = 'mf+LowRankReg' 
    print "Using model: {0}".format(model)

    max_iters = 3000
    SGD = True
    delay = ['fixed', 1.0]
    minibatch_size = 300
    eps = 1e-8
    verbose = True
    lambda1 = 1.
    lambda2 = 1.
    completely_missing_Omega = gen_completely_missing_Omega(T, Y_train.shape[1], Y_cold_start.shape[1], .2) # Unobservable by learning algorithm
    Omega_train = gen_Omega(T, Y_train.shape[1], completely_missing_Omega, .2) # (indices of) validation set 

    # smart_init = smart_initialization(Y_train, phi_train, k_in, model, Omega_train=Omega_train, completely_missing_Omega=completely_missing_Omega, lambda1=lambda1, lambda2=lambda2, init_opt_vars=None, SGD=SGD, minibatch_size=minibatch_size)
    smart_init = None
    opt_vars, obj_values, test_cold_start_values, test_missing_data_values = cold_start_learning(Y_train, phi_train, k_in, model, Y_cold_start, phi_cold_start, Omega_train, completely_missing_Omega, lambda1=lambda1, lambda2=lambda2,
                                                                             initial_opt_vars=smart_init, max_iters=max_iters, SGD=SGD, delay=delay, minibatch_size=minibatch_size, eps=eps, verbose=verbose)

    end_time = time()
    print "Time take to run algorithm: {0} seconds".format(round(end_time - start_time, 3))


    # Plot everything. Set y limits to put stuff on same scale
    if len(args) > 0:
        directory = args[0]
    else:
        directory = 'test/synthetic/single_run'
    if not os.path.exists(directory):
        os.makedirs(directory)
    # plot_experiments(Y_train, phi_train, Y_cold_start, phi_cold_start, model, opt_vars, obj_values, Omega_train, completely_missing_Omega,
    #                  test_cold_start_values, test_missing_data_values, SGD=SGD, directory=directory, file_prefix=model+'_')
    plot_convergence(model, obj_values, test_cold_start_values, test_missing_data_values, SGD=SGD, directory=directory, file_prefix=model+'_')


def synthetic_data_every_model_experiment(Y_train, phi_train, k_in, Y_cold_start, phi_cold_start, Omega_train, completely_missing_Omega, missing_data_experiments, pure_or_OOS,
                                          reg_lambda_1, mf_reg_lambda_1, mf_reg_lambda_2, lowrankreg_lambda_1, mf_lowrankreg_lambda_1, mf_lowrankreg_lambda_2, directory=None):
    """ This function is for training every model with the hyperparameters found by cross validation
        on synthetic data
    """

    if directory is None:
        directory = 'test/synthetic/train_after_CV'
    else:
        directory += '/train_after_CV'
    if not os.path.exists(directory):
        os.makedirs(directory)

    start_time = time()
    num_random_restarts = 10

    # First run reg and low rank reg in parallel
    cold_start_args = []

    # Reg (Just run once)
    # Args: Y_train, phi_train, k_in, model, Y_cold_start, phi_cold_start, Omega_train, completely_missing_Omega, 
    #       lambda1, lambda2, initial_opt_vars, max_iters, SGD, delay, minibatch_size, eps, verbose, smart_init_bool, seed, process_num
    cold_start_args.append( [Y_train, phi_train, k_in, 'reg', Y_cold_start, phi_cold_start, Omega_train, completely_missing_Omega, 
                             reg_lambda_1, 52, None, 10000, False, None, 52, 1e-3, False, False, randint(1e7), 1] )

    # Low Rank Reg
    for number in range(num_random_restarts):
        # Args: Y_train, phi_train, k_in, model, Y_cold_start, phi_cold_start, Omega_train, completely_missing_Omega, 
        #       lambda1, lambda2, initial_opt_vars, max_iters, SGD, delay, minibatch_size, eps, verbose, smart_init_bool, seed, process_num
        cold_start_args.append( [Y_train, phi_train, k_in, 'LowRankReg', Y_cold_start, phi_cold_start, Omega_train, completely_missing_Omega, 
                                 lowrankreg_lambda_1, 52, None, 50000, True, ['fixed', 1.0], 300, 1e-8, False, False, randint(1e7), 2] )    

    all_runs_obj_values = grid_map(parallel_cold_start_learning, cold_start_args, quiet=False, max_processes=10, local=True, queue='all.q')    

    # Find the best run for Reg model
    best_reg_model_opt_vars = all_runs_obj_values[0][0]
    best_reg_train_objs = all_runs_obj_values[0][1]
    best_reg_test_cold_start_values = all_runs_obj_values[0][2]
    best_reg_test_missing_data_values = all_runs_obj_values[0][3]

    # Find the best run for Low Rank Reg model
    best_lowrankreg_index = 1 + np.argmin([temp[1][-1] for temp in all_runs_obj_values[1:]]) # + 1 because the first is the reg run
    best_lowrankreg_model_opt_vars = all_runs_obj_values[best_lowrankreg_index][0]
    best_lowrankreg_train_objs = all_runs_obj_values[best_lowrankreg_index][1]
    best_lowrankreg_test_cold_start_values = all_runs_obj_values[best_lowrankreg_index][2]
    best_lowrankreg_test_missing_data_values = all_runs_obj_values[best_lowrankreg_index][3]

    # model, obj_values, test_cold_start_values=None, test_missing_data_values=None, SGD=False, directory='.', file_prefix=''
    if missing_data_experiments:
        plot_convergence('reg', best_reg_train_objs, None, best_reg_test_missing_data_values, False, directory, file_prefix='reg'+'_')
        plot_convergence('LowRankReg', best_lowrankreg_train_objs, None, best_lowrankreg_test_missing_data_values, True, directory, file_prefix='LowRankReg'+'_')
    else:
        plot_convergence('reg', best_reg_train_objs, best_reg_test_cold_start_values, None, False, directory, file_prefix='reg'+'_')
        plot_convergence('LowRankReg', best_lowrankreg_train_objs, best_lowrankreg_test_cold_start_values, None, True, directory, file_prefix='LowRankReg'+'_')



    # Next, run matrix factorization methods in parallel
    cold_start_args = []

    # mf+reg
    for number in range(num_random_restarts):
        # Args: Y_train, phi_train, k_in, model, Y_cold_start, phi_cold_start, Omega_train, completely_missing_Omega, 
        #       lambda1, lambda2, initial_opt_vars, max_iters, SGD, delay, minibatch_size, eps, verbose, smart_init_bool, seed, process_num
        cold_start_args.append( [Y_train, phi_train, k_in, 'mf+reg', Y_cold_start, phi_cold_start, Omega_train, completely_missing_Omega, 
                                 # mf_reg_lambda_1, mf_reg_lambda_2, top_model_opt_vars[0], 50000, True, None, 30, 1e-5, False, True, randint(1e7)] )
                                 mf_reg_lambda_1, mf_reg_lambda_2, None, 50000, True, ['fixed', 1.0], 300, 1e-8, False, False, randint(1e7), 1] )

    # mf+LowRankReg        
    for number in range(num_random_restarts):
        # Args: Y_train, phi_train, k_in, model, Y_cold_start, phi_cold_start, Omega_train, completely_missing_Omega, 
        #       lambda1, lambda2, initial_opt_vars, max_iters, SGD, delay, minibatch_size, eps, verbose, smart_init_bool, seed, process_num
        cold_start_args.append( [Y_train, phi_train, k_in, 'mf+LowRankReg', Y_cold_start, phi_cold_start, Omega_train, completely_missing_Omega,
                             # mf_lowrankreg_lambda_1, mf_lowrankreg_lambda_2, top_model_opt_vars[1], 50000, True, None, 30, 1e-5, False, True, randint(1e7)] )    
                             mf_lowrankreg_lambda_1, mf_lowrankreg_lambda_2, None, 50000, True, ['fixed', 1.0], 300, 1e-8, False, False, randint(1e7), 2] )    

    all_runs_obj_values = grid_map(parallel_cold_start_learning, cold_start_args, quiet=False, max_processes=10, local=True, queue='all.q')

    # Find the best run for MF + Reg model
    best_mfreg_index = np.argmin([temp[1][-1] for temp in all_runs_obj_values[:num_random_restarts]])
    best_mfreg_model_opt_vars = all_runs_obj_values[best_mfreg_index][0]
    best_mfreg_train_objs = all_runs_obj_values[best_mfreg_index][1]
    best_mfreg_test_cold_start_values = all_runs_obj_values[best_mfreg_index][2]
    best_mfreg_test_missing_data_values = all_runs_obj_values[best_mfreg_index][3]

    # Find the best run for MF + Low Rank Reg model
    best_mflowrankreg_index = num_random_restarts + np.argmin([temp[1][-1] for temp in all_runs_obj_values[num_random_restarts:]])
    best_mflowrankreg_model_opt_vars = all_runs_obj_values[best_mflowrankreg_index][0]
    best_mflowrankreg_train_objs = all_runs_obj_values[best_mflowrankreg_index][1]
    best_mflowrankreg_test_cold_start_values = all_runs_obj_values[best_mflowrankreg_index][2]
    best_mflowrankreg_test_missing_data_values = all_runs_obj_values[best_mflowrankreg_index][3]

    if missing_data_experiments:
        plot_convergence('mf+reg', best_mfreg_train_objs, None, best_mfreg_test_missing_data_values, True, directory, file_prefix='mf+reg'+'_')
        plot_convergence('mf+LowRankReg', best_mflowrankreg_train_objs, None, best_mflowrankreg_test_missing_data_values, True, directory, file_prefix='mf+LowRankReg'+'_')
    else:
        plot_convergence('mf+reg', best_mfreg_train_objs, best_mfreg_test_cold_start_values, None, True, directory, file_prefix='mf+reg'+'_')
        plot_convergence('mf+LowRankReg', best_mflowrankreg_train_objs, best_mflowrankreg_test_cold_start_values, None, True, directory, file_prefix='mf+LowRankReg'+'_')

    ### SAVE MODELS ###

    # Save best Reg model
    do = Dump_Object(best_reg_model_opt_vars, best_reg_train_objs, best_reg_test_cold_start_values, best_reg_test_missing_data_values)
    pickle.dump(do, open('opt_dumps/Synth Reg {0} Model.pkl'.format(pure_or_OOS), 'wb'))

    # Save best MF + Reg model
    do = Dump_Object(best_mfreg_model_opt_vars, best_mfreg_train_objs, best_mfreg_test_cold_start_values, best_mfreg_test_missing_data_values)
    pickle.dump(do, open('opt_dumps/Synth MF+Reg {0} Model.pkl'.format(pure_or_OOS), 'wb'))

    # Save best Low Rank Reg model
    do = Dump_Object(best_lowrankreg_model_opt_vars, best_lowrankreg_train_objs, best_lowrankreg_test_cold_start_values, best_lowrankreg_test_missing_data_values)
    pickle.dump(do, open('opt_dumps/Synth LowRankReg {0} Model.pkl'.format(pure_or_OOS), 'wb'))

    # Save best MF + Low Rank Reg model
    do = Dump_Object(best_mflowrankreg_model_opt_vars, best_mflowrankreg_train_objs, best_mflowrankreg_test_cold_start_values, best_mflowrankreg_test_missing_data_values)
    pickle.dump(do, open('opt_dumps/Synth MF+LowRankReg {0} Model.pkl'.format(pure_or_OOS), 'wb'))

    end_time = time()
    print "Time take to run algorithm: {0} seconds".format(round(end_time - start_time, 3))

    print "Training RSS:"
    print "%20s: %10.10f" % ("reg", best_reg_train_objs[-1])
    print "%20s: %10.10f" % ("mf+reg", best_mfreg_train_objs[-1])
    print "%20s: %10.10f" % ("LowRankReg", best_lowrankreg_train_objs[-1])
    print "%20s: %10.10f" % ("mf+LowRankReg", best_mflowrankreg_train_objs[-1])

    if missing_data_experiments:
        print "Test Missing Data RSS:"
        print "%20s: %10.10f" % ("reg", best_reg_test_missing_data_values[-1])
        print "%20s: %10.10f" % ("mf+reg", best_mfreg_test_missing_data_values[-1])
        print "%20s: %10.10f" % ("LowRankReg", best_lowrankreg_test_missing_data_values[-1])
        print "%20s: %10.10f" % ("mf+LowRankReg", best_mflowrankreg_test_missing_data_values[-1])
    else:
        print "Test Cold Start RSS:"
        print "%20s: %10.10f" % ("reg", best_reg_test_cold_start_values[-1])
        print "%20s: %10.10f" % ("mf+reg", best_mfreg_test_cold_start_values[-1])
        print "%20s: %10.10f" % ("LowRankReg", best_lowrankreg_test_cold_start_values[-1])
        print "%20s: %10.10f" % ("mf+LowRankReg", best_mflowrankreg_test_cold_start_values[-1])

    embed()


def synthetic_data_cross_validation_uniform_random_missing_data_experiments():

    ###### Generate the synthetic data #######

    # Seed the randomness
    np.random.seed(1)
    # np.random.seed(int(time()))

    T = 300                  # Length of each univariate time series
    N = 5000                  # Number of generated time series (some will be save for cold start forecasting)
    m = 1000                   # Length of metadata vector
    k = 20                   # True number of latent factors
    Y, phi, L, R, H, U = gen_series(T,N,m,k, model='mf+reg_GP')
    phi = scipy.sparse.csc_matrix(phi)

    # This is the test set
    test_Omega = gen_uniformly_missing_test_set(T, N, .2)
    K = 5 # 5-fold cross validation
    CV_folds = missing_data_CV_folds(T, N, K, test_Omega)

    # Cross validate on training data
    k_in = k    
    max_iters = 5000
    minibatch_size = 300
    missing_data_experiments = True
    directory = 'test/synthetic/cross_validation/uniformly_missing'
    start_time = time()

    # temp = cross_validation(Y, phi, k_in, max_iters, minibatch_size, missing_data_experiments, CV_folds, test_Omega, directory=directory)
    # reg_lambda_1 = temp[0]
    # mf_reg_lambda_1, mf_reg_lambda_2 = temp[1]    
    # lowrankreg_lambda_1 = temp[2]
    # mf_lowrankreg_lambda_1, mf_lowrankreg_lambda_2 = temp[3]
    reg_lambda_1 = 1
    mf_reg_lambda_1, mf_reg_lambda_2 = 1,10   
    lowrankreg_lambda_1 = 1
    mf_lowrankreg_lambda_1, mf_lowrankreg_lambda_2 = 1,10

    end_time = time()
    print "Time take to run algorithm: {0} seconds".format(round(end_time - start_time, 3))

    print "Best lambda values:"
    print "%25s: %10.10f" % ("reg lambda_1", reg_lambda_1)
    print "%25s: %10.10f,\t%25s: %10.10f" % ("mf+reg lambda_1", mf_reg_lambda_1, "mf+reg lambda_2", mf_reg_lambda_2)
    print "%25s: %10.10f" % ("LowRankReg lambda_1", lowrankreg_lambda_1)
    print "%25s: %10.10f,\t%25s: %10.10f" % ("mf+LowRankReg lambda_1", mf_lowrankreg_lambda_1, "mf+LowRankReg lambda_2", mf_lowrankreg_lambda_2)

    print "\nTraining models on values of lambda found through CV"
    # test_Omega[0] is passed in as Omega_train so we can evaluate on the missing test set
    synthetic_data_every_model_experiment(Y, phi, k_in, None, None, test_Omega[0], None, missing_data_experiments, 'Missing', 
                                          reg_lambda_1, mf_reg_lambda_1, mf_reg_lambda_2, 
                                          lowrankreg_lambda_1, mf_lowrankreg_lambda_1, mf_lowrankreg_lambda_2, directory=directory)




def synthetic_data_cross_validation_OOS_cold_start_experiments():
    """ Out-Of-Sample cold start experiments for synthetic data """

    ###### Generate the synthetic data #######

    # Seed the randomness
    np.random.seed(1)
    # np.random.seed(int(time()))

    T = 300                  # Length of each univariate time series
    num_phis = 1000
    K = 5 # Used for cross validation, and also for how many time series per phi
    m = 1000                   # Length of metadata vector
    k = 20                   # True number of latent factors
    Y_train, phi_train, Y_cold_start, phi_cold_start, CV_folds, L, R, H, U = gen_OOS_cold_start(T, num_phis, K, m, k, model='mf+reg_GP')
    phi_train = scipy.sparse.csc_matrix(phi_train)
    phi_cold_start = scipy.sparse.csc_matrix(phi_cold_start)

    # Generate a completely missing Omega, which the algorithm will never get to see
    N_train = Y_train.shape[1]
    N_cold_start = Y_cold_start.shape[1]
    completely_missing_Omega = gen_completely_missing_Omega(T, N_train, N_cold_start, 0.2)

    # Cross validate on training data
    k_in = k    
    max_iters = 5000
    minibatch_size = 300
    missing_data_experiments = False
    directory = 'test/synthetic/cross_validation/OOS_cold_start'
    start_time = time()

    # temp = cross_validation(Y_train, phi_train, k_in, max_iters, minibatch_size, missing_data_experiments, CV_folds, completely_missing_Omega, directory=directory)
    # reg_lambda_1 = temp[0]
    # mf_reg_lambda_1, mf_reg_lambda_2 = temp[1]    
    # lowrankreg_lambda_1 = temp[2]
    # mf_lowrankreg_lambda_1, mf_lowrankreg_lambda_2 = temp[3]
    reg_lambda_1 = 1
    mf_reg_lambda_1, mf_reg_lambda_2 = 1,10   
    lowrankreg_lambda_1 = 1
    mf_lowrankreg_lambda_1, mf_lowrankreg_lambda_2 = 1,10

    end_time = time()
    print "Time take to run algorithm: {0} seconds".format(round(end_time - start_time, 3))

    print "Best lambda values:"
    print "%25s: %10.10f" % ("reg lambda_1", reg_lambda_1)
    print "%25s: %10.10f,\t%25s: %10.10f" % ("mf+reg lambda_1", mf_reg_lambda_1, "mf+reg lambda_2", mf_reg_lambda_2)
    print "%25s: %10.10f" % ("LowRankReg lambda_1", lowrankreg_lambda_1)
    print "%25s: %10.10f,\t%25s: %10.10f" % ("mf+LowRankReg lambda_1", mf_lowrankreg_lambda_1, "mf+LowRankReg lambda_2", mf_lowrankreg_lambda_2)

    print "\nTraining models on values of lambda found through CV"
    synthetic_data_every_model_experiment(Y_train, phi_train, k_in, Y_cold_start, phi_cold_start, None, completely_missing_Omega, missing_data_experiments, 'OOS',
                                          reg_lambda_1, mf_reg_lambda_1, mf_reg_lambda_2, 
                                          lowrankreg_lambda_1, mf_lowrankreg_lambda_1, mf_lowrankreg_lambda_2, directory=directory)





def synthetic_data_cross_validation_pure_cold_start_experiments():
    """ Pure cold start experiments for synthetic data """

    ###### Generate the synthetic data #######

    # Seed the randomness
    np.random.seed(1)
    # np.random.seed(int(time()))

    T = 300                  # Length of each univariate time series
    N_train = 5000            # Number of generated time series (some will be save for cold start forecasting)
    N_cold_start = 1000
    m = 1000                   # Length of metadata vector
    k = 20                   # True number of latent factors
    Y_train, phi_train, Y_cold_start, phi_cold_start, L, R, H, U = gen_pure_cold_start(T, N_train, N_cold_start, m, k, model='mf+reg_GP')
    phi_train = scipy.sparse.csc_matrix(phi_train)
    phi_cold_start = scipy.sparse.csc_matrix(phi_cold_start)

    # Generate a completely missing Omega, which the algorithm will never get to see
    completely_missing_Omega = gen_completely_missing_Omega(T, N_train, N_cold_start, 0.2)

    K = 5 # 5-fold cross validation
    CV_folds = cold_start_CV_folds(T, N_train, K)

    # Cross validate on training data
    k_in = k    
    max_iters = 5000
    minibatch_size = 300
    missing_data_experiments = False
    directory = 'test/synthetic/cross_validation/pure_cold_start'
    start_time = time()

    # temp = cross_validation(Y_train, phi_train, k_in, max_iters, minibatch_size, missing_data_experiments, CV_folds, completely_missing_Omega, directory=directory)
    # reg_lambda_1 = temp[0]
    # mf_reg_lambda_1, mf_reg_lambda_2 = temp[1]    
    # lowrankreg_lambda_1 = temp[2]
    # mf_lowrankreg_lambda_1, mf_lowrankreg_lambda_2 = temp[3]
    reg_lambda_1 = 1
    mf_reg_lambda_1, mf_reg_lambda_2 = 1,10   
    lowrankreg_lambda_1 = 1
    mf_lowrankreg_lambda_1, mf_lowrankreg_lambda_2 = 1,10

    end_time = time()
    print "Time take to run algorithm: {0} seconds".format(round(end_time - start_time, 3))

    print "Best lambda values:"
    print "%25s: %10.10f" % ("reg lambda_1", reg_lambda_1)
    print "%25s: %10.10f,\t%25s: %10.10f" % ("mf+reg lambda_1", mf_reg_lambda_1, "mf+reg lambda_2", mf_reg_lambda_2)
    print "%25s: %10.10f" % ("LowRankReg lambda_1", lowrankreg_lambda_1)
    print "%25s: %10.10f,\t%25s: %10.10f" % ("mf+LowRankReg lambda_1", mf_lowrankreg_lambda_1, "mf+LowRankReg lambda_2", mf_lowrankreg_lambda_2)

    print "\nTraining models on values of lambda found through CV"
    synthetic_data_every_model_experiment(Y_train, phi_train, k_in, Y_cold_start, phi_cold_start, None, completely_missing_Omega, missing_data_experiments, 'Pure CS', 
                                          reg_lambda_1, mf_reg_lambda_1, mf_reg_lambda_2, 
                                          lowrankreg_lambda_1, mf_lowrankreg_lambda_1, mf_lowrankreg_lambda_2, directory=directory)




# To run file:
# $:~ python cold_start_time_series.py <time_series_data_file> <metadata_file> <output_directory>
def real_data_single_experiments():

    # Seed the randomness
    np.random.seed(1)
    # np.random.seed(int(time()))

    # Load the data
    print "Loading in data..."
    args = sys.argv[1:]
    Y_dataframe = pd.read_csv(args[0], index_col=0) # Y is a pandas dataframe
    Y = Y_dataframe.values
    phi = pickle.load(open(args[1], 'rb')) # phi is a scipy.sparse. Largest singular value is 64.5 (64.5^2 ~ 4160)
    T, N = Y.shape
    print "Loaded in data!"

    # print "Removing spikes..."
    # spike_completely_missing_Omega, Y_dataframe = remove_spikes_and_rescale(Y_dataframe)
    # print "Removed spikes!"
    Y = Y_dataframe.values

    # Eventually cross-validation
    num_series_used = N - 100    # Number of univariate time series used. N - num_series_used are saved for cold start forecasting
    print "Number of series used: {0}".format(num_series_used)
    rand_ind = np.random.permutation(N)
    ind_train = rand_ind[:num_series_used]
    ind_cold_start = rand_ind[num_series_used:]

    Y_train = Y[:, ind_train]
    Y_cold_start = Y[:, ind_cold_start]
    phi_train = phi[:, ind_train]
    phi_cold_start = phi[:, ind_cold_start]
    # permuted_spike_completely_missing_Omega = (np.array(spike_completely_missing_Omega)[ind_train], np.array(spike_completely_missing_Omega)[ind_cold_start])

    # Some parameters
    k_in = 5
    model = 'mf+LowRankReg' 
    SGD = True
    minibatch_size = 300
    max_iters = 1000
    delay = ['fixed', 1e-1]
    eps = 1e-8
    verbose = True
    lambda1 = 10
    lambda2 = 1000
    completely_missing_Omega = gen_completely_missing_Omega(T, Y_train.shape[1], Y_cold_start.shape[1], .2) # Unobservable by learning algorithm
    # completely_missing_Omega = None
    # completely_missing_Omega = permuted_spike_completely_missing_Omega
    Omega_train = gen_Omega(T, Y_train.shape[1], completely_missing_Omega, .2) # (indices of) validation set 
    # Omega_train = None

    ###### Run algorithm ######
    print "Using model: {0}".format(model)
    start_time = time()
    # smart_init = smart_initialization(Y_train, phi_train, k_in, model, Omega_train=Omega_train, completely_missing_Omega=completely_missing_Omega, lambda1=lambda1, lambda2=lambda2, init_opt_vars=None, SGD=SGD, minibatch_size=minibatch_size)
    smart_init = None
    opt_vars, obj_values, test_cold_start_values, test_missing_data_values = cold_start_learning(Y_train, phi_train, k_in, model, Y_cold_start, phi_cold_start, Omega_train, completely_missing_Omega, lambda1=lambda1, lambda2=lambda2,
                                                                             initial_opt_vars=smart_init, max_iters=max_iters, SGD=SGD, delay=delay, minibatch_size=minibatch_size, eps=eps, verbose=verbose)

    end_time = time()
    print "Time take to run algorithm: {0} seconds".format(round(end_time - start_time, 3))


    # Plot everything. Set y limits to put stuff on same scale
    if len(args) >= 3:
        directory = args[2]
    else:
        directory = 'test/real_data/single_run'
    if not os.path.exists(directory):
        os.makedirs(directory)
    # plot_experiments(Y_train, phi_train, Y_cold_start, phi_cold_start, model, opt_vars, obj_values, Omega_train, completely_missing_Omega,
    #                  test_cold_start_values, test_missing_data_values, SGD=SGD, directory=directory, file_prefix=model+'_')
    plot_convergence(model, obj_values, test_cold_start_values, test_missing_data_values, SGD=SGD, directory=directory, file_prefix=model+'_')


def real_data_after_CV_experiments(Y_train, phi_train, k_in, Y_cold_start, phi_cold_start, Omega_train, completely_missing_Omega, missing_data_experiments, pure_or_OOS, 
                                   reg_lambda_1, mf_reg_lambda_1, mf_reg_lambda_2, lowrankreg_lambda_1, mf_lowrankreg_lambda_1, mf_lowrankreg_lambda_2, 
                                   directory=None, model_flags=[1,1,1,1]):
    """ A function to train a LowRankReg Models after running cross validation to select hyperparameters on the real data
    """

    if directory is None:
        directory = 'test/real_data/train_after_CV'
    else:
        directory += '/train_after_CV'
    if not os.path.exists(directory):
        os.makedirs(directory)

    start_time = time()
    num_random_restarts = 10
    process_num = 1

    cold_start_args = []

    # Reg (Just run once)
    if model_flags[0]:
        # Args: Y_train, phi_train, k_in, model, Y_cold_start, phi_cold_start, Omega_train, completely_missing_Omega, 
        #       lambda1, lambda2, initial_opt_vars, max_iters, SGD, delay, minibatch_size, eps, verbose, smart_init_bool, seed, process_num
        cold_start_args.append( [Y_train, phi_train, k_in, 'reg', Y_cold_start, phi_cold_start, Omega_train, completely_missing_Omega, 
                                 reg_lambda_1, 52, None, 20000, False, None, 52, 1e-5, False, False, randint(1e7), process_num] )
        process_num += 1

    # Low Rank Reg
    if model_flags[2]:
        for number in range(num_random_restarts):
            # Args: Y_train, phi_train, k_in, model, Y_cold_start, phi_cold_start, Omega_train, completely_missing_Omega, 
            #       lambda1, lambda2, initial_opt_vars, max_iters, SGD, delay, minibatch_size, eps, verbose, smart_init_bool, seed, process_num 
            cold_start_args.append( [Y_train, phi_train, k_in, 'LowRankReg', Y_cold_start, phi_cold_start, Omega_train, completely_missing_Omega, 
                                     lowrankreg_lambda_1, 52, None, 20000, True, ['fixed', 0.15], 500, 1e-8, False, False, randint(1e7), process_num] )
            process_num += 1


    all_runs_obj_values = grid_map(parallel_cold_start_learning, cold_start_args, quiet=False, max_processes=10, local=True, queue='all.q')    

    # Find the best run for Reg model
    start_index_for_lowrankreg = 0
    if model_flags[0]:
        best_reg_model_opt_vars = all_runs_obj_values[0][0]
        best_reg_train_objs = all_runs_obj_values[0][1]
        best_reg_test_cold_start_values = all_runs_obj_values[0][2]
        best_reg_test_missing_data_values = all_runs_obj_values[0][3]
        start_index_for_lowrankreg = 1

    # Find the best run for Low Rank Reg model
    if model_flags[2]:
        best_lowrankreg_index = start_index_for_lowrankreg + np.argmin([temp[1][-1] for temp in all_runs_obj_values[start_index_for_lowrankreg:]]) # + 1 because the first is the reg run
        best_lowrankreg_model_opt_vars = all_runs_obj_values[best_lowrankreg_index][0]
        best_lowrankreg_train_objs = all_runs_obj_values[best_lowrankreg_index][1]
        best_lowrankreg_test_cold_start_values = all_runs_obj_values[best_lowrankreg_index][2]
        best_lowrankreg_test_missing_data_values = all_runs_obj_values[best_lowrankreg_index][3]

    # model, obj_values, test_cold_start_values=None, test_missing_data_values=None, SGD=False, directory='.', file_prefix=''
    if missing_data_experiments:
        if model_flags[0]:
            plot_convergence('reg', best_reg_train_objs, None, best_reg_test_missing_data_values, False, directory, file_prefix='reg'+'_')
        if model_flags[2]:
            plot_convergence('LowRankReg', best_lowrankreg_train_objs, None, best_lowrankreg_test_missing_data_values, True, directory, file_prefix='LowRankReg'+'_')
    else:
        if model_flags[0]:
            plot_convergence('reg', best_reg_train_objs, best_reg_test_cold_start_values, None, False, directory, file_prefix='reg'+'_')
        if model_flags[2]:
            plot_convergence('LowRankReg', best_lowrankreg_train_objs, best_lowrankreg_test_cold_start_values, None, True, directory, file_prefix='LowRankReg'+'_')



    # Next, run matrix factorization methods in parallel
    cold_start_args = []

    # mf+reg
    if model_flags[1]:
        for number in range(num_random_restarts):
            # Args: Y_train, phi_train, k_in, model, Y_cold_start, phi_cold_start, Omega_train, completely_missing_Omega, 
            #       lambda1, lambda2, initial_opt_vars, max_iters, SGD, delay, minibatch_size, eps, verbose, smart_init_bool, seed, process_num
            cold_start_args.append( [Y_train, phi_train, k_in, 'mf+reg', Y_cold_start, phi_cold_start, Omega_train, completely_missing_Omega, 
                                     # mf_reg_lambda_1, mf_reg_lambda_2, top_model_opt_vars[0], 50000, True, None, 30, 1e-5, False, True, randint(1e7)] )
                                     mf_reg_lambda_1, mf_reg_lambda_2, None, 50000, True, ['fixed', 0.05], 300, 1e-10, False, False, randint(1e7), process_num] )
            process_num += 1

    # mf+LowRankReg
    if model_flags[3]:
        for number in range(num_random_restarts):
            # Args: Y_train, phi_train, k_in, model, Y_cold_start, phi_cold_start, Omega_train, completely_missing_Omega, 
            #       lambda1, lambda2, initial_opt_vars, max_iters, SGD, delay, minibatch_size, eps, verbose, smart_init_bool, seed, process_num
            cold_start_args.append( [Y_train, phi_train, k_in, 'mf+LowRankReg', Y_cold_start, phi_cold_start, Omega_train, completely_missing_Omega,
                                 mf_lowrankreg_lambda_1, mf_lowrankreg_lambda_2, None, 20000, True, ['fixed', 0.15], 500, 1e-8, False, False, randint(1e7), process_num] )
            process_num += 1    

    all_runs_obj_values = grid_map(parallel_cold_start_learning, cold_start_args, quiet=False, max_processes=10, local=True, queue='all.q')

    # Find the best run for MF + Reg model
    start_index_for_mflowrankreg = 0
    if model_flags[1]:
        best_mfreg_index = np.argmin([temp[1][-1] for temp in all_runs_obj_values[:num_random_restarts]])
        best_mfreg_model_opt_vars = all_runs_obj_values[best_mfreg_index][0]
        best_mfreg_train_objs = all_runs_obj_values[best_mfreg_index][1]
        best_mfreg_test_cold_start_values = all_runs_obj_values[best_mfreg_index][2]
        best_mfreg_test_missing_data_values = all_runs_obj_values[best_mfreg_index][3]
        start_index_for_mflowrankreg = num_random_restarts

    # Find the best run for MF + Low Rank Reg model
    if model_flags[3]:
        best_mflowrankreg_index = start_index_for_mflowrankreg + np.argmin([temp[1][-1] for temp in all_runs_obj_values[start_index_for_mflowrankreg:]])
        best_mflowrankreg_model_opt_vars = all_runs_obj_values[best_mflowrankreg_index][0]
        best_mflowrankreg_train_objs = all_runs_obj_values[best_mflowrankreg_index][1]
        best_mflowrankreg_test_cold_start_values = all_runs_obj_values[best_mflowrankreg_index][2]
        best_mflowrankreg_test_missing_data_values = all_runs_obj_values[best_mflowrankreg_index][3]

    if missing_data_experiments:
        if model_flags[1]:
            plot_convergence('mf+reg', best_mfreg_train_objs, None, best_mfreg_test_missing_data_values, True, directory, file_prefix='mf+reg'+'_')
        if model_flags[3]:
            plot_convergence('mf+LowRankReg', best_mflowrankreg_train_objs, None, best_mflowrankreg_test_missing_data_values, True, directory, file_prefix='mf+LowRankReg'+'_')
    else:
        if model_flags[1]:
            plot_convergence('mf+reg', best_mfreg_train_objs, best_mfreg_test_cold_start_values, None, True, directory, file_prefix='mf+reg'+'_')
        if model_flags[3]:
            plot_convergence('mf+LowRankReg', best_mflowrankreg_train_objs, best_mflowrankreg_test_cold_start_values, None, True, directory, file_prefix='mf+LowRankReg'+'_')

    ### SAVE MODELS ###

    # Save best Reg model
    if model_flags[0]:
        do = Dump_Object(best_reg_model_opt_vars, best_reg_train_objs, best_reg_test_cold_start_values, best_reg_test_missing_data_values)
        pickle.dump(do, open('opt_dumps/Final Reg {0} Model.pkl'.format(pure_or_OOS), 'wb'))

    # Save best MF + Reg model
    if model_flags[1]:
        do = Dump_Object(best_mfreg_model_opt_vars, best_mfreg_train_objs, best_mfreg_test_cold_start_values, best_mfreg_test_missing_data_values)
        pickle.dump(do, open('opt_dumps/Final MF+Reg {0} Model.pkl'.format(pure_or_OOS), 'wb'))

    # Save best Low Rank Reg model
    if model_flags[2]:
        do = Dump_Object(best_lowrankreg_model_opt_vars, best_lowrankreg_train_objs, best_lowrankreg_test_cold_start_values, best_lowrankreg_test_missing_data_values)
        pickle.dump(do, open('opt_dumps/Final LowRankReg {0} Model.pkl'.format(pure_or_OOS), 'wb'))

    # Save best MF + Low Rank Reg model
    if model_flags[3]:
        do = Dump_Object(best_mflowrankreg_model_opt_vars, best_mflowrankreg_train_objs, best_mflowrankreg_test_cold_start_values, best_mflowrankreg_test_missing_data_values)
        pickle.dump(do, open('opt_dumps/Final MF+LowRankReg {0} Model.pkl'.format(pure_or_OOS), 'wb'))

    end_time = time()
    print "Time take to run algorithm: {0} seconds".format(round(end_time - start_time, 3))

    missing_data_mask, validation_missing_data_mask, validation_cold_start_mask, Omega_train, completely_missing_Omega = construct_missing_data_masks(Y_train, Y_cold_start, Omega_train, completely_missing_Omega)

    print ""
    print "Training RSS:"
    if model_flags[0]:
        print "%20s: %10.10f" % ("reg", best_reg_train_objs[-1])
    if model_flags[1]:
        print "%20s: %10.10f" % ("mf+reg", best_mfreg_train_objs[-1])
    if model_flags[2]:
        print "%20s: %10.10f" % ("LowRankReg", best_lowrankreg_train_objs[-1])
    if model_flags[3]:
        print "%20s: %10.10f" % ("mf+LowRankReg", best_mflowrankreg_train_objs[-1])
    print ""

    if missing_data_experiments:
        print "Test Missing Data RSS:"
        if model_flags[0]:
            print "%20s: %10.10f" % ("reg", best_reg_test_missing_data_values[-1])
        if model_flags[1]:
            print "%20s: %10.10f" % ("mf+reg", best_mfreg_test_missing_data_values[-1])
        if model_flags[2]:
            print "%20s: %10.10f" % ("LowRankReg", best_lowrankreg_test_missing_data_values[-1])
        if model_flags[3]:
            print "%20s: %10.10f" % ("mf+LowRankReg", best_mflowrankreg_test_missing_data_values[-1])
    else:
        print "Test Cold Start RSS:"
        if model_flags[0]:
            print "%20s: %10.10f" % ("reg", best_reg_test_cold_start_values[-1])
        if model_flags[1]:
            print "%20s: %10.10f" % ("mf+reg", best_mfreg_test_cold_start_values[-1])
        if model_flags[2]:
            print "%20s: %10.10f" % ("LowRankReg", best_lowrankreg_test_cold_start_values[-1])
        if model_flags[3]:
            print "%20s: %10.10f" % ("mf+LowRankReg", best_mflowrankreg_test_cold_start_values[-1])

        print ""

        print "Test Cold Start MAE:"
        if model_flags[0]:
            print "%20s: %10.10f" % ("reg", test_cold_start_error(best_reg_model_opt_vars, Y_cold_start, phi_cold_start, validation_cold_start_mask, 'reg', mse_mae='mae'))
        if model_flags[1]:
            print "%20s: %10.10f" % ("mf+reg", test_cold_start_error(best_mfreg_model_opt_vars, Y_cold_start, phi_cold_start, validation_cold_start_mask, 'mf+reg', mse_mae='mae'))
        if model_flags[2]:
            print "%20s: %10.10f" % ("LowRankReg", test_cold_start_error(best_lowrankreg_model_opt_vars, Y_cold_start, phi_cold_start, validation_cold_start_mask, 'LowRankReg', mse_mae='mae'))
        if model_flags[3]:
            print "%20s: %10.10f" % ("mf+LowRankReg", test_cold_start_error(best_mflowrankreg_model_opt_vars, Y_cold_start, phi_cold_start, validation_cold_start_mask, 'mf+LowRankReg', mse_mae='mae'))

    embed()




def real_data_cross_validation_large_chunk_missing_experiments():

    # Seed the randomness
    np.random.seed(1)
    # np.random.seed(int(time()))

    # Load the data
    print "Loading in data..."
    args = sys.argv[1:]
    Y_dataframe = pd.read_csv(args[0], index_col=0) # Y_dataframe is a pandas dataframe
    Y = Y_dataframe.values
    phi = pickle.load(open(args[1], 'rb')) # phi is a scipy.sparse matrix
    T, N = Y.shape
    print "Loaded in data!"

    # This is the test set
    completely_missing_Omega = gen_large_missing_chunks_test_set(T, N, .5)
    K = 5 # 5-fold cross validation
    CV_folds = missing_data_CV_folds(T, N, K, completely_missing_Omega, shuffle=False)

    # Cross validate on training data
    k_in = 5
    max_iters = 30000
    minibatch_size = 300
    missing_data_experiments = True
    directory = 'test/real_data/large_chunk'
    model_flags = [1, 1, 1, 1]

    start_time = time()
    temp = cross_validation(Y, phi, k_in, max_iters, minibatch_size, missing_data_experiments, CV_folds, completely_missing_Omega, directory=directory, model_flags=model_flags)
    reg_lambda_1 = temp[0]
    mf_reg_lambda_1, mf_reg_lambda_2 = temp[1]    
    lowrankreg_lambda_1 = temp[2]
    mf_lowrankreg_lambda_1, mf_lowrankreg_lambda_2 = temp[3]
    end_time = time()
    print "Time take to run algorithm: {0} seconds".format(round(end_time - start_time, 3))

    print "Best lambda values:"
    if model_flags[0]:
        print "%25s: %10.10f" % ("reg lambda_1", reg_lambda_1)
    if model_flags[1]:
        print "%25s: %10.10f,\t%25s: %10.10f" % ("mf+reg lambda_1", mf_reg_lambda_1, "mf+reg lambda_2", mf_reg_lambda_2)
    if model_flags[2]:
        print "%25s: %10.10f" % ("LowRankReg lambda_1", lowrankreg_lambda_1)
    if model_flags[3]:
        print "%25s: %10.10f,\t%25s: %10.10f" % ("mf+LowRankReg lambda_1", mf_lowrankreg_lambda_1, "mf+LowRankReg lambda_2", mf_lowrankreg_lambda_2)

    print "\nTraining models on values of lambda found through CV"
    real_data_after_CV_experiments(Y, phi, k_in, None, None, completely_missing_Omega[0], None, missing_data_experiments, "Missing", 
                                   reg_lambda_1, mf_reg_lambda_1, mf_reg_lambda_2, lowrankreg_lambda_1, mf_lowrankreg_lambda_1, mf_lowrankreg_lambda_2, 
                                   directory=directory, model_flags=model_flags)




def real_data_cross_validation_warm_start_missing_experiments():

    # Seed the randomness
    np.random.seed(1)
    # np.random.seed(int(time()))

    # Load the data
    print "Loading in data..."
    args = sys.argv[1:]
    Y_dataframe = pd.read_csv(args[0], index_col=0) # Y_dataframe is a pandas dataframe
    Y = Y_dataframe.values
    phi = pickle.load(open(args[1], 'rb')) # phi is a scipy.sparse matrix
    print "Loaded in data!"

    # This is the test set
    K = 5 # 5-fold cross validation
    # Y_train, phi_train, Y_cold_start, phi_cold_start, CV_folds, train_articles_indices, test_articles_indices, index_to_article = segment_articles_into_OOS_CV_folds(Y_dataframe, phi, K)
    Y_train, phi_train, Y_cold_start, phi_cold_start, train_articles_indices, test_articles_indices, index_to_article = segment_articles_into_pure_cold_start_train_test(Y_dataframe, phi)
    N_train = Y_train.shape[1]; N_cold_start = Y_cold_start.shape[1]
    Y = np.concatenate((Y_train, Y_cold_start), axis=1)
    phi = scipy.sparse.hstack((phi_train, phi_cold_start))
    T, N = Y.shape
    completely_missing_Omega, CV_folds = gen_warm_start_test_set(T, N_train, N_cold_start, K, num_present=8)

    # Cross validate on training data
    k_in = 20
    max_iters = 10000
    minibatch_size = 500
    missing_data_experiments = True
    directory = 'test/real_data/warm_start'
    model_flags = [0, 0, 1, 1]

    start_time = time()
    temp = cross_validation(Y_train, phi_train, k_in, max_iters, minibatch_size, missing_data_experiments, CV_folds, None, directory=directory, model_flags=model_flags)
    reg_lambda_1 = temp[0]
    mf_reg_lambda_1, mf_reg_lambda_2 = temp[1]    
    lowrankreg_lambda_1 = temp[2]
    mf_lowrankreg_lambda_1, mf_lowrankreg_lambda_2 = temp[3]
    end_time = time()
    print "Time take to run algorithm: {0} seconds".format(round(end_time - start_time, 3))

    print "Best lambda values:"
    if model_flags[0]:
        print "%25s: %10.10f" % ("reg lambda_1", reg_lambda_1)
    if model_flags[1]:
        print "%25s: %10.10f,\t%25s: %10.10f" % ("mf+reg lambda_1", mf_reg_lambda_1, "mf+reg lambda_2", mf_reg_lambda_2)
    if model_flags[2]:
        print "%25s: %10.10f" % ("LowRankReg lambda_1", lowrankreg_lambda_1)
    if model_flags[3]:
        print "%25s: %10.10f,\t%25s: %10.10f" % ("mf+LowRankReg lambda_1", mf_lowrankreg_lambda_1, "mf+LowRankReg lambda_2", mf_lowrankreg_lambda_2)

    print "\nTraining models on values of lambda found through CV"
    real_data_after_CV_experiments(Y, phi, k_in, None, None, completely_missing_Omega[0], None, missing_data_experiments, "WS", 
                                   reg_lambda_1, mf_reg_lambda_1, mf_reg_lambda_2, lowrankreg_lambda_1, mf_lowrankreg_lambda_1, mf_lowrankreg_lambda_2, 
                                   directory=directory, model_flags=model_flags)




def real_data_cross_validation_OOS_cold_start_experiments():

    # Seed the randomness
    np.random.seed(1)
    # np.random.seed(int(time()))

    # Load the data
    print "Loading in data..."
    args = sys.argv[1:]
    Y_dataframe = pd.read_csv(args[0], index_col=0) # Y_dataframe is a pandas dataframe
    phi = pickle.load(open(args[1], 'rb')) # phi is a scipy.sparse matrix
    # phi = scipy.sparse.csc_matrix(np.zeros(phi.shape))
    print "Loaded in data!"

    # print "Removing spikes..."
    # spike_completely_missing_Omega, Y_dataframe = remove_spikes_and_rescale(Y_dataframe)
    # print "Removed spikes!"

    K = 5 # 5-fold Cross Validation
    Y_train, phi_train, Y_cold_start, phi_cold_start, CV_folds, train_articles_indices, test_articles_indices, index_to_article = segment_articles_into_OOS_CV_folds(Y_dataframe, phi, K)

    # Generate a completely missing Omega, which the algorithm will never get to see
    T, N_train = Y_train.shape
    N_cold_start = Y_cold_start.shape[1]
    completely_missing_Omega = gen_completely_missing_Omega(T, N_train, N_cold_start, 0.2)
    # completely_missing_Omega[0] = gen_large_missing_chunks_test_set(T, N_train, .5)[0]
    # completely_missing_Omega[0] = gen_completely_missing_Omega(T, N_train, N_cold_start, 0.45)[0]
    # Combine completely_missing_Omega with spike missing
    # permuted_spike_completely_missing_Omega = (np.array(spike_completely_missing_Omega)[train_articles_indices], np.array(spike_completely_missing_Omega)[test_articles_indices])
    # completely_missing_Omega = combine_CMOs(completely_missing_Omega, permuted_spike_completely_missing_Omega)

    # FOR MAKING PLOTS
    do = pickle.load(open('opt_dumps/wiki_data/stddev_data/Final MF+LowRankReg OOS Model.pkl', 'rb'))
    opt_vars = do.opt_vars
    model = 'mf+LowRankReg'
    # indices = [3662, 2213, 2729, 834, 1124, 2738, 2906, 3152, 443, 304, 250, 1274, 2947, 517, 3897, 2617, 689, 1737, 1084, 2427, 2893, 2665, 3377, 2876, 3406, 3757, 3475, 3211, 1956, 2610, 3244]
    indices = [3724, 2238, 2773, 840, 1126, 2782, 2956, 3203, 174, 302, 250, 1277, 2996, 515, 3967, 2659, 2767, 1740, 1086, 2458, 2943, 2709, 3435, 2927, 3465, 3819, 3536, 3263, 1968, 2651, 3299]
    # generate_cold_start_plot(Y_cold_start, phi_cold_start, opt_vars, model, completely_missing_Omega, test_articles_indices, index_to_article, indices=None)
    generate_single_cold_start_plot(Y_cold_start, phi_cold_start, opt_vars, model, completely_missing_Omega, test_articles_indices, index_to_article, indices=indices, pure_or_OOS="OOS")
    embed()


    # Cross validate on training data
    k_in = 20
    max_iters = 10000
    minibatch_size = 500
    missing_data_experiments = False
    directory = 'test/real_data/OOS_cold_start'
    model_flags = [1, 1, 1, 1]

    start_time = time()
    temp = cross_validation(Y_train, phi_train, k_in, max_iters, minibatch_size, missing_data_experiments, CV_folds, completely_missing_Omega, directory=directory, model_flags=model_flags)
    reg_lambda_1 = temp[0]
    mf_reg_lambda_1, mf_reg_lambda_2 = temp[1]    
    lowrankreg_lambda_1 = temp[2]
    mf_lowrankreg_lambda_1, mf_lowrankreg_lambda_2 = temp[3]
    end_time = time()
    print "Time take to run algorithm: {0} seconds".format(round(end_time - start_time, 3))

    print "Best lambda values:"
    if model_flags[0]:
        print "%25s: %10.10f" % ("reg lambda_1", reg_lambda_1)
    if model_flags[1]:
        print "%25s: %10.10f,\t%25s: %10.10f" % ("mf+reg lambda_1", mf_reg_lambda_1, "mf+reg lambda_2", mf_reg_lambda_2)
    if model_flags[2]:
        print "%25s: %10.10f" % ("LowRankReg lambda_1", lowrankreg_lambda_1)
    if model_flags[3]:
        print "%25s: %10.10f,\t%25s: %10.10f" % ("mf+LowRankReg lambda_1", mf_lowrankreg_lambda_1, "mf+LowRankReg lambda_2", mf_lowrankreg_lambda_2)

    print "\nTraining models on values of lambda found through CV"
    real_data_after_CV_experiments(Y_train, phi_train, k_in, Y_cold_start, phi_cold_start, None, completely_missing_Omega, missing_data_experiments, "OOS", 
                                   reg_lambda_1, mf_reg_lambda_1, mf_reg_lambda_2, lowrankreg_lambda_1, mf_lowrankreg_lambda_1, mf_lowrankreg_lambda_2, 
                                   directory=directory, model_flags=model_flags)




def real_data_cross_validation_pure_cold_start_experiments():
    """ Pure cold start experiments for synthetic data """

    # Seed the randomness
    np.random.seed(1)
    # np.random.seed(int(time()))

    # Load the data
    print "Loading in data..."
    args = sys.argv[1:]
    Y_dataframe = pd.read_csv(args[0], index_col=0) # Y_dataframe is a pandas dataframe
    phi = pickle.load(open(args[1], 'rb')) # phi is a scipy.sparse matrix
    print "Loaded in data!"

    # print "Removing spikes..."
    # spike_completely_missing_Omega, Y_dataframe = remove_spikes_and_rescale(Y_dataframe)
    # print "Removed spikes!"

    K = 5 # 5-fold Cross Validation
    Y_train, phi_train, Y_cold_start, phi_cold_start, train_articles_indices, test_articles_indices, index_to_article = segment_articles_into_pure_cold_start_train_test(Y_dataframe, phi)

    # Generate a completely missing Omega, which the algorithm will never get to see
    T, N_train = Y_train.shape
    N_cold_start = Y_cold_start.shape[1]
    completely_missing_Omega = gen_completely_missing_Omega(T, N_train, N_cold_start, 0.2)
    completely_missing_Omega[0] = gen_large_missing_chunks_test_set(T, N_train, .5)[0]
    # Combine completely_missing_Omega with spike missing
    # permuted_spike_completely_missing_Omega = (np.array(spike_completely_missing_Omega)[train_articles_indices], np.array(spike_completely_missing_Omega)[test_articles_indices])
    # completely_missing_Omega = combine_CMOs(completely_missing_Omega, permuted_spike_completely_missing_Omega)

    # FOR MAKING PLOTS
    do = pickle.load(open('opt_dumps/wiki_data/stddev_data/Final MF+LowRankReg Pure CS Model.pkl', 'rb'))
    opt_vars = do.opt_vars
    model = 'mf+LowRankReg'
    # indices = [317, 623, 746, 911, 910, 382, 338, 531, 887, 0, 48, 138, 238, 290, 85, 270, 420, 361, 271, 79, 660, 468, 187]# [623, 468, 265, 704, 321]
    indices = [ 720,  133,   11,  520, 1014,    4,  304,  286, 1013,  265]
    # generate_cold_start_plot(Y_cold_start, phi_cold_start, opt_vars, model, completely_missing_Omega, test_articles_indices, index_to_article, indices=None)
    generate_single_cold_start_plot(Y_cold_start, phi_cold_start, opt_vars, model, completely_missing_Omega, test_articles_indices, index_to_article, indices=indices, pure_or_OOS="Pure")
    embed()

    # Create CV folds
    CV_folds = cold_start_CV_folds(T, N_train, K)

    # Cross validate on training data
    k_in = 20
    max_iters = 10000
    minibatch_size = 500
    missing_data_experiments = False
    directory = 'test/real_data/pure_cold_start'
    model_flags = [1, 1, 1, 1]

    start_time = time()
    temp = cross_validation(Y_train, phi_train, k_in, max_iters, minibatch_size, missing_data_experiments, CV_folds, completely_missing_Omega, directory=directory, model_flags=model_flags)
    reg_lambda_1 = temp[0]
    mf_reg_lambda_1, mf_reg_lambda_2 = temp[1]    
    lowrankreg_lambda_1 = temp[2]
    mf_lowrankreg_lambda_1, mf_lowrankreg_lambda_2 = temp[3]
    end_time = time()
    print "Time take to run algorithm: {0} seconds".format(round(end_time - start_time, 3))

    print "Best lambda values:"
    if model_flags[0]:
        print "%25s: %10.10f" % ("reg lambda_1", reg_lambda_1)
    if model_flags[1]:
        print "%25s: %10.10f,\t%25s: %10.10f" % ("mf+reg lambda_1", mf_reg_lambda_1, "mf+reg lambda_2", mf_reg_lambda_2)
    if model_flags[2]:
        print "%25s: %10.10f" % ("LowRankReg lambda_1", lowrankreg_lambda_1)
    if model_flags[3]:
        print "%25s: %10.10f,\t%25s: %10.10f" % ("mf+LowRankReg lambda_1", mf_lowrankreg_lambda_1, "mf+LowRankReg lambda_2", mf_lowrankreg_lambda_2)

    print "\nTraining models on values of lambda found through CV"
    real_data_after_CV_experiments(Y_train, phi_train, k_in, Y_cold_start, phi_cold_start, None, completely_missing_Omega, missing_data_experiments, "Pure CS", 
                                   reg_lambda_1, mf_reg_lambda_1, mf_reg_lambda_2, lowrankreg_lambda_1, mf_lowrankreg_lambda_1, mf_lowrankreg_lambda_2, 
                                   directory=directory, model_flags=model_flags)



def real_data_OOS_baseline_cold_start_experiments():
    """ OOS cold start baseline for real data. Average past years """

    # Seed the randomness
    np.random.seed(1)
    # np.random.seed(int(time()))

    # Load the data
    print "Loading in data..."
    args = sys.argv[1:]
    Y_dataframe = pd.read_csv(args[0], index_col=0) # Y_dataframe is a pandas dataframe
    phi = pickle.load(open(args[1], 'rb')) # phi is a scipy.sparse matrix
    print "Loaded in data!"

    K = 5 # 5-fold Cross Validation
    Y_train, phi_train, Y_cold_start, phi_cold_start, CV_folds, train_articles_indices, test_articles_indices, index_to_article = segment_articles_into_OOS_CV_folds(Y_dataframe, phi, K)

    # Generate a completely missing Omega, which the algorithm will never get to see
    T, N_train = Y_train.shape
    N_cold_start = Y_cold_start.shape[1]
    completely_missing_Omega = gen_completely_missing_Omega(T, N_train, N_cold_start, 0.2)
    print "Test set size: {0}".format(N_cold_start)

    start_time = time()    
    baseline_error = OOS_baseline_average_past_years(Y_dataframe.values, phi, train_articles_indices, test_articles_indices, completely_missing_Omega, index_to_article)
    end_time = time()
    print "Time take to run OOS baseline: {0} seconds".format(round(end_time - start_time, 3))

    print "Error using OOS baseline for OOS cold start: {0}".format(baseline_error)



def real_data_kNN_pure_cold_start_experiments():
    """ Pure cold start baseline for real data """

    # Seed the randomness
    np.random.seed(1)
    # np.random.seed(int(time()))

    # Load the data
    print "Loading in data..."
    args = sys.argv[1:]
    Y_dataframe = pd.read_csv(args[0], index_col=0) # Y_dataframe is a pandas dataframe
    phi = pickle.load(open(args[1], 'rb')) # phi is a scipy.sparse matrix
    print "Loaded in data!"

    K = 5 # 5-fold Cross Validation
    Y_train, phi_train, Y_cold_start, phi_cold_start, train_articles_indices, test_articles_indices, index_to_article = segment_articles_into_pure_cold_start_train_test(Y_dataframe, phi)

    # Generate a completely missing Omega, which the algorithm will never get to see
    T, N_train = Y_train.shape
    N_cold_start = Y_cold_start.shape[1]
    completely_missing_Omega = gen_completely_missing_Omega(T, N_train, N_cold_start, 0.2)

    K_for_k_NN = 10
    start_time = time()    
    k_NN_error = kNN_time_series_prediction(K_for_k_NN, Y_dataframe.values, phi, Y_cold_start, phi_cold_start, train_articles_indices, completely_missing_Omega, index_to_article, test_articles_indices)
    end_time = time()
    print "Time take to run k-NN: {0} seconds".format(round(end_time - start_time, 3))

    print "Error using {0}-NN for PURE cold start: {1}".format(K_for_k_NN, k_NN_error)




def real_data_cold_start_plots():

    # Seed the randomness
    np.random.seed(1)
    # np.random.seed(int(time()))

    # Load the data
    print "Loading in data..."
    args = sys.argv[1:]
    Y_dataframe = pd.read_csv(args[0], index_col=0) # Y_dataframe is a pandas dataframe
    phi = pickle.load(open(args[1], 'rb')) # phi is a scipy.sparse matrix
    print "Loaded in data!"

    OOS = True # Use this to flip between Pure or OOS
    if OOS:
        K = 5
        Y_train, phi_train, Y_cold_start, phi_cold_start, CV_folds, train_articles_indices, test_articles_indices, index_to_article = segment_articles_into_OOS_CV_folds(Y_dataframe, phi, K)
        pure_or_OOS = "OOS"
        indices = [3662, 2213, 2729, 834, 1124, 2738]
    else:
        Y_train, phi_train, Y_cold_start, phi_cold_start, train_articles_indices, test_articles_indices, index_to_article = segment_articles_into_pure_cold_start_train_test(Y_dataframe, phi)
        pure_or_OOS = "Pure CS"
        indices = [317, 623, 746, 911, 910, 382, 338, 531, 887, 0, 48, 138, 238, 290, 85]# [623, 468, 265, 704, 321]

    # FOR MAKING PLOTS
    do = pickle.load(open('opt_dumps/Final LowRankReg {0} Model.pkl'.format(pure_or_OOS), 'rb'))
    lrr_opt_vars = do.opt_vars
    do = pickle.load(open('opt_dumps/Final MF+LowRankReg {0} Model.pkl'.format(pure_or_OOS), 'rb'))
    mflrr_opt_vars = do.opt_vars

    print "Articles: "
    for index, article in zip(indices, [index_to_article[test_articles_indices[i]] for i in indices]):
        print str(index) + '\t' + article

    for fig_num, i in enumerate(indices):
        article = index_to_article[test_articles_indices[i]]

        fig = figure(fig_num+1)
        fig.suptitle(article)

        subplot(2, 1, 1)
        H, U, b = extract_variables(lrr_opt_vars, 'LowRankReg')
        lrr_projection = H.dot(U).dot( phi_cold_start[:,i].toarray()[:,0] ) + b
        plot(Y_cold_start[:,i], color='magenta') # or red
        plot(lrr_projection, linewidth=1, color='blue' )
        MSE_lrr = .5 * pow(Y_cold_start[:,i] - lrr_projection, 2)

        plot_shift = 1.5
        L, R, H, U, b = extract_variables(mflrr_opt_vars, 'mf+LowRankReg')
        mflrr_projection = H.dot(U).dot( phi_cold_start[:,i].toarray()[:,0] ) + b
        plot(Y_cold_start[:,i] + plot_shift, color='magenta') # or red
        plot(mflrr_projection + plot_shift, linewidth=1, color='green' )        
        MSE_mflrr = .5 * pow(Y_cold_start[:,i] - lrr_projection, 2) + plot_shift

        xlim([0, 365])
        locator_params(axis='y',nbins=3)
        locator_params(axis='x',nbins=5)

        # Plot MSE
        subplot(2, 1, 2)
        plot(MSE_lrr+0.1, color='blue')
        plot(MSE_mflrr+0.1, color='green')
        legend(['LowRankReg', 'MF+LowRankReg'], loc='best')

        xlim([0, 365])
        locator_params(axis='y',nbins=3)
        locator_params(axis='x',nbins=5)


        subplots_adjust(hspace=0.2)

        savefig('Images/Real Data Experiments/Cold Start Predictions/lrr vs. mflrr/{0}/'.format(pure_or_OOS) + article + '.pdf', bbox_inches='tight')




def real_data_error_hist():

    # Seed the randomness
    np.random.seed(1)
    # np.random.seed(int(time()))

    # Load the data
    print "Loading in data..."
    args = sys.argv[1:]
    Y_dataframe = pd.read_csv(args[0], index_col=0) # Y_dataframe is a pandas dataframe
    phi = pickle.load(open(args[1], 'rb')) # phi is a scipy.sparse matrix
    print "Loaded in data!"


    OOS = True # Use this to flip between Pure or OOS
    model = 'mf+LowRankReg'

    if OOS:
        K = 5
        Y_train, phi_train, Y_cold_start, phi_cold_start, CV_folds, train_articles_indices, test_articles_indices, index_to_article = segment_articles_into_OOS_CV_folds(Y_dataframe, phi, K)
        pure_or_OOS = "OOS"
    else:
        Y_train, phi_train, Y_cold_start, phi_cold_start, train_articles_indices, test_articles_indices, index_to_article = segment_articles_into_pure_cold_start_train_test(Y_dataframe, phi)
        pure_or_OOS = "Pure CS"

    # Load opt_vars
    do = pickle.load(open('opt_dumps/Final {0} {1} Model.pkl'.format(model, pure_or_OOS), 'rb'))
    opt_vars = do.opt_vars

    T, N_train = Y_train.shape
    N_cold_start = Y_cold_start.shape[1]
    completely_missing_Omega = gen_completely_missing_Omega(T, N_train, N_cold_start, 0.2)
    missing_data_mask, validation_missing_data_mask, validation_cold_start_mask, Omega_train, completely_missing_Omega = construct_missing_data_masks(Y_train, Y_cold_start, None, completely_missing_Omega)

    test_error = test_cold_start_error(opt_vars, Y_cold_start, phi_cold_start, validation_cold_start_mask, model)
    print "Test error: {0}".format(test_error)

    # Create histogram of test error
    if model == 'LowRankReg':
        H, U, b = extract_variables(opt_vars, model)
    elif model == 'mf+LowRankReg':
        L, R, H, U, b = extract_variables(opt_vars, model)

    individual_errors = pow(validation_cold_start_mask.T * ( Y_cold_start.T - phi_cold_start.T.dot(U.T).dot(H.T) - np.outer(np.ones(N_cold_start), b) ), 2)
    hist(individual_errors.reshape(-1), bins=100)
    title("Histogram of Errors, {0}, {1}".format(model, pure_or_OOS))
    ylim([0,1000])
    xlim([0, 1.4])
    show()
    embed()


    # thresholds = [0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1.0, 1.1, 1.2, 1.3, 1.4, 1.5]

    # do = pickle.load(open('opt_dumps/Final {0} {1} Model.pkl'.format('LowRankReg', pure_or_OOS), 'rb'))
    # lrr_H, lrr_U, lrr_b = extract_variables(do.opt_vars, 'LowRankReg')
    # lrr_individual_errors = pow(validation_cold_start_mask.T * ( Y_cold_start.T - phi_cold_start.T.dot(lrr_U.T).dot(lrr_H.T) - np.outer(np.ones(N_cold_start), lrr_b) ), 2)
    # do = pickle.load(open('opt_dumps/Final {0} {1} Model.pkl'.format('mf+LowRankReg', pure_or_OOS), 'rb'))
    # mflrr_L, mflrr_R, mflrr_H, mflrr_U, mflrr_b = extract_variables(do.opt_vars, 'mf+LowRankReg')
    # mflrr_individual_errors = pow(validation_cold_start_mask.T * ( Y_cold_start.T - phi_cold_start.T.dot(mflrr_U.T).dot(mflrr_H.T) - np.outer(np.ones(N_cold_start), mflrr_b) ), 2)
    # for th in thresholds:
    #     lrr_error = sum(.5 / N_cold_start * lrr_individual_errors[lrr_individual_errors < th])
    #     mflrr_error = sum(.5 / N_cold_start * mflrr_individual_errors[mflrr_individual_errors < th])
    #     print "Threshold: {0}, LRR Error: {1}, MF+LRR Error: {2}, MF+LRR lower: {3}".format(th, lrr_error, mflrr_error, mflrr_error < lrr_error)

    # embed()


if __name__ == "__main__":

    logging.captureWarnings(True)
    logging.basicConfig(format=('%(asctime)s - %(name)s - %(levelname)s - ' +
                                '%(message)s'), level=logging.INFO)

    # synthetic_data_single_experiment()
    # synthetic_data_cross_validation_uniform_random_missing_data_experiments()
    # synthetic_data_cross_validation_OOS_cold_start_experiments()
    # synthetic_data_cross_validation_pure_cold_start_experiments()
    # real_data_single_experiments()
    # real_data_cross_validation_large_chunk_missing_experiments()
    real_data_cross_validation_warm_start_missing_experiments()
    # real_data_cross_validation_OOS_cold_start_experiments()
    # real_data_cross_validation_pure_cold_start_experiments()
    # real_data_OOS_baseline_cold_start_experiments()
    # real_data_kNN_pure_cold_start_experiments()
    # real_data_cold_start_plots()
    # real_data_error_hist()

