from pylab import *
from IPython import embed
import scipy.sparse
import scipy.sparse.linalg

from patsy import dmatrix


def augment_basis_matrix_with_Fourier_basis_functions(B):
    """ Augments the basis matrix B with 2 Fourier basis functions: 

        sin(2*pi * t/7)
        cos(2*pi * t/7)

        This will help handle oscillating weekly patterns in real data
    """
    num_days = B.shape[0]
    days = np.arange(1, num_days + 1)
    sin_basis_func = np.sin(days * 2*np.pi / 7.)
    cos_basis_func = np.cos(days * 2*np.pi / 7.)

    augment = np.array([sin_basis_func, cos_basis_func]).T

    return np.concatenate((B, augment), axis=1)

def augment_basis_matrix_with_chopped_Fourier_basis_functions(B):
    """ Augments the basis matrix B with 2 Fourier basis functions: 

        sin(2*pi * t/7)
        cos(2*pi * t/7)

        but chopped up by 28-day segments (only goes up to 364 days, but should be okay).
    """
    T = B.shape[0]
    num_days = B.shape[0]
    days = np.arange(1, num_days + 1)
    sin_basis_func = np.sin(days * 2*np.pi / 7.)
    cos_basis_func = np.cos(days * 2*np.pi / 7.)

    sin_augment = np.zeros((T, 13))
    cos_augment = np.zeros((T, 13))
    for i in xrange(13):
        sin_augment[i*28 : (i+1)*28, i] = sin_basis_func[i*28 : (i+1)*28]
        cos_augment[i*28 : (i+1)*28, i] = cos_basis_func[i*28 : (i+1)*28]

    augment = np.concatenate((sin_augment, cos_augment), axis=1)

    return np.concatenate((B, augment), axis=1)

def remove_spikes_and_rescale(Y_dataframe):
    """ Removes spikes by calculating the 3 sigma range and letting anything bigger than that be a "spike"

        Returns a partial completely_missing_Omega
    """
    Y_values = Y_dataframe.values
    N = Y_values.shape[1]

    # Get 3-sigma value
    three_sigma = 3 * np.std(Y_values)

    # Indices of "spikes"
    spike_indices = np.where(Y_values > three_sigma)
    spike_row_vals = spike_indices[0]
    spike_col_vals = spike_indices[1]

    # Create a partial completely_missing_Omega (and corresponding mask)
    mask = np.ones(Y_values.shape)
    completely_missing_Omega = [[] for i in xrange(N)]
    for col, row in zip(spike_col_vals, spike_row_vals):
        completely_missing_Omega[col].append(row)
        mask[row, col] = 0
    for i in xrange(N):
        completely_missing_Omega[i] = np.array(completely_missing_Omega[i], dtype=int)

    # Now rescale Y_dataframe
    unspiked_mean = np.mean(Y_values * mask)
    unspiked_stddev = np.std(Y_values * mask)
    Y_dataframe = Y_dataframe.applymap(lambda x, unspiked_mean=unspiked_mean : x - unspiked_mean)
    Y_dataframe = Y_dataframe.applymap(lambda x, unspiked_stddev=unspiked_stddev : x / unspiked_stddev)

    return completely_missing_Omega, Y_dataframe

def combine_CMOs(cmo1, cmo2):
    """ Combine 2 different completely_missing_Omegas
    """

    N_train = len(cmo1[0])
    N_cold_start = len(cmo1[1])

    if N_train != len(cmo2[0]) or N_cold_start != len(cmo2[1]):
        raise Exception("Lengths of CMO's don't match")

    joined_cmo_train = []
    for i in xrange(N_train):
        joined_cmo_train.append( np.array( list( set( np.concatenate((cmo1[0][i], cmo2[0][i])) ) ) ) )

    joined_cmo_test = []
    for i in xrange(N_cold_start):
        joined_cmo_test.append(  np.array( list( set( np.concatenate((cmo1[1][i], cmo2[1][i])) ) ) ) )

    return (joined_cmo_train, joined_cmo_test)


def generate_cubic_B_spline_basis_matrix(T, K):
    """ Generates a cubic B spline basis in matrix form with x = 1:T, K equally spaced knots (no intercept)

        The returned matrix B has dimension T x (K+3) indicating K+3 basis functions evaluated at 1:T
    """

    x = np.arange(1, T+1)
    B = dmatrix("bs(x, df=K+3, degree=3, include_intercept=False)", {"x": x, "K": K})
    B = np.array(B)

    if B.shape[1] == K+3+1:
        B = B[:, 1:]

    return B
    # return augment_basis_matrix_with_chopped_Fourier_basis_functions(B)


def step_size(t, model, SGD='False', delay=None):

    # Fixed step size
    # Tuned for synthetic data experiments
    if SGD:

        if type(delay) == list and delay[0] == 'fixed': # e.g. delay = ['fixed', 0.1]
            return delay[1]

        if delay is None:
            delay = 10 # Default delay
        forget_rate = 0.5
        return 1.0/(pow(t, forget_rate) + delay)

    else: # Fixed step size

        if model == 'reg':
            step_length = 0.01
        elif model == 'mf+reg':
            step_length = 0.1
        elif model == 'LowRankReg':
            step_length = 0.01
        elif model == 'mf+LowRankReg':
            step_length = 0.01
        return step_length


def get_col_sparse_matrix(M, i):
    """ 
    Get M[:,i] in 1D numpy array format

    :param M: scipy.sparse.csc_matrix
    :param i: index of column
    """

    return M[:,i].toarray().T[0]

def sparse_equality(v1, v2):
    """ Takes two scipy.sparse matrices (or vectors) and compares each element in a efficient fashion

        Returns True if v1 == v2, False otherwise
    """
    return (v1 != v2).nnz == 0

def append_bias_to_metadata(phi):
    N = phi.shape[1]
    if type(phi) == scipy.sparse.csc_matrix:
        return scipy.sparse.csc_matrix(scipy.sparse.vstack((np.ones(N), phi)))
    else:
        return vstack((np.ones(N), phi))

def cold_start_CV_folds(T, N_train, K):
    """ Returns indices of cold start """

    shuffled_indices = np.random.permutation(N_train)
    return np.array_split(shuffled_indices, K)

def missing_data_CV_folds(T, N, K, test_Omega, shuffle=True):
    """ Returns Omegas """

    list_of_Omegas = [[] for k in xrange(K)]
    for i in xrange(N):

        # Get rid of missing values in test
        if test_Omega is not None:
            test_missing_values = test_Omega[0][i]
        else:
            test_missing_values = []

        # Random split
        shuffled_indices = np.delete(np.arange(T), test_missing_values)
        if shuffle:
            np.random.shuffle(shuffled_indices)

        # Chunk into K splits, add to respective Omega
        split_indices = np.array_split(shuffled_indices, K)
        np.random.shuffle(split_indices)
        for k, arr in enumerate(split_indices):
            list_of_Omegas[k].append(arr)

    return list_of_Omegas


def construct_missing_data_masks(Y_train, Y_cold_start, Omega_train, completely_missing_Omega):
    """ Also alters Omega_train, completely_missing_Omega
    """

    N = Y_train.shape[1]

    # Handle missing values and create missing data mask. Also handle cross validation missing data vs. test set missing data
    no_train_missing_values = Omega_train is None
    if no_train_missing_values:
        Omega_train = [np.array([], dtype=int64) for i in xrange(N)]

    if completely_missing_Omega is None:
        completely_missing_Omega = [0, 0]
        completely_missing_Omega[0] = [np.array([]) for i in xrange(N)]
        if Y_cold_start is not None:
            completely_missing_Omega[1] = [np.array([]) for i in xrange(Y_cold_start.shape[1])]

    # Masks for Y_train
    missing_data_mask = np.ones(Y_train.shape) # 0's where data is missing (used for objective calculation)
    validation_missing_data_mask = np.zeros(Y_train.shape) # 1's where validation missing data is (used for test_missing_data_error)
    for i in xrange(N):
        if len(Omega_train[i]) > 0:
            missing_data_mask[Omega_train[i], i] = 0
            validation_missing_data_mask[Omega_train[i], i] = 1
        if len(completely_missing_Omega[0][i]) > 0: # This data is part of the test set
            missing_data_mask[completely_missing_Omega[0][i], i] = 0 # Validation error won't touch this data
            Omega_train[i] = np.concatenate((Omega_train[i], completely_missing_Omega[0][i])) # Gradients won't touch this data. We are assuming Omega_train and completely_missing_Omega are disjoint

    # Masks for Y_cold_start
    validation_cold_start_mask = None
    if Y_cold_start is not None:
        validation_cold_start_mask = np.ones(Y_cold_start.shape) # 1's where validation cold data is (used for test_cold_start_error)
        for i in xrange(Y_cold_start.shape[1]):
            if len(completely_missing_Omega[1][i]) > 0:
                validation_cold_start_mask[completely_missing_Omega[1][i], i] = 0 # Won't be counted in the cold start error (as if data is actually missing)

    return missing_data_mask, validation_missing_data_mask, validation_cold_start_mask, Omega_train, completely_missing_Omega


# Simple function to return the right optimization variables
def extract_variables(opt_vars, model):

        # Last opt var is bias
        return opt_vars

# Simple function to return the right regularization parameters
def extract_reg_parameters(reg_params, model):
    
    if model == 'reg':
        return reg_params[0]
    else:
        return reg_params

def full_objective(opt_vars, reg_params, Y_train, phi_train, missing_data_mask, model):

    N = Y_train.shape[1]

    if model == 'reg':
        W, b = extract_variables(opt_vars, model)
        lambda_W = extract_reg_parameters(reg_params, model)
        error = .5 * pow(np.linalg.norm(missing_data_mask.T * ( Y_train.T - phi_train.T.dot(W.T) - np.outer(np.ones(N), b) ), 'fro'), 2) # Use the scipy sparse dot product
        obj = error + .5 * lambda_W * pow(np.linalg.norm(W, 'fro'), 2)

    elif model == 'mf+reg':
        L, R, W, b = extract_variables(opt_vars, model)
        lambda_L, lambda_R, lambda_W = extract_reg_parameters(reg_params, model)
        error = .5 * pow(np.linalg.norm(missing_data_mask.T * ( Y_train.T - (L.dot(R)).T - phi_train.T.dot(W.T) - np.outer(np.ones(N), b) ), 'fro') , 2)
        obj = error + .5 * lambda_L * pow(np.linalg.norm(L, 'fro'), 2) + \
                      .5 * lambda_R * pow(np.linalg.norm(R, 'fro'), 2) + \
                      .5 * lambda_W * pow(np.linalg.norm(W, 'fro'), 2)

    elif model == 'LowRankReg':
        H, U, b = extract_variables(opt_vars, model)
        lambda_H, lambda_U = extract_reg_parameters(reg_params, model)
        error = .5 * pow(np.linalg.norm(missing_data_mask.T * ( Y_train.T -  phi_train.T.dot(U.T).dot(H.T) - np.outer(np.ones(N), b) ), 'fro'), 2)
        obj = error + .5 * lambda_H * pow(np.linalg.norm(H, 'fro'), 2) + \
                      .5 * lambda_U * pow(np.linalg.norm(U, 'fro'), 2)

    elif model == 'mf+LowRankReg':
        L, R, H, U, b = extract_variables(opt_vars, model)
        lambda_L, lambda_R, lambda_H, lambda_U = extract_reg_parameters(reg_params, model)
        error = .5 * pow(np.linalg.norm(missing_data_mask.T * ( Y_train.T -  (L.dot(R)).T - phi_train.T.dot(U.T).dot(H.T) - np.outer(np.ones(N), b) ), 'fro'), 2)
        obj = error + .5 * lambda_L * pow(np.linalg.norm(L, 'fro'), 2) + \
                      .5 * lambda_R * pow(np.linalg.norm(R, 'fro'), 2) + \
                      .5 * lambda_H * pow(np.linalg.norm(H, 'fro'), 2) + \
                      .5 * lambda_U * pow(np.linalg.norm(U, 'fro'), 2)

    return obj / float(N)


def test_missing_data_error(opt_vars, Y_train, phi_train, data_mask, model):
    """ Calculates Error for problem P2 """

    N_train = Y_train.shape[1]

    if model == 'reg':
        W, b = extract_variables(opt_vars, model)
        error = .5 * pow(np.linalg.norm(data_mask.T * ( Y_train.T - phi_train.T.dot(W.T) - np.outer(np.ones(N_train), b) ), 'fro'), 2)

    elif model == 'mf+reg':
        L, R, W, b = extract_variables(opt_vars, model)
        error = .5 * pow(np.linalg.norm(data_mask.T * ( Y_train.T - (L.dot(R)).T - phi_train.T.dot(W.T) - np.outer(np.ones(N_train), b) ), 'fro') , 2)

    elif model == 'LowRankReg':
        H, U, b = extract_variables(opt_vars, model)
        error = .5 * pow(np.linalg.norm(data_mask.T * ( Y_train.T -  phi_train.T.dot(U.T).dot(H.T) - np.outer(np.ones(N_train), b) ), 'fro'), 2)

    elif model == 'mf+LowRankReg':
        L, R, H, U, b = extract_variables(opt_vars, model)
        error = .5 * pow(np.linalg.norm(data_mask.T * ( Y_train.T -  (L.dot(R)).T - phi_train.T.dot(U.T).dot(H.T) - np.outer(np.ones(N_train), b) ), 'fro'), 2)        

    return error / float(N_train)


def test_cold_start_error(opt_vars, Y_test, phi_test, data_mask, model, mse_mae='mse'):
    """ Calculates Error for problems P1 and P3 """

    N_test = Y_test.shape[1]

    if model == 'reg':
        W, b = extract_variables(opt_vars, model)
        A = data_mask.T * ( Y_test.T - phi_test.T.dot(W.T) - np.outer(np.ones(N_test), b) )
        error = .5 * pow(np.linalg.norm(A, 'fro'), 2)

    elif model == 'mf+reg':
        L, R, W, b = extract_variables(opt_vars, model)
        A = data_mask.T * ( Y_test.T -  phi_test.T.dot(W.T) - np.outer(np.ones(N_test), b) )
        error = .5 * pow(np.linalg.norm(A, 'fro') , 2)

    elif model == 'LowRankReg':
        H, U, b = extract_variables(opt_vars, model)
        A = data_mask.T * ( Y_test.T -  phi_test.T.dot(U.T).dot(H.T) - np.outer(np.ones(N_test), b) )
        error = .5 * pow(np.linalg.norm(A, 'fro'), 2)

    elif model == 'mf+LowRankReg':
        L, R, H, U, b = extract_variables(opt_vars, model)
        A = data_mask.T * ( Y_test.T - phi_test.T.dot(U.T).dot(H.T) - np.outer(np.ones(N_test), b) )
        error = .5 * pow(np.linalg.norm(A, 'fro'), 2)

    if mse_mae == 'mse':
        return error / float(N_test)
    elif mse_mae == 'mae':
        return .5 * sum(abs(A)) / float(N_test)


def test_reconstruction_error(opt_vars, Y_train, phi_train, Y_test, phi_test, missing_data_mask, model):

    return test_missing_data_error(opt_vars, Y_train, phi_train, missing_data_mask, model) + test_cold_start_error(opt_vars, Y_test, phi_test, model)


def extract_reg_parameters_with_splines(reg_params, model):
    
    if model == 'reg':
        return None
    elif model == 'LowRankReg':
        return reg_params[0]
    else: # model == 'mf+reg' or model == 'mf+LowRankReg'
        return reg_params

def full_objective_with_splines(opt_vars, reg_params, B, Y_train, phi_train, missing_data_mask, model):

    N = Y_train.shape[1]

    if model == 'reg':
        Q, b = extract_variables(opt_vars, model)
        error = .5 * pow(np.linalg.norm(missing_data_mask.T * ( Y_train.T - phi_train.T.dot(Q.T).dot(B.T) - np.outer(np.ones(N), b) ), 'fro'), 2) # Use the scipy sparse dot product
        obj = error

    elif model == 'mf+reg':
        L, R, Q, b = extract_variables(opt_vars, model)
        lambda_L, lambda_R = extract_reg_parameters_with_splines(reg_params, model)
        error = .5 * pow(np.linalg.norm(missing_data_mask.T * ( Y_train.T - L.dot(R).T - phi_train.T.dot(Q.T).dot(B.T) - np.outer(np.ones(N), b) ), 'fro') , 2)
        obj = error + .5 * lambda_L * pow(np.linalg.norm(L, 'fro'), 2) + \
                      .5 * lambda_R * pow(np.linalg.norm(R, 'fro'), 2)

    elif model == 'LowRankReg':
        M, U, b = extract_variables(opt_vars, model)
        lambda_U = extract_reg_parameters_with_splines(reg_params, model)
        error = .5 * pow(np.linalg.norm(missing_data_mask.T * ( Y_train.T - phi_train.T.dot(U.T).dot(M.T).dot(B.T) - np.outer(np.ones(N), b) ), 'fro'), 2)
        obj = error + .5 * lambda_U * pow(np.linalg.norm(U, 'fro'), 2)

    elif model == 'mf+LowRankReg':
        L, R, M, U, b = extract_variables(opt_vars, model)
        lambda_L, lambda_R, lambda_U = extract_reg_parameters_with_splines(reg_params, model)
        error = .5 * pow(np.linalg.norm(missing_data_mask.T * ( Y_train.T - L.dot(R).T - phi_train.T.dot(U.T).dot(M.T).dot(B.T) - np.outer(np.ones(N), b) ), 'fro'), 2)
        obj = error + .5 * lambda_L * pow(np.linalg.norm(L, 'fro'), 2) + \
                      .5 * lambda_R * pow(np.linalg.norm(R, 'fro'), 2) + \
                      .5 * lambda_U * pow(np.linalg.norm(U, 'fro'), 2)

    return obj / float(N)


def test_missing_data_with_splines_error(opt_vars, B, Y_train, phi_train, data_mask, model):
    """ Calculates Error for problem P2 """

    N_train = Y_train.shape[1]

    if model == 'reg':
        Q, b = extract_variables(opt_vars, model)
        error = .5 * pow(np.linalg.norm(data_mask.T * ( Y_train.T - phi_train.T.dot(Q.T).dot(B.T) - np.outer(np.ones(N_train), b) ), 'fro'), 2)

    elif model == 'mf+reg':
        L, R, Q, b = extract_variables(opt_vars, model)
        error = .5 * pow(np.linalg.norm(data_mask.T * ( Y_train.T - L.dot(R).T - phi_train.T.dot(Q.T).dot(B.T) - np.outer(np.ones(N_train), b) ), 'fro') , 2)

    elif model == 'LowRankReg':
        M, U, b = extract_variables(opt_vars, model)
        error = .5 * pow(np.linalg.norm(data_mask.T * ( Y_train.T -  phi_train.T.dot(U.T).dot(M.T).dot(B.T) - np.outer(np.ones(N_train), b) ), 'fro'), 2)

    elif model == 'mf+LowRankReg':
        L, R, M, U, b = extract_variables(opt_vars, model)
        error = .5 * pow(np.linalg.norm(data_mask.T * ( Y_train.T -  L.dot(R).T - phi_train.T.dot(U.T).dot(M.T).dot(B.T) - np.outer(np.ones(N_train), b) ), 'fro'), 2)        

    return error / float(N_train)


def test_cold_start_with_splines_error(opt_vars, B, Y_test, phi_test, data_mask, model, mse_mae='mse'):
    """ Calculates Error for problems P1 and P3 """

    N_test = Y_test.shape[1]

    if model == 'reg':
        Q, b = extract_variables(opt_vars, model)
        A = data_mask.T * ( Y_test.T - phi_test.T.dot(Q.T).dot(B.T) - np.outer(np.ones(N_test), b) )
        error = .5 * pow(np.linalg.norm(A, 'fro'), 2)

    elif model == 'mf+reg':
        L, R, Q, b = extract_variables(opt_vars, model)
        A = data_mask.T * ( Y_test.T - phi_test.T.dot(Q.T).dot(B.T) - np.outer(np.ones(N_test), b) )
        error = .5 * pow(np.linalg.norm(A, 'fro') , 2)

    elif model == 'LowRankReg':
        M, U, b = extract_variables(opt_vars, model)
        A = data_mask.T * ( Y_test.T - phi_test.T.dot(U.T).dot(M.T).dot(B.T) - np.outer(np.ones(N_test), b) )
        error = .5 * pow(np.linalg.norm(A, 'fro'), 2)

    elif model == 'mf+LowRankReg':
        L, R, M, U, b = extract_variables(opt_vars, model)
        A = data_mask.T * ( Y_test.T - phi_test.T.dot(U.T).dot(M.T).dot(B.T) - np.outer(np.ones(N_test), b) )
        error = .5 * pow(np.linalg.norm(A, 'fro'), 2)

    if mse_mae == 'mse':
        return error / float(N_test)
    elif mse_mae == 'mae':
        return .5 * sum(abs(A)) / float(N_test)

def kNN_time_series_prediction(k, full_Y, full_phi, Y_cold_start, phi_cold_start, train_articles_indices, completely_missing_Omega, index_to_article, test_articles_indices):


    set_of_train_indices = set(train_articles_indices)
    error = 0
    predictions = np.empty((Y_cold_start.shape[0], 0))

    # full_phi has consecutive copies of same metadata vector (corresponding to different year of time series data)
    # Only consider the last one
    # Precompute the indices of the last of each metadata vector
    from time import time
    start_time = time()
    print "Precomputing some index stuff..."
    indices_of_last_year_of_metadata_vector = []
    for j in range(full_phi.shape[1]):
        if j < full_phi.shape[1]-1 and sparse_equality(full_phi[:,j], full_phi[:,j+1]):
            continue
        indices_of_last_year_of_metadata_vector.append(j)   
    end_time = time()
    print "Finished precomputation!"
    print "Time taken for precomputation: {0} seconds".format(round(end_time - start_time, 3))

    for i in range(phi_cold_start.shape[1]):

        if i % 10 == 0:
            print "k-NN iteration {0}".format(i)

        # Query point
        phi_i = phi_cold_start[:,i]

        distances = {}

        # Calculate distances to neighbors (of unique metadata vectors)
        for j in indices_of_last_year_of_metadata_vector:

            phi_j = full_phi[:,j]

            # Make sure this time series belongs to the training set
            if j not in set_of_train_indices:
                continue

            distances[j] = scipy.sparse.linalg.norm(phi_i - phi_j) # L2 (Frobenius) norm distance

        # Sort based on distance
        sorted_distances = sorted(distances.items(), key=lambda x: x[1])

        # Get best k
        prediction_series = np.zeros((full_Y.shape[0], k))
        for l in range(k):
            index = sorted_distances[l][0]

            # Average all previous years of this time series.
            num_datapoints_present_over_years = np.zeros(full_Y[:,index].shape)
            averaged_time_series_over_years = np.zeros(full_Y[:,index].shape)

            # Find where index is in train_articles_indices, and then use that to find mask in completely_missing_Omega
            index_in_train_articles = np.where(train_articles_indices == index)[0][0]
            if len(completely_missing_Omega[0][index_in_train_articles]) > 0:
                temp = full_Y[:, index].copy()
                datapoints_present = np.ones(temp.shape)
                datapoints_present[completely_missing_Omega[0][index_in_train_articles]] = 0
                averaged_time_series_over_years += temp * datapoints_present
                num_datapoints_present_over_years += datapoints_present # Add 1 for each day if there is a datapoint that is present
            else:
                averaged_time_series_over_years += full_Y[:, index]
                num_datapoints_present_over_years += 1

            while index > 0 and sparse_equality(full_phi[:, index], full_phi[:, index-1]):
                index -= 1

                # Find where index is in train_articles_indices, and then use that to find mask in completely_missing_Omega
                index_in_train_articles = np.where(train_articles_indices == index)[0][0]
                if len(completely_missing_Omega[0][index_in_train_articles]) > 0:
                    temp = full_Y[:, index].copy()
                    datapoints_present = np.ones(temp.shape)
                    datapoints_present[completely_missing_Omega[0][index_in_train_articles]] = 0
                    averaged_time_series_over_years += temp * datapoints_present
                    num_datapoints_present_over_years += datapoints_present # Add 1 for each day if there is a datapoint that is present
                else:
                    averaged_time_series_over_years += full_Y[:, index]
                    num_datapoints_present_over_years += 1

            averaged_time_series_over_years /= (num_datapoints_present_over_years + 1e-10) # + 1e-10 b/c if no datapoints for a specific day, it's 0/0.

            prediction_series[:,l] = averaged_time_series_over_years

        # Calculate normalized distance weights
        weights = np.array([x[1] for x in sorted_distances[:k]])
        weights /= sum(weights)

        # Weighted k-NN prediction
        prediction = prediction_series.dot(weights)

        # Calculate error from this prediction
        difference = Y_cold_start[:, i] - prediction
        # if len(completely_missing_Omega[1][i]) > 0:
        #     difference[completely_missing_Omega[1][i]] = 0 # This data is missing
        error += pow(np.linalg.norm(difference, 2), 2)

        predictions = np.concatenate((predictions, prediction[:, np.newaxis]), axis=1)

        # Plot to see what's happening here
        # article = index_to_article[test_articles_indices[i]]

        # fig = figure()
        # fig.clf()
        # fig.suptitle(article)

        # subplot(2, 1, 1)
        # plot(Y_cold_start[:,i], color='magenta')
        # plot(prediction, linewidth=2, color='green' )
        # xlim([0, 365])
        # locator_params(axis='y',nbins=3)
        # locator_params(axis='x',nbins=5)

        # subplot(2, 1, 2)
        # plot(prediction, linewidth=2, color='green' )
        # xlim([0, 365])
        # locator_params(axis='y',nbins=3)
        # locator_params(axis='x',nbins=5)

        # subplots_adjust(hspace=0.2)

        # savefig('Images/Real Data Experiments/Pure_Baseline/' + article + '.pdf', bbox_inches='tight')
        # close()


    np.savetxt('opt_dumps/kNN_coldstart_predictions', predictions)

    # Multiply by 1 / (2N)
    return error / (2 * phi_cold_start.shape[1])


def OOS_baseline_average_past_years(full_Y, full_phi, train_articles_indices, test_articles_indices, completely_missing_Omega, index_to_article):
    """ For each test article (index), get the previous years and average them

        test_articles_indices are indices of test set w.r.t. original Y matrix (full_Y)
    """
    error = 0
    predictions = np.empty((full_Y.shape[0], 0))

    for long_range_index, i in enumerate(test_articles_indices):

        if long_range_index % 10 == 0:
            print "OOS baseline iteration {0}".format(long_range_index)

        # Query point
        phi_i = full_phi[:,i]

        # Make sure there is at least 1 previous year
        if not sparse_equality(phi_i, full_phi[:, i-1]):
            raise Exception("This test article does not have any previous years in the training set")

        # Average all previous years of this time series.
        index = i-1
        num_datapoints_present_over_years = np.zeros(full_Y[:,index].shape)
        averaged_time_series_over_years = np.zeros(full_Y[:,index].shape)

        # Find where index is in train_articles_indices, and then use that to find mask in completely_missing_Omega
        index_in_train_articles = np.where(train_articles_indices == index)[0][0]
        if len(completely_missing_Omega[0][index_in_train_articles]) > 0:
            temp = full_Y[:, index].copy()
            datapoints_present = np.ones(temp.shape)
            datapoints_present[completely_missing_Omega[0][index_in_train_articles]] = 0
            averaged_time_series_over_years += temp * datapoints_present
            num_datapoints_present_over_years += datapoints_present # Add 1 for each day if there is a datapoint that is present
        else:
            averaged_time_series_over_years += full_Y[:, index]
            num_datapoints_present_over_years += 1

        while index > 0 and sparse_equality(full_phi[:, index], full_phi[:, index-1]):
            index -= 1

            # Find where index is in train_articles_indices, and then use that to find mask in completely_missing_Omega
            index_in_train_articles = np.where(train_articles_indices == index)[0][0]
            if len(completely_missing_Omega[0][index_in_train_articles]) > 0:
                temp = full_Y[:, index].copy()
                datapoints_present = np.ones(temp.shape)
                datapoints_present[completely_missing_Omega[0][index_in_train_articles]] = 0
                averaged_time_series_over_years += temp * datapoints_present
                num_datapoints_present_over_years += datapoints_present # Add 1 for each day if there is a datapoint that is present
            else:
                averaged_time_series_over_years += full_Y[:, index]
                num_datapoints_present_over_years += 1

        averaged_time_series_over_years /= (num_datapoints_present_over_years + 1e-10) # + 1e-10 b/c if no datapoints for a specific day, it's 0/0.

        prediction = averaged_time_series_over_years

        # Calculate error from this prediction
        difference = full_Y[:, i] - prediction
        # if len(completely_missing_Omega[1][long_range_index]) > 0:
        #     difference[completely_missing_Omega[1][long_range_index]] = 0 # This data is missing
        error += pow(np.linalg.norm(difference, 2), 2)

        predictions = np.concatenate((predictions, prediction[:, np.newaxis]), axis=1)


        # Plot to see what's happening here
        # article = index_to_article[i]

        # fig = figure()
        # fig.clf()
        # fig.suptitle(article)

        # subplot(2, 1, 1)
        # plot(full_Y[:,i], color='magenta')
        # plot(prediction, linewidth=2, color='green' )
        # xlim([0, 365])
        # locator_params(axis='y',nbins=3)
        # locator_params(axis='x',nbins=5)

        # subplot(2, 1, 2)
        # plot(prediction, linewidth=2, color='green' )
        # xlim([0, 365])
        # locator_params(axis='y',nbins=3)
        # locator_params(axis='x',nbins=5)

        # subplots_adjust(hspace=0.2)

        # savefig('Images/Real Data Experiments/OOS_Baseline/' + article + '.pdf', bbox_inches='tight')
        # close()

    np.savetxt('opt_dumps/avg_past_years_longrange_predictions', predictions)

    # Multiply by 1 / (2N)
    return error / (2 * len(test_articles_indices))

    