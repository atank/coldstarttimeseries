from __future__ import division
from pylab import *
from IPython import embed
from util import *

"""
    Returns a T x N matrix of time series 
    and a m x N matrix of metadata
    k is the latent dimension of the series
"""

# Possible models:
possible_models = {'reg', 'mf+reg', 'LowRankReg', 'mf+LowRankReg'}

density = 0.02 # 0.002 for large data, 0.02 for medium data, 0.1 for small data

def gen_series_reg(T, N, m, k):

    pass

def gen_series_mf_reg(T, N, m, k):
    pass

def gen_series_LowRankReg(T, N, m, k):
    pass

def gen_series_mf_LowRankReg(T, N, m, k):

    # Generate basis functions
    L = np.zeros((T,k))
    for i in xrange(k):
        perturb = np.random.normal(0,1)
        for j in xrange(T):
            L[j,i] = np.sin(j*( (i+1+perturb) *2.0*np.pi)/T)+1

    H = np.zeros((T,k))
    for i in xrange(k):
        perturb = np.random.normal(0,1)
        for j in xrange(T):
            H[j,i] = np.sin(j*( (i+1+perturb) *2.0*np.pi)/T)+1

    # L += .05 * np.random.randn(T,k)
    # H += .05 * np.random.randn(T,k)

    # Generate factor loadings
    R = np.random.normal(0, .3 * 1/float(k), [k, N])

    # Generate linear transformation of metadata
    U = np.random.normal(0, 1/float(k), [k, m])

    # Generate metadata
    phi = np.random.exponential(.2, [m,N]) * np.random.binomial(1, density, [m,N]) # Exponential mean is .2, expected density of phi is .1
    # Make sure not every column is all 0
    for i in xrange(N):
        while np.count_nonzero(phi[:,i]) == 0:
            phi[:,i] = np.random.exponential(.2, [m,]) * np.random.binomial(1, density, [m,])

    Y = L.dot(R) + H.dot(U).dot(phi) + .2 * np.random.randn(T,N)

    return Y, phi, L, R, H, U

def gen_series_reg_plus_sparse_noise(T, N, m):

    # Generate basis functions
    L = np.zeros((T,T))
    for i in xrange(T):
        perturb = np.random.normal(0,1)
        for j in xrange(T):
            L[j,i] = np.sin(j*( (i+1+perturb) *2.0*np.pi)/T)+1

    H = np.zeros((T,T))
    for i in xrange(T):
        perturb = np.random.normal(0,1)
        for j in xrange(T):
            H[j,i] = np.sin(j*( (i+1+perturb) *2.0*np.pi)/T)+1

    # Generate factor loadings
    R = np.random.normal(0, .3 * 1/float(T), [T, N])

    # Generate linear transformation of metadata
    U = np.random.normal(0, 1/float(T), [T, m])

    # Generate metadata
    phi = np.random.exponential(.2, [m,N]) * np.random.binomial(1, density, [m,N]) # Exponential mean is .2, expected density of phi is .1
    # Make sure not every column is all 0
    for i in xrange(N):
        while np.count_nonzero(phi[:,i]) == 0:
            phi[:,i] = np.random.exponential(.2, [m,]) * np.random.binomial(1, density, [m,])

    L_dot_R = L.dot(R)
    sparse_noise = L_dot_R * np.random.binomial(1, .2, [T,N]) # Remove 80% of the values
    for i in xrange(N):
        while np.count_nonzero(sparse_noise[:,i]) == 0:
            sparse_noise[:,i] = L_dot_R[:,i] * np.random.binomial(1, .2, [T,])

    Y = sparse_noise + H.dot(U).dot(phi) + .2 * np.random.randn(T,N)

    return Y, phi, L, R, H, U

def gen_series_mf_reg_spline(T, N, m, k):

    # Generate basis functions
    L = np.zeros((T,k))
    for i in xrange(k):
        perturb = np.random.normal(0,1)
        for j in xrange(T):
            L[j,i] = np.sin(j*( (i+1+perturb) *2.0*np.pi)/T)+1

    num_knots = 50
    B = generate_cubic_B_spline_basis_matrix(T, num_knots)

    # Generate factor loadings
    R = np.random.normal(0, .3 * 1/float(k), [k, N])

    # Generate linear transformation of metadata
    Q = np.random.normal(0, 4/float(k), [B.shape[1], m])

    # Generate metadata
    phi = np.random.exponential(.2, [m,N]) * np.random.binomial(1, density, [m,N]) # Exponential mean is .2, expected density of phi is .1
    # Make sure not every column is all 0
    for i in xrange(N):
        while np.count_nonzero(phi[:,i]) == 0:
            phi[:,i] = np.random.exponential(.2, [m,]) * np.random.binomial(1, density, [m,])

    Y = L.dot(R) + B.dot(Q).dot(phi) + .1 * np.random.randn(T,N)

    return Y, phi, L, R, B, Q

def gen_series_mf_reg_GP(T, N, m, k):

    # Generate basis functions
    L = np.zeros((T,k))
    for i in xrange(k):
        perturb = np.random.normal(0,1)
        for j in xrange(T):
            L[j,i] = np.sin(j*( (i+1+perturb) *2.0*np.pi)/T)+1

    # Generate basis functions by sampling from a GP 
    num_basis_functions = 50
    B = np.zeros((T, num_basis_functions))
    time_points = np.arange(1, T+1)[:, np.newaxis]
    # mean function is just 0
    mean_func = np.zeros(T)
    # double exponential kernel
    tau = 5
    K_x = np.exp(- 0.5 / np.square(tau) * np.square(time_points - time_points.T)) # eigvals are almost 0 since tau is so large, hopefully that's okay numerically for simulation
    for i in xrange(num_basis_functions):
        B[:, i] = np.random.multivariate_normal(np.zeros(T), K_x)

    # Generate factor loadings
    R = np.random.normal(0, .3 * 1/float(k), [k, N])

    # Generate linear transformation of metadata
    Q = np.random.normal(0, .4/float(k), [B.shape[1], m])

    # Generate metadata
    phi = np.random.exponential(.2, [m,N]) * np.random.binomial(1, density, [m,N]) # Exponential mean is .2, expected density of phi is .1
    # Make sure not every column is all 0
    for i in xrange(N):
        while np.count_nonzero(phi[:,i]) == 0:
            phi[:,i] = np.random.exponential(.2, [m,]) * np.random.binomial(1, density, [m,])

    Y = L.dot(R) + B.dot(Q).dot(phi) + .1 * np.random.randn(T,N)

    return Y, phi, L, R, B, Q

def gen_series(T, N, m, k, model='mf+LowRankReg'):
    """ Generate synthetic time series data based on model """

    if model == 'reg':
        return gen_series_reg(T, N, m, k)
    elif model == 'mf+reg':
        return gen_series_mf_reg(T, N, m, k)
    elif model == 'LowRankReg':
        return gen_series_LowRankReg(T, N, m, k)
    elif model == 'mf+LowRankReg':
        return gen_series_mf_LowRankReg(T, N, m, k)
    elif model == 'reg+sparse_noise':
        return gen_series_reg_plus_sparse_noise(T, N, m)
    elif model == 'mf+reg_spline':
        return gen_series_mf_reg_spline(T, N, m, k)
    elif model == 'mf+reg_GP':
        return gen_series_mf_reg_GP(T, N, m, k)


def gen_Omega(T, N, test_Omega, percentage):
    """ Used as a proof of concept. Basically generates a training/validation (index) set
    """

    Omega = []
    for i in xrange(N):

        if test_Omega is not None:
            test_missing_values = test_Omega[0][i] # Training test Omega
        else:
            test_missing_values = []
        shuffled_indices = np.delete(np.arange(T), test_missing_values)
        Omega.append( np.random.choice(shuffled_indices, size=int(percentage * len(shuffled_indices)), replace=False) )

    return Omega

def gen_completely_missing_Omega(T, N_train, N_cold_start, percentage):
    """ Generate missing values for Y_train and Y_cold_start

        Used for synthetic data settings
    """

    Omega_train = []
    for i in xrange(N_train):
        Omega_train.append( np.random.choice(T, size=int(percentage * T), replace=False) )

    Omega_cold_start = []

    for i in xrange(N_cold_start):
        Omega_cold_start.append( np.random.choice(T, size=int(percentage * T), replace=False) )

    return [Omega_train, Omega_cold_start]

def gen_uniformly_missing_test_set(T, N_train, percentage):
    """ Returns a test_Omega for the uniformly random missing data experiments 

        No cold start in this setup, so N_cold_start = 0
    """
    return gen_completely_missing_Omega(T, N_train, 0, percentage)

def gen_large_missing_chunks_test_set(T, N, percentage):
    """ Generates a large missing chunk from each time series

        For missing data experiments
    """

    avg_missing = int(percentage * T)
    max_missing_from_a_series = int(2*T / 3) # most that can be missing from a series is 2/3 of it
    
    Omega_train = []
    for i in xrange(N):
        start_missing = np.random.randint(0, T)
        end_missing = min(start_missing + np.random.geometric(p = 1. / avg_missing), T)
        while end_missing - start_missing > max_missing_from_a_series:
            end_missing = min(start_missing + np.random.geometric(p = 1. / avg_missing), T)
        Omega_train.append( np.arange(start_missing, end_missing) )

    Omega_cold_start = [] # Omega_cold_start is nothing

    return [Omega_train, Omega_cold_start]

def gen_warm_start_test_set(T, N_train, N_cold_start, K, num_present = 60):
    """ Generates a warm start missing index

        Also generates warm start CV folds
    """

    # max_start = int(T * percentage) # maximum starting point of missing ness
    
    # Completely missing Omega
    Omega_train = []
    for i in xrange(N_train):
        Omega_train.append(np.array([], dtype=int64))
    for i in xrange(N_cold_start):
        Omega_train.append( np.arange(num_present, T) )

    Omega_cold_start = [] # Omega_cold_start is nothing

    completely_missing_Omega = [Omega_train, Omega_cold_start]

    # CV Folds: randomly separate remaining examples
    list_of_Omegas_for_CV_folds = [[np.array([], dtype=int64) for i in xrange(N_train)] for k in xrange(K)]
    for i in xrange(N_train):
        k = np.random.randint(0, K)
        list_of_Omegas_for_CV_folds[k][i] = np.arange(num_present, T)

    return completely_missing_Omega, list_of_Omegas_for_CV_folds


def gen_different_beginning_test_set(N_train):
    """ Returns a test_Omega for the different beginnings missing data experiments 

        No cold start in this setup, so N_cold_start = 0
    """

    avg_missing = 15

    Omega_train = []
    for i in xrange(N_train):
        # Sample from a geometric distribution
        num_missing = np.random.geometric(p = 1. / avg_missing)
        Omega_train.append( np.arange(num_missing) )

    # Make sure at least a few time series start from very beginning
    avg_full = 50
    num_full = np.random.geometric(p = 1. / avg_full)
    indices_of_num_full = np.random.choice(N_train, size=num_full, replace=False)
    for i in indices_of_num_full:
        Omega_train[i] = np.array([])

    Omega_cold_start = [] # Omega_cold_start is nothing

    return [Omega_train, Omega_cold_start]


def gen_pure_cold_start(T, N_train, N_cold_start, m, k, model='mf+LowRankReg'):
    """ Generate pure cold start synthetic dataset.

        Generate phi's randomly. No same phi's.
    """

    # First generate all data
    Y, phi, L, R, H, U = gen_series(T, N_train + N_cold_start, m, k, model=model)

    # Next, split into training and test. All phi's are different
    Y_train = Y[:, : N_train]
    phi_train = phi[:, : N_train]
    Y_cold_start = Y[:, N_train : ]
    phi_cold_start = phi[:, N_train : ]

    # Double check
    if Y_cold_start.shape[1] != N_cold_start or phi_cold_start.shape[1] != N_cold_start:
        raise Exception('Y_cold_start and phi_cold_start need to be length {0}'.format(N_cold_start))

    return Y_train, phi_train, Y_cold_start, phi_cold_start, L, R, H, U


def gen_OOS_cold_start(T, num_phis, K, m, k, model=None):
    """ Generate OOS cold start synthetic dataset. Also generate CV folds
    """
    if model == 'reg+sparse_noise':
        k = T

    # Generate basis functions
    L = np.zeros((T,k))
    for i in xrange(k):
        perturb = np.random.normal(0,1)
        for j in xrange(T):
            L[j,i] = np.sin(j*( (i+1+perturb) *2.0*np.pi)/T)+1

    H = np.zeros((T,k))
    for i in xrange(k):
        perturb = np.random.normal(0,1)
        for j in xrange(T):
            H[j,i] = np.sin(j*( (i+1+perturb) *2.0*np.pi)/T)+1

    # Generate factor loadings
    R = np.random.normal(0, .3 * 1/float(k), [k, num_phis * (K+1)])

    # Generate linear transformation of metadata
    U = np.random.normal(0, 1/float(k), [k, m])

    # Generate metadata
    phi = np.random.exponential(.2, [m,num_phis]) * np.random.binomial(1, density, [m,num_phis]) # Exponential mean is .2

    # Make sure not every column is all 0
    for i in xrange(num_phis):
        while np.count_nonzero(phi[:,i]) == 0:
            phi[:,i] = np.random.exponential(.2, [m,]) * np.random.binomial(1, density, [m,])

    # Duplicate the phis
    phi = tile(phi, [1, K+1])
    # Using this, generate the CV folds
    CV_folds = np.array_split(np.arange(num_phis * K), K) # Only use the first K repeats for CV folds.

    Y = L.dot(R) + H.dot(U).dot(phi) + .2 * np.random.randn(T,num_phis * (K+1))

    if model == 'reg+sparse_noise':
        L_dot_R = L.dot(R)
        sparse_noise = L_dot_R * np.random.binomial(1, .2, [T,num_phis * (K+1)]) # Remove 80% of the values
        for i in xrange(num_phis * (K+1)):
            while np.count_nonzero(sparse_noise[:,i]) == 0:
                sparse_noise[:,i] = L_dot_R[:,i] * np.random.binomial(1, .2, [T,])

        Y = sparse_noise + H.dot(U).dot(phi) + .2 * np.random.randn(T,num_phis * (K+1))

    if model == 'mf+reg_spline':
        num_knots = 50
        H = generate_cubic_B_spline_basis_matrix(T, num_knots) # B
        U = np.random.normal(0, 4/float(k), [H.shape[1], m]) # Q
        Y = L.dot(R) + H.dot(U).dot(phi) + .1 * np.random.randn(T,num_phis * (K+1))

    if model == 'mf+reg_GP':
        # Generate basis functions by sampling from a GP 
        num_basis_functions = 50
        B = np.zeros((T, num_basis_functions))
        time_points = np.arange(1, T+1)[:, np.newaxis]
        # mean function is just 0
        mean_func = np.zeros(T)
        # double exponential kernel
        tau = 5
        K_x = np.exp(- 0.5 / np.square(tau) * np.square(time_points - time_points.T)) # eigvals are almost 0 since tau is so large, hopefully that's okay numerically for simulation
        for i in xrange(num_basis_functions):
            B[:, i] = np.random.multivariate_normal(np.zeros(T), K_x)
        H = B

        Q = np.random.normal(0, .4/float(k), [B.shape[1], m])
        U = Q

        Y = L.dot(R) + H.dot(U).dot(phi) + .1 * np.random.randn(T,num_phis * (K+1))

    Y_train = Y[:, :num_phis*K]
    phi_train = phi[:, :num_phis*K]
    Y_cold_start = Y[:, num_phis*K : ]
    phi_cold_start= phi[:, num_phis*K : ]

    return Y_train, phi_train, Y_cold_start, phi_cold_start, CV_folds, L, R, H, U


def segment_articles_into_OOS_CV_folds(Y_dataframe, phi, K):

    # Find each type of article page and the years associated with it
    article_names_with_years = Y_dataframe.columns.tolist()
    article_names_year_list = {}
    for entire_name in article_names_with_years:
        temp = entire_name.split(' ')
        year = temp[-1]
        name = ' '.join(temp[:-1])
        if name not in article_names_year_list:
            article_names_year_list[name] = []
        article_names_year_list[name].append(year)

    # Pages with this many years: 1      2   3    4    5    6     7
    #                             219    2   68   62   58   94    3937

    # Simultaneously grab a test set and generate CV folds
    test_articles_names = np.array([], dtype=str) # ['Toast 2014', 'LeBron James 2014', ...]
    CV_folds_by_article_names = [np.array([], dtype=str) for k in xrange(K)] # [['Toast 2013', 'LeBron James 2013', ...], ['Toast 2012', 'LeBron James 2012', ...], ..]
    for article in article_names_year_list:

        # Drop the set of articles if it doesn't have enough years
        if K+1 > len(article_names_year_list[article]):
            continue

        ind_to_grab = -1
        new_test_article_name = article + ' ' + article_names_year_list[article][ind_to_grab]

        # Split the remaining years evenly
        # temp = np.array_split(article_names_year_list[article], K+1)
        temp = np.array_split(np.delete(article_names_year_list[article], ind_to_grab), K)

        # Add the year string back onto the article name (e.g. 'Toast 2013'), then add those to the test set of articles
        # new_test_article_names = np.array([article + ' ' + year for year in temp[K]]) # Last couple years for test set
        # test_articles_names = np.concatenate((test_articles_names, new_test_article_names)) 
        test_articles_names = np.concatenate((test_articles_names, [new_test_article_name])) 

        # Do the same for each fold for CV
        for k in xrange(K):
            new_fold_article_names = np.array([article + ' ' + year for year in temp[k]])
            CV_folds_by_article_names[k] = np.concatenate((CV_folds_by_article_names[k], new_fold_article_names))

    # Useful indices
    index_to_article = {i : article for i, article in enumerate(Y_dataframe.columns)}
    article_to_index = {article : i for i, article in enumerate(Y_dataframe.columns)}

    # Grab indices to create Y_train, phi_train, Y_cold_start, phi_cold_start
    train_articles_names = np.concatenate(CV_folds_by_article_names)
    test_articles_indices = np.array([article_to_index[article] for article in test_articles_names])
    train_article_indices = np.array([article_to_index[article] for article in train_articles_names])

    # Get training data
    Y_train = Y_dataframe[train_articles_names].values
    phi_train = phi[:, train_article_indices]

    # Get test data
    Y_cold_start = Y_dataframe[test_articles_names].values
    phi_cold_start = phi[:, test_articles_indices]
    
    # CV folds:
    CV_folds = []
    i = 0
    for k in xrange(K):
        len_of_fold_k = len(CV_folds_by_article_names[k])
        CV_folds.append(np.arange(i, i + len_of_fold_k))
        i += len_of_fold_k

    print "Number of time series in training set: {0}".format(Y_train.shape[1])
    print "Number of time series in test set: {0}".format(Y_cold_start.shape[1])
    print "Total time series:{0} ".format(Y_train.shape[1] + Y_cold_start.shape[1])
    return Y_train, phi_train, Y_cold_start, phi_cold_start, CV_folds, train_article_indices, test_articles_indices, index_to_article



def segment_articles_into_pure_cold_start_train_test(Y_dataframe, phi):

    # Find each type of article page and the years associated with it
    article_names_with_years = Y_dataframe.columns.tolist()
    article_names_year_list = {}
    for entire_name in article_names_with_years:
        temp = entire_name.split(' ')
        year = temp[-1]
        name = ' '.join(temp[:-1])
        if name not in article_names_year_list:
            article_names_year_list[name] = []
        article_names_year_list[name].append(year)

    # Randomly grab test set
    percentage = 0.25 # Results in 1110 time series for test set
    num_test_series = int(len(article_names_year_list) * percentage)
    rand_perm_of_article_names = np.random.permutation(article_names_year_list.keys())

    # Extract test time series. If they have previous years, don't use them in training set
    test_articles_names_without_years = rand_perm_of_article_names[ : num_test_series]
    test_articles_names = np.array([], dtype=str)
    for article in test_articles_names_without_years:
        article_with_year = article + ' ' + article_names_year_list[article][-1] # Get latest year for test set
        test_articles_names = np.concatenate((test_articles_names, [article_with_year]))

    # Extract training time series (all years)
    train_articles_names_without_years = rand_perm_of_article_names[num_test_series : ]
    train_articles_names = np.array([], dtype=str)
    for article in train_articles_names_without_years:

        # Add the year string back onto the article name (e.g. 'Toast 2013'), then add those to the training set of articles
        new_train_article_names = np.array([article + ' ' + year for year in article_names_year_list[article]]) 
        train_articles_names = np.concatenate((train_articles_names, new_train_article_names)) 

    # Useful indices
    index_to_article = {i : article for i, article in enumerate(Y_dataframe.columns)}
    article_to_index = {article : i for i, article in enumerate(Y_dataframe.columns)}

    # Using those indices, map train and test article names to indices
    train_articles_indices = np.array([article_to_index[article] for article in train_articles_names])
    test_articles_indices = np.array([article_to_index[article] for article in test_articles_names])

    # Get training data
    Y_train = Y_dataframe[train_articles_names].values
    phi_train = phi[:, train_articles_indices]

    # Get test data
    Y_cold_start = Y_dataframe[test_articles_names].values
    phi_cold_start = phi[:, test_articles_indices]

    print "Number of time series in training set: {0}".format(Y_train.shape[1])
    print "Number of time series in test set: {0}".format(Y_cold_start.shape[1])
    print "Total time series:{0} ".format(Y_train.shape[1] + Y_cold_start.shape[1])
    return Y_train, phi_train, Y_cold_start, phi_cold_start, train_articles_indices, test_articles_indices, index_to_article











