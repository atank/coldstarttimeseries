import pandas as pd
import pickle
import sys
import rpy2.robjects as robjects
import scipy
import scipy.sparse
from rpy2.robjects.packages import importr

from pylab import *
from IPython import embed
from numpy import ceil, asarray


def stl(data, ns, np=None, nt=None, nl=None, isdeg=0, itdeg=1, ildeg=1,
        nsjump=None, ntjump=None, nljump=None, ni=2, no=0):
    """
    Seasonal-Trend decomposition procedure based on LOESS
    data : pandas.Series
    ns : int
        Length of the seasonal smoother.
        The value of  ns should be an odd integer greater than or equal to 3.
        A value ns>6 is recommended. As ns  increases  the  values  of  the
        seasonal component at a given point in the seasonal cycle (e.g., January
        values of a monthly series with  a  yearly cycle) become smoother.
    np : int
        Period of the seasonal component.
        For example, if  the  time series is monthly with a yearly cycle, then
        np=12.
        If no value is given, then the period will be determined from the
        ``data`` timeseries.
    nt : int
        Length of the trend smoother.
        The  value  of  nt should be an odd integer greater than or equal to 3.
        A value of nt between 1.5*np and 2*np is  recommended. As nt increases,
        the values of the trend component become  smoother.
        If nt is None, it is estimated as the smallest odd integer greater
        or equal to ``(1.5*np)/[1-(1.5/ns)]``
    nl : int
        Length of the low-pass filter.
        The value of nl should  be an odd integer greater than or equal to 3.
        The smallest odd integer greater than or equal to np is used by default.
    isdeg : int
        Degree of locally-fitted polynomial in seasonal smoothing.
        The value is 0 or 1.
    itdeg : int
        Degree of locally-fitted polynomial in trend smoothing.
        The value is 0 or 1.
    ildeg : int
        Degree of locally-fitted polynomial in low-pass smoothing.
        The value is 0 or 1.
    nsjump : int
        Skipping value for seasonal smoothing.
        The seasonal smoother skips ahead nsjump points and then linearly
        interpolates in between.  The value  of nsjump should be a positive
        integer; if nsjump=1, a seasonal smooth is calculated at all n points.
        To make the procedure run faster, a reasonable choice for nsjump is
        10%-20% of ns. By default, nsjump= 0.1*ns.
    ntjump : int
        Skipping value for trend smoothing. If None, ntjump= 0.1*nt
    nljump : int
        Skipping value for low-pass smoothing. If None, nljump= 0.1*nl
    ni :int
        Number of loops for updating the seasonal and trend  components.
        The value of ni should be a positive integer.
        See the next argument for advice on the  choice of ni.
        If ni is None, ni is set to 2 for robust fitting, to 5 otherwise.
    no : int
        Number of iterations of robust fitting. The value of no should
        be a nonnegative integer. If the data are well behaved without
        outliers, then robustness iterations are not needed. In this case
        set no=0, and set ni=2 to 5 depending on how much security
        you want that  the seasonal-trend looping converges.
        If outliers are present then no=3 is a very secure value unless
        the outliers are radical, in which case no=5 or even 10 might
        be better.  If no>0 then set ni to 1 or 2.
        If None, then no is set to 15 for robust fitting, to 0 otherwise.
    fulloutput : bool
        If True, a dictionary holding the full output of the original R routine
        will be returned.
    returns
    data : pandas.DataFrame
        The seasonal, trend, and remainder components
    """


    ts_ = robjects.r['ts']
    stl_ = robjects.r['stl']
    zoo_ = importr("zoo")
    naaction_ = robjects.r['na.approx']

    # find out the period of the time series
    if np is None:
        print "MUST specify np..."
        raise NotImplementedError()

    # fill default values
    if nt is None:
        nt = ceil((1.5 * np) / (1 - (1.5 / ns)))
    nt = nt + 1 if nt % 2 == 0 else nt

    if nl is None:
        nl = np if np % 2 is 1 else np + 1
    if nsjump is None:
        nsjump = ceil(ns / 10.)
    if ntjump is None:
        ntjump = ceil(nt / 10.)
    if nljump is None:
        nljump = ceil(nl / 10.)

    if nt is None:
        nt = robjects.rinterface.R_NilValue

    # Convert data to R object
    ts = ts_(robjects.FloatVector(asarray(data)), frequency=np)

    # Call STL
    result = stl_(ts, ns, isdeg, nt, itdeg, nl, ildeg, nsjump, ntjump, nljump,
                  False, ni, no, naaction_)

    res_ts = asarray(result[0])
    try:
        res_ts = pd.DataFrame({"seasonal" : pd.Series(res_ts[:,0],
                                                           index=data.index),
                                   "trend" : pd.Series(res_ts[:,1],
                                                           index=data.index),
                                   "remainder" : pd.Series(res_ts[:,2],
                                                           index=data.index)})
    except:
        pass

    return res_ts


def main():

    ### BEGIN LOAD DATA ###

    args = sys.argv[1:]

    # Load the data in, don't forget about the index column
    data = pd.read_csv(args[0], index_col=0)
    # data = data.drop(['2008/02/29', '2012/02/29']) # Drop these dates to preserve periodicity
    data = data.ix[:365*7+2] # Beginning of 2008 to end of 2014 (7 years total, with 2 leap years). Drop the rest of the data (first 7 months of 2015)

    # Load the metadata in
    metadata = pickle.load(open(args[1], 'rb'))
    article_names = metadata.columns.tolist()

    ### END LOAD DATA ###




    ### BEGIN DETREND DATA ###
    print 'BEGIN: Detrending Time Series Data...'

    # Only grab the relevant articles, which is prescribed by the list that we pass in as the second argument
    detrended_data = pd.DataFrame(index=data.index, columns=article_names)
    # means = pd.DataFrame(index=['means'], columns=article_names)
    # stddevs = pd.DataFrame(index=['stddev'], columns=article_names)

    # Of these guys, about half of them can't be put into STL because they only have 365 days or so.
    i = 1
    for article in article_names:
        if i % 50 == 0:
            print "Decomposing article {0}...".format(i)
        try:                              
            prod_decomp = stl(data[article], 7, np=365)
            if sum(np.isnan(data[article].values)) > 0: # Hack because I know what happens when there are NaNs in data
                indices = data.index[~np.isnan(data[article].values)]
                detrended_article_data = prod_decomp[:,0] + prod_decomp[:,2] # Seasonal + Remainder, this is the same as subtracting the trend from the whole data
                detrended_data[article] = pd.Series(index=indices, data=detrended_article_data)
                # resulting thing will have NaNs in data
            else:
                detrended_article_data = prod_decomp['seasonal'] + prod_decomp['remainder']
                detrended_data[article] = detrended_article_data
        except:
            # I already looked at the data, these guys will have <= 365 days worth of data
            # No need to detrend these since there's only one period or less
            detrended_data[article] = data[article]

        # means[article] = np.mean(detrended_data[article])
        # stddevs[article] = np.std(detrended_data[article])
        # Divide by full article max here
        # detrended_data[article] /= max(abs(detrended_data[article]))

        # mean center, then divide by stddev
        detrended_data[article] -= np.mean(detrended_data[article])
        detrended_data[article] /= np.std(detrended_data[article]) 

        i += 1

    print 'END: Detrending Time Series Data...'
    ### END DETREND DATA ###




    ### BEGIN PUT IN YEARLY FORMAT ###
    print "BEGIN: Putting time series data and metadata in yearly format..."

    # Instantiate the yearly format time series dataframe
    yearly_index = map(lambda x : x[5:], data.index[:365]) # Pull off MM/DD portion of year
    yearly_format_time_series_dataframe = pd.DataFrame(index=yearly_index)
    
    # Columns of the metadata
    yearly_format_metadata_column_order = [] # Used to access the metadata columns
    yearly_format_metadata_column_names = [] # Used to rename the metadata columns

    for article in article_names:
        for year in range(2008, 2015): # excluding 2015
            begin_year = str(year) + '/01/01'
            end_year = (str(year) + '/12/31') if (year not in [2008, 2012]) else (str(year) + '/12/30') # Leap years need to end at 12/30 to keep 365 days total
            article_data_for_year = detrended_data.ix[begin_year : end_year, article].values
            if sum(~np.isnan(article_data_for_year)) > 0:
                if sum(np.isnan(article_data_for_year)) > 0:
                    print "Article {0} in year {1} has {2} NaNs...".format(article, year, sum(np.isnan(article_data_for_year)))
                    continue

                if np.count_nonzero(article_data_for_year) == 0:
                    print "Article {0} in year {1} is all zeros...".format(article, year)
                    continue

                # Standardize columns to have a maximum magnitude of 1  
                # yearly_format_time_series_dataframe[article + ' ' + str(year)] = article_data_for_year / abs(article_data_for_year).max()

                # Manual shift per year
                if year == 2008: # Pad Sunday and Monday with 0's
                    article_data_for_year = np.concatenate(([0,0], article_data_for_year))
                    article_data_for_year = article_data_for_year[:-2]
                elif year == 2009: # Drop until first Sunday of year, pad later end with 0's
                    article_data_for_year = article_data_for_year[3:]
                    article_data_for_year = np.concatenate((article_data_for_year, [0,0,0]))
                elif year == 2010: # Drop until first Sunday of year, pad later end with 0's
                    article_data_for_year = article_data_for_year[2:]
                    article_data_for_year = np.concatenate((article_data_for_year, [0,0]))
                elif year == 2011: # Drop until first Sunday of year, pad later end with 0's
                    article_data_for_year = article_data_for_year[1:]
                    article_data_for_year = np.concatenate((article_data_for_year, [0]))
                elif year == 2012: # Do nothing, this year starts on Sunday
                    pass 
                elif year == 2013: # Pad Sunday and Monday with 0's
                    article_data_for_year = np.concatenate(([0,0], article_data_for_year))
                    article_data_for_year = article_data_for_year[:-2]
                elif year == 2014: # Drop until first Sunday of year, pad later end with 0's
                    article_data_for_year = article_data_for_year[4:]
                    article_data_for_year = np.concatenate((article_data_for_year, [0,0,0,0]))
                yearly_format_time_series_dataframe[article + ' ' + str(year)] = article_data_for_year # Already divided by full article max


                yearly_format_metadata_column_order.append(article)
                yearly_format_metadata_column_names.append(article + ' ' + str(year))

    sparse_yearly_format_metadata = metadata[yearly_format_metadata_column_order]
    sparse_yearly_format_metadata.columns = yearly_format_metadata_column_names

    # means_yearly_format = means[yearly_format_metadata_column_order]
    # stddevs_yearly_format = stddevs[yearly_format_metadata_column_order]
    # means_yearly_format.columns = yearly_format_metadata_column_names
    # stddevs_yearly_format.columns = yearly_format_metadata_column_names
    # means_yearly_format.to_csv('means_dataframe.csv')
    # stddevs_yearly_format.to_csv('stddevs_dataframe.csv')

    print "END: Putting time series data and metadata in yearly format..."
    ### END PUT IN YEARLY FORMAT ###




    # Check density of yearly dataframe
    print "Density of non-nans resulting dataframe: {0}".format(sum(~np.isnan(yearly_format_time_series_dataframe.values)) / float(yearly_format_time_series_dataframe.values.size))
    print "Density of resulting time series dataframe: {0}".format(sum(np.count_nonzero(yearly_format_time_series_dataframe.values)) / float(yearly_format_time_series_dataframe.values.size))
    print "Density of resulting metadata dataframe: {0}".format(sum(np.count_nonzero(sparse_yearly_format_metadata.values)) / float(sparse_yearly_format_metadata.values.size))

    # Save dataframe as csv
    yearly_format_time_series_dataframe.to_csv('time_series_yearly_format_no_nans.csv')
    # pickle.dump(sparse_yearly_format_metadata, open('preprocessed_metadata_sparse_yearly_format.pkl', 'wb'))
    sparse_yearly_format_metadata_np = scipy.sparse.coo_matrix(sparse_yearly_format_metadata)
    sparse_yearly_format_metadata_np = sparse_yearly_format_metadata_np.tocsc()
    pickle.dump(sparse_yearly_format_metadata_np, open('preprocessed_summary_metadata_sparse_yearly_format_no_nans.pkl', 'wb')) # when adding nans back in, just change file name

# To run file:
# $:~ python yearly_formatter.py <csv_file_of_raw_weekly_data> <preprocessed_metadata>
if __name__ == '__main__':
    main()