from mechanize import Browser
from bs4 import BeautifulSoup as BS
from time import sleep, time
from IPython import embed
import re
import pickle


br = Browser()
 
# Browser options
# Ignore robots.txt. Do not do this without thought and consideration.
br.set_handle_robots(False)
 
# Don't add Referer (sic) header
br.set_handle_referer(False)
 
# Don't handle Refresh redirections
br.set_handle_refresh(False)
 
#Setting the user agent as Chrome
br.addheaders = [('User-agent', 'Chrome')]

scraping_start_time = time()
wiki_page_names = []

# Quick function for encoding with ascii, leaving out the non-ascii characters
def ascii_encode(string):
    return string.encode('ascii', errors='ignore')

# Debug
def write_page(soup):
    my_html = str(soup)
    f = open('try_this.html', 'w')
    f.write(my_html)
    f.close()

# If you run into some bug, just save the pickled data
try:

    # compute time it takes
    start_time = time()

    # Open amazon.com
    br.open('https://en.wikipedia.org/wiki/User:West.andrew.g/Popular_pages')

    # Grab HTML text
    soup = BS(br.response())
 
    # Grab first product
    article_table = soup.find_all('table')[2] # Hack

    table_rows = article_table.find_all('tr')

    for row_num in range(1, 5001): # Exclude the first row which is the Title row
        row = table_rows[row_num]

        # Extract wiki page name
        page_name = ascii_encode(row.find_all('td')[1].getText())
        wiki_page_names.append(page_name)

        # go to wiki page and extract info


        print "Extracted page: {0}".format(page_name)


except Exception as e:
    print e
    pass

total_time = time() - scraping_start_time

# Print computation time
print "Total wallclock time: {0} seconds".format(round(total_time,2))

# Pickle the Python objects
pickle.dump(wiki_page_names, open('wiki_page_names.pkl', 'wb'))

# pickle.load(open('wiki_page_names.pkl', 'rb'))











