from time import sleep, time
from IPython import embed
import sys
import pickle
import pandas as pd
import wikipedia

# Quick function for encoding with ascii, leaving out the non-ascii characters
def ascii_encode(string):
    return string.encode('ascii', errors='ignore')


def scrape_wiki_metadata():

    # Load article names
    args = sys.argv[1:]
    article_names = pd.read_csv(args[0], index_col=0) # time series file
    article_names = article_names.columns.tolist()

    if 'Main Page' in article_names:
        article_names.remove('Main Page')

    # Initialize dictionary
    wiki_metadata = {}

    for index, article_name in enumerate(article_names):
        wiki_metadata[article_name] = {}

        print "Extracting metadata from page {0}: {1}".format(index, article_name)

        # Search wikipedia
        try:
            page = wikipedia.page(article_name)
        except:
            print "Couldn't find page {0}".format(article_name)
            del wiki_metadata[article_name]
            continue

        # Coordinates
        try:
            x = float(page.coordinates[0])
            y = float(page.coordinates[1])
        except:
            x = 0
            y = 0
        wiki_metadata[article_name]['coordinates'] = [x,y]

        # Categories
        try:
            wiki_metadata[article_name]['categories'] = map(ascii_encode, page.categories)
        except:
            print "Article {0} has no categories...".format(article_name)
            wiki_metadata[article_name]['categories'] = []

        # Summary
        try:
            wiki_metadata[article_name]['summary'] = ascii_encode(page.summary)
        except:
            print "Article {0} has no summary...".format(article_name)
            wiki_metadata[article_name]['summary'] = ''

        if wiki_metadata[article_name]['coordinates'] == [0, 0] and wiki_metadata[article_name]['categories'] == [] and wiki_metadata[article_name]['summary'] == '':
            del wiki_metadata[article_name]
            print "Article {0} has no metadata, dropping...".format(article_name)

    # Save this
    pickle.dump(wiki_metadata, open('wiki_metadata.pkl', 'wb'))

    # Transform it into a TF-IDF matrix


# To run file:
# $:~ python wiki_metadata_scraper.py <time_series_data>
if __name__ == '__main__':
    start_time = time()
    scrape_wiki_metadata()
    print "Time taken to perform scraping: {0} seconds".format(round(time() - start_time, 3))







