
import sys
import pickle
import urllib2
from mwviews.api import PageviewsClient
import numpy as np
import pandas as pd
from IPython import embed


def scrape_with_mwviews():
    p = PageviewsClient() # 50

    args = sys.argv[1:]
    article_names = pickle.load(open(args[0], 'rb'))
    # article_names = article_names[:10]

    # Date range
    start_date = '20150101'
    end_date = '20151231'

    # Get all hits to English wikipedia from the desktop, by a human. With daily granularity
    response = p.article_views('en.wikipedia', article_names, access='desktop', agent='user', granularity='daily',
               start=start_date, end=end_date)

    # Every column is a data point
    time_series_data = np.array([ response[key].values() for key in sorted(response.keys()) ])

    # Create pandas dataframe
    time_series_dataframe = pd.DataFrame(index=sorted(response.keys()))
    for i, page_name in enumerate(response[response.keys()[0]].keys()):
        page_name = page_name.replace('_', ' ')
        time_series_dataframe[page_name] = time_series_data[:,i]

    time_series_dataframe.to_csv('time_series_dataframe.csv')


def scrape_with_wikitrends():

    # Load article names
    args = sys.argv[1:]
    article_names = pickle.load(open(args[0], 'rb'))
    # article_names = article_names[:10] # testing

    # Base URL
    base_url = 'http://www.wikipediatrends.com/csv.php?query[]='

    # Initialize dataframe
    time_series_dataframe = pd.DataFrame()

    # Loop through article names
    for article_name in article_names:
        
        print "Scraping {0}".format(article_name)

        # Construct GET request
        url_query = base_url
        url_query +=  '+'.join(article_name.split(' '))
        response = urllib2.urlopen(url_query)

        # Loop through response
        values = {}
        for ind, line in enumerate(response):
            if ind >= 2: # Skip first two lines
                date_str = line.strip().split(', ')[0][1:-1]
                try:
                    count = int(line.strip().split(', ')[1])
                except:
                    print "Corrupted line: {0}".format(line.strip())
                    count = int(float(line.strip().split(', ')[1])) # Assuming it looks like '0.'
                values[date_str] = count

        values = pd.Series(values)

        # Add the column (pd.Series) if it has at least a year's worth of data
        if np.count_nonzero(~np.isnan(values.values)) < 365: # Can threshold with other stuff
            continue
        time_series_dataframe[article_name] = values

    time_series_dataframe.to_csv('time_series_dataframe.csv')

# To run file:
# $:~ python wiki_trends_scraper.py <article_names_file>
if __name__ == '__main__':
    # scrape_with_mwviews()
    scrape_with_wikitrends()

