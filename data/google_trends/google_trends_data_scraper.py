import os, sys, re, csv, time
import pandas as pd
import datetime
import gtrends # Can install via pip. Python package, look it up
from IPython import embed


products = ['aaa batteries', 'adidas running shoes', 'air conditioner', 'air fan', 'air purifier', 'asics running shoes',
            'bathing suit', 'beach tent', 'binders', 'birkin bag', 'blueberries', 'bookcases', 'boots', 'braun shaver', 
            'brother cartridge', 'brother printer', 'burton snowboard', 'canon camera', 'canon cartridge', 'canon printer', 
            'casio calculator', 'ceramic heater', 'christmas card', 'clothes iron', 'coach bag', 'coffee grinder', 
            'colored pencils', 'computer keyboard', 'cooler', 'cranberry', 'crayons', 'dehumidifier', 'dell computer', 
            'desk fan', 'eggnog', 'epson cartridge', 'epson printer', 'fabric steamer', 'file cabinets', 'floor fan', 
            'food processor', 'fujifilm camera', 'gillette razor', 'gloves', 'golf equipment', 
            'gucci bag', 'hamilton blender', 'hand mixer', 'heating oil', 'hermes bag', 'hp calculator', 'hp cartridge', 
            'hp computer', 'hp printer', 'humidifier', 'ironing board', 'kitchen backsplash', 'kitchen cabinet', 
            'kitchen countertop', 'kitchen faucet', 'kitchen sink', 'kitchenaid mixer', 'kodak printer', 'lenovo computer', 
            'louis vuitton bag', 'lunch bags', 'markers', 'microwave', 'new balance running shoes', 'nike running shoes', 
            'nikon camera', 'notebooks', 'oakley sunglasses', 'toaster oven', 'paint brushes', 'paper shredder', 
            'philips shaver', 'portable heater', 'pressure cooker', 'ray ban sunglasses', 'remington shaver', 'rice cooker', 
            'samsung camera', 'samsung printer', 'sandals', 'sandbags', 'school backpack', 'ski jacket', 'sleds', 'slow cooker', 
            'snow shovel', 'snowmobile', 'strawberry', 'sunscreen', 'swimwear', 'texas instruments calculator', 'toaster', 
            'vacuum cleaner', 'watermelon', 'wedding centerpiece', 'wedding flowers', 'wedding gown', 'winter coat', 'plates', 
            'cups', 'spoons', 'forks', 'knives', 'spatula', 'socks', 'shirts', 'pants', 'jackets', 'dresses', 'ipad', 'iphone', 
            'iphone case', 'tablet', 'macbook', 'samsung galaxy', 'toothbrush', 'toothpaste', 'floss', 'shampoo', 'soap', 
            'nail polish', 'tweezers', 'necklace', 'jewelry', 'bracelet', 'ring', 'flashlight', 'hammer', 'lightbulbs', 
            'wrench', 'power tools', 'roomba', 'broom', 'vacuum', 'watch', 'xbox', 'playstation', 'wii', 'nintendo', 
            'dog food', 'cat food', 'cat litter', 'fish food', 'shovel', 'lawnmower', 'weeder', 'flower seeds', 
            'sharpie markers', 'dry erase markers', 'scotch tape', 'guitar picks', 'guitar stand', 
            'piano keyboard', 'drum sticks', 'recording microphone', 'cast iron skillet', 'water filter', 'earphones', 
            'headphones', 'bluetooth speaker', 'external hard drive', 'brownies', 'cupcakes', 'apple pie', 
            'creme brulee torch', 'paper towel', 'toilet paper', 'kleenex', 'shaving cream', 'blanket', 'comforter', 
            'bed sheets', 'pillow case', 'sponge', 'dish soap', 'laundry detergent', 'dryer sheets', 'wine cooler', 'wine rack', 
            'decanter', 'thermos', 'rubber bands', 'post it notes', 'shower head', 'french press', 'cookbook', 
            'calendar', 'tote bag', 'lotion', 'hairbrush', 'fish tank']

# Scrape 10 years: from Jan 2006 to Dec 2015
startmonth = 1
startyear = 2006 # 2006
endmonth = 1 # End month excluded
endyear = 2016 # 2016

username = 
password = 

startDt = datetime.datetime(year=startyear, month=startmonth, day=1)
endDt = datetime.datetime(year=endyear, month=endmonth, day=1)

myDir = '.'


# Weekly data
if not os.path.exists(myDir + "/weekly_raw"):
        os.makedirs(myDir + "/weekly_raw")

"""
# Scrape google trends
for product in products:
    # trends = gtrends.collectTrends(username, password, [product], startDt, endDt,
    #                 granularity='w', sum=False, savePath=myDir+'/weekly_raw/'+'_'.join(product.split())+'.csv')
    trends = gtrends._downloadFullReport(username, password, product, startDt, endDt)
    gtrends._saveString(myDir+'/weekly_raw/'+'_'.join(product.split())+'.csv', trends)
    time.sleep(5) # So as not to hit the quota...
"""

### Combine data into one large time series csv ###

def extractWeeklyData(file_name):

    date_pattern = re.compile('\d\d\d\d-\d\d-\d\d\s-\s\d\d\d\d-\d\d-\d\d')

    newname = file_name[:-4] + '_extracted.csv'
    temp_file = csv.reader(open(file_name,'ru'))
    with open(newname,'wb') as write_to:
        write_data = csv.writer(write_to, delimiter=',')
        for row in temp_file:
            if len(row) == 2:
                if 'Week' == row[0]:
                    write_data.writerows([row])                    
                if re.search(date_pattern,row[0]) is not None:
                    write_data.writerows([row])


# Extract data frame information
for product in products:
    extractWeeklyData(myDir+'/weekly_raw/'+'_'.join(product.split()) + '.csv')

# Create weekly data frame
weekly_dataframe = pd.read_csv(myDir+'/weekly_raw/'+'_'.join(products[0].split()) + '_extracted.csv')
del weekly_dataframe[products[0]]
for product in products:
    print 'Aggregating ' + product
    prod_dataframe = pd.read_csv(myDir+'/weekly_raw/'+'_'.join(product.split()) + '_extracted.csv')
    weekly_dataframe[product] = prod_dataframe[product]

weekly_dataframe.to_csv(myDir + '/weekly_data.csv', sep=',')

print 'The data has been aggregated!'


# To read: pd.read_csv(file_name, index_col=0)
