import pandas as pd
from pylab import *
from time import time
import sys

# To run file:
# $:~ python plot_random_time_series.py <csv_file_time_series> <product 1> <product 2> ...
# Ex: python plot_random_time_series.py weekly_data_detrended.csv french_press soap kitchen_sink
def main():

    # Load in the data, don't forget about the index column. Either plot full data or averaged
    args = sys.argv[1:]
    data = pd.read_csv(args[0], index_col=0)

    # Grab a few random products and plot them
    products = list(data.columns)
    if 'Week' in products:
        products.remove('Week')


    # plot 5 products at a time
    for i in range(0, len(products), 5):

        figure(1)


        for j in range(5):

            subplot(5, 1, j+1)
            data_to_plot = data[products[i + j]].values
            y_vals = data_to_plot
            plot(y_vals)
            title(products[i + j])

            if i + j == len(products)-1:
                break

        show()


if __name__ == '__main__':
    main()