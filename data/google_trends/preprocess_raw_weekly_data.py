import pandas as pd
from pylab import *
from IPython import embed
import sys
from detrend_data import stl
import scipy
import scipy.optimize
import scipy.special
from scipy.stats import mode

month_ends = [31,
              28,
              31,
              30,
              31,
              30,
              31,
              31,
              30,
              31,
              30,
              31]

products_to_remove = ['ipad',
                      'iphone',
                      'iphone case',
                      'tablet',
                      'samsung galaxy']

def date_stringify(num):
    if num < 10:
        return '0' + str(num)
    else:
        return str(num)

# To run file:
# $:~ python preprocess_raw_weekly_data.py <csv_file_of_raw_weekly_data>
def main():

    ### BEGIN LOAD DATA ###

    args = sys.argv[1:]

    # Load the data in, don't forget about the index column
    full_data = pd.read_csv(args[0], index_col=0)

    ### END LOAD DATA ###



    ### BEGIN DATA IMPUTATION AND REALIGNMENT ###
    print 'BEGIN: Data Imputation and Realignment...'

    # Fill in days that are missing by imputing values
    imputed_daily_data = pd.DataFrame()
    # Construct day column
    date_list = []
    for year in range(2006, 2016):
        for month in range(1,13):
            for day in range(1, month_ends[month-1]+1):
                
                # year
                date_str = str(year) + '-'

                # month
                date_str += date_stringify(month) + '-'

                # day
                date_str += date_stringify(day)

                date_list.append(date_str)

            # DISREGARD leap year to keep strict period of 365 days
            # if year % 4 == 0 and month == 2:
            #     date_list.append(str(year) + '-02-29')

    imputed_daily_data['Date'] = date_list
    num_days = len(date_list)

    # Impute the data for each product
    products = list(full_data.columns)
    if 'Week' in products:
        # Get rid of week column
        products.remove('Week') 
    for product in products_to_remove:
        products.remove(product)

    for product in products:

        print "Imputing values for {0}".format(product)
        imputed_daily_data[product] = np.zeros( (num_days,) )

        for week in full_data['Week']:

            # Grab range of dates
            begin, throw_away, end = week.split()
            begin_day = int(begin.split('-')[-1])
            end_day = int(end.split('-')[-1])

            # Grab imputation value
            value = full_data[full_data['Week'] == week][product]
            value = value.values[0]

            # Impute the values (don't impute leap year date 2/29 to keep strict seasonality period of 365 days)
            if begin_day < end_day:
                for day in range(begin_day, end_day + 1):
                    day_str = '-'.join(begin.split('-')[:-1])
                    day_str += '-' + date_stringify(day)
                    imputed_daily_data.ix[imputed_daily_data['Date'] == day_str, product] = value
            else: # month wraps around
                begin_month = int(begin.split('-')[1])
                end_month = int(end.split('-')[1])
                for day in range(begin_day, month_ends[begin_month-1] + 1):
                    day_str = '-'.join(begin.split('-')[:-1])
                    day_str += '-' + date_stringify(day)
                    imputed_daily_data.ix[imputed_daily_data['Date'] == day_str, product] = value
                for day in range(1, end_day + 1):
                    day_str = '-'.join(end.split('-')[:-1])
                    day_str += '-' + date_stringify(day)
                    imputed_daily_data.ix[imputed_daily_data['Date'] == day_str, product] = value
                    

    print 'END: Data Imputation and Realignment...'
    ### END DATA IMPUTATION AND REALIGNMENT ###




    ### BEGIN SAMPLE WEEKLY DATA ###
    print 'BEGIN: Weekly Sampling...'
    realigned_weekly_data = pd.DataFrame()

    for product in products:
        realigned_weekly_data[product] = np.zeros((52 * 10,)) # 52 weeks in a year, 10 years
        for i in range(10): # 10 years
            base = 365 * i
            for j in range(52): # 52 weeks per year
                temp = mode(imputed_daily_data.ix[base + j * 7 : base + (j+1) * 7, product])
                if j == 51:
                    temp = mode(imputed_daily_data.ix[base + j * 7 : base + j * 7 + 8, product]) # Include last day of year
                realigned_weekly_data.ix[i * 52 + j, product] = temp.mode[0]

    print 'END: Weekly Sampling...'
    ### END SAMPLE WEEKLY DATA ###




    ### BEGIN DETREND DATA ###
    print 'BEGIN: Detrending Data...'

    # Detrended dataframe
    detrended_data = pd.DataFrame()

    for product in products:

        # Calculate detrended time series
        prod_decomp = stl(realigned_weekly_data[product], 7, np=52)
        detrended_data[product] = prod_decomp["seasonal"] + prod_decomp["remainder"] # this is the same as subtracting the trend from the whole data

    print 'END: Detrending Data...'
    ### END DETREND DATA ###




    ### BEGIN AVERAGE DATA (by minimizing Huber penalty) ###
    print 'BEGIN: Huber Average of Data...'

    # Number of chunks we want to take the Huber average over
    num_chunks = 10 # we have 10 years of data, so 1 chunk will be a year
    len_chunk = len(detrended_data) / num_chunks

    M = 10 # Huber penalty parameter
    reshaped_data = np.zeros((num_chunks, len_chunk, len(products)))
    for i in range(num_chunks):
        reshaped_data[i] = detrended_data[products][i * len_chunk : (i+1) * len_chunk].values
    huber_data = np.zeros((len_chunk, len(products)))
    for i in range(len_chunk):
        for j in range(len(products)):
            # First, define the huber function for this week's/product's data
            prod_week_data = reshaped_data[:, i, j]
            f_huber = lambda y : sum(scipy.special.huber(M, prod_week_data-y))
            huber_data[i,j] = scipy.optimize.fmin(f_huber, 0, disp=False)

    print 'END: Huber Average of Data...'
    ### END AVERAGE DATA ###


    # Put into dataframe
    huber_average_dataframe = pd.DataFrame(huber_data)
    huber_average_dataframe.columns = products

    # Save dataframe as csv
    huber_average_dataframe.to_csv('preprocessed_time_series_data.csv')


if __name__ == '__main__':
    # To run file:
    # $:~ python preprocess_raw_weekly_data.py <csv_file_of_raw_weekly_data>
    main()