import pandas as pd
from pylab import *
from time import time
import sys

# products = ['aaa batteries', 'adidas running shoes', 'air conditioner', 'air fan', 'air purifier', 'asics running shoes',
#             'bathing suit', 'beach tent', 'binders', 'birkin bag', 'blueberries', 'bookcases', 'boots', 'braun shaver', 
#             'brother cartridge', 'brother printer', 'burton snowboard', 'canon camera', 'canon cartridge', 'canon printer', 
#             'casio calculator', 'ceramic heater', 'christmas card', 'clothes iron', 'coach bag', 'coffee grinder', 
#             'colored pencils', 'computer keyboard', 'cooler', 'cranberry', 'crayons', 'dehumidifier', 'dell computer', 
#             'desk fan', 'eggnog', 'epson cartridge', 'epson printer', 'fabric steamer', 'file cabinets', 'floor fan', 
#             'food processor', 'fujifilm camera', 'gillette razor', 'gloves', 'golf equipment', 
#             'gucci bag', 'hamilton blender', 'hand mixer', 'heating oil', 'hermes bag', 'hp calculator', 'hp cartridge', 
#             'hp computer', 'hp printer', 'humidifier', 'ironing board', 'kitchen backsplash', 'kitchen cabinet', 
#             'kitchen countertop', 'kitchen faucet', 'kitchen sink', 'kitchenaid mixer', 'kodak printer', 'lenovo computer', 
#             'louis vuitton bag', 'lunch bags', 'markers', 'microwave', 'new balance running shoes', 'nike running shoes', 
#             'nikon camera', 'notebooks', 'oakley sunglasses', 'toaster oven', 'paint brushes', 'paper shredder', 
#             'philips shaver', 'portable heater', 'pressure cooker', 'ray ban sunglasses', 'remington shaver', 'rice cooker', 
#             'samsung camera', 'samsung printer', 'sandals', 'sandbags', 'school backpack', 'ski jacket', 'sleds', 'slow cooker', 
#             'snow shovel', 'snowmobile', 'strawberry', 'sunscreen', 'swimwear', 'texas instruments calculator', 'toaster', 
#             'vacuum cleaner', 'watermelon', 'wedding centerpiece', 'wedding flowers', 'wedding gown', 'winter coat', 'plates', 
#             'cups', 'spoons', 'forks', 'knives', 'spatula', 'socks', 'shirts', 'pants', 'jackets', 'dresses', 'ipad', 'iphone', 
#             'iphone case', 'tablet', 'macbook', 'samsung galaxy', 'toothbrush', 'toothpaste', 'floss', 'shampoo', 'soap', 
#             'nail polish', 'tweezers', 'necklace', 'jewelry', 'bracelet', 'ring', 'flashlight', 'hammer', 'lightbulbs', 
#             'wrench', 'power tools', 'roomba', 'broom', 'vacuum', 'watch', 'xbox', 'playstation', 'wii', 'nintendo', 
#             'dog food', 'cat food', 'cat litter', 'fish food', 'shovel', 'lawnmower', 'weeder', 'flower seeds', 
#             'sharpie markers', 'dry erase markers', 'scotch tape', 'guitar picks', 'guitar stand', 
#             'piano keyboard', 'drum sticks', 'recording microphone', 'cast iron skillet', 'water filter', 'earphones', 
#             'headphones', 'bluetooth speaker', 'external hard drive', 'brownies', 'cupcakes', 'apple pie', 
#             'creme brulee torch', 'paper towel', 'toilet paper', 'kleenex', 'shaving cream', 'blanket', 'comforter', 
#             'bed sheets', 'pillow case', 'sponge', 'dish soap', 'laundry detergent', 'dryer sheets', 'wine cooler', 'wine rack', 
#             'decanter', 'thermos', 'rubber bands', 'post it notes', 'shower head', 'french press', 'cookbook', 
#             'calendar', 'tote bag', 'lotion', 'hairbrush', 'fish tank']

# To run file:
# $:~ python plot_random_time_series.py <csv_file_time_series> <product 1> <product 2> ...
# Ex: python plot_random_time_series.py weekly_data_detrended.csv french_press soap kitchen_sink
def main():

    # Load in the data, don't forget about the index column. Either plot full data or averaged
    args = sys.argv[1:]
    data = pd.read_csv(args[0], index_col=0)

    # Number of plots
    k = 5

    wanted_products = []
    if len(args) > 1:
        wanted_products = args[1:]
        wanted_products = map(lambda x : x.replace('_', ' '), wanted_products)
        wanted_products = wanted_products[:5] # max at k

    # Grab a few random products and plot them
    products = list(data.columns)
    if 'Week' in products:
        products.remove('Week')

    # np.random.seed(0)
    np.random.seed(int(time()))
    indices = np.random.permutation(len(products))[ : max(k-len(wanted_products), 0) ]
    products_to_plot = wanted_products + [products[index] for index in indices]

    # Grab data in the form of numpy array
    data_to_plot = data[products_to_plot].values

    # plot on subplots
    figure(1)
    for i in range(k):
        subplot(k, 1, i+1)
        y_vals = data_to_plot[:,i]
        plot(y_vals)
        title(products_to_plot[i])


    show()


if __name__ == '__main__':
    main()