# -*- coding: utf-8 -*-

""" This file converts all of the raw csv files of the Google Flu Trends data 
    into one pandas dataframe (saved as a CSV file).

    The names of the columnes of this CSV are the time series that we use
    to scrape metadata from Wikipedia.
"""


from time import sleep, time
from IPython import embed
import sys, os
import pickle
import pandas as pd
import wikipedia

# listdir, but don't show hidden paths
def listdir_nohidden(path):
    dir_list = []
    for f in os.listdir(path):
        if not f.startswith('.'):
            dir_list.append(f)
    return dir_list

# Quick function for encoding with ascii, leaving out the non-ascii characters
def ascii_encode(string):
    return string.encode('ascii', errors='ignore')

def scrape_wiki_metadata():

    ######## First, join all csv files into one single PANDAS dataframe #########

    main_dataframe = pd.DataFrame()  

    for csv_file in listdir_nohidden('raw_csv_files'):
        country_name = csv_file.split('.')[0]

        # Load the csv file as a PANDAS dataframe
        temp_dataframe = pd.read_csv('raw_csv_files/{0}'.format(csv_file), index_col=0, skiprows=11)        

        # Update the column names with country names (only for cities/regions)
        new_col_names = []
        for i, col_name in enumerate(temp_dataframe.columns):
            if i == 0: # This is always just the country
                new_col_names.append(col_name)
            else:
                new_col_names.append(col_name + ', ' + country_name)
        temp_dataframe.columns = new_col_names

        # Concatenate it to the main dataframe
        main_dataframe = pd.concat([main_dataframe, temp_dataframe], axis=1)

    ### Drop the HHS columns... ###
    col_names_to_drop = []
    for col_name in main_dataframe.columns:
        if col_name.startswith('HHS'):
            col_names_to_drop.append(col_name)
    for col_name in col_names_to_drop:
        del main_dataframe[col_name]

    # Save the dataframe
    main_dataframe.to_csv('time_series_dataframe.csv')


    ######## Using the column names of this dataframe, extract wiki metadata ########

    wiki_metadata = {}
    
    # Keep track of places where we couldn't find the page
    list_of_trouble_places = ['Brittany, France', 'Aguascalientes, Mexico', 'Campeche, Mexico', 'Chihuahua, Mexico', 'Oaxaca, Mexico',
                              'Puebla, Mexico', 'Yucatán, Mexico', 'Friesland, Netherlands', 'Noord-Brabant, Netherlands', 'New York, United States',
                              'Washington, United States', 'Portland, OR, United States']
    fix_dict = {'Brittany, France' : 'Brittany',
                'Aguascalientes, Mexico' : 'Aguascalientes',
                'Campeche, Mexico' : 'Campeche',
                'Chihuahua, Mexico' : 'Chihuahua (state)',
                'Oaxaca, Mexico' : 'Oaxaca',
                'Puebla, Mexico' : 'Puebla',
                'Yucatán, Mexico' : 'Yucatán',
                'Friesland, Netherlands' : 'Frisia',
                'Noord-Brabant, Netherlands' : 'North Brabant',
                'New York, United States' : 'New York',
                'Washington, United States' : 'Washington (state)',
                'Portland, OR, United States' : 'Portland, Oregon'}


    for place in main_dataframe.columns:

        # Initialize dictionary
        wiki_metadata[place] = {}

        print "Attemping to extract metadata for: {0}".format(place)

        # Search wikipedia
        try:
            search_finds = wikipedia.search(place)
            best_find = search_finds[0]
            page = wikipedia.page(best_find)
        except:
            print "Could not find page for: {0}".format(place)
            page = wikipedia.page(fix_dict[place])
            print "Used manual fix to find right page for: {0}".format(place)

        # Coordinates
        try:
            x = float(page.coordinates[0])
            y = float(page.coordinates[1])
        except:
            print "Could not find coordinates for: {0}".format(place)
            x = 0
            y = 0
        wiki_metadata[place]['coordinates'] = [x,y]

        # Summary
        try:
            wiki_metadata[place]['summary'] = ascii_encode(page.summary)
        except:
            print "Could not find Wikipedia summary for: {0}".format(place)
            wiki_metadata[place]['summary'] = ''

        if wiki_metadata[place]['coordinates'] == [0, 0] and wiki_metadata[place]['summary'] == '':
            del wiki_metadata[place]
            print "No metadata at all for: {0}".format(place)
            

    # Save the metadata
    pickle.dump(wiki_metadata, open('wiki_metadata.pkl', 'wb'))


# To run, just call:
# $:~ python wiki_metadata_scraper.py
if __name__ == '__main__':
    start_time = time()
    scrape_wiki_metadata()
    print "Time taken to perform scraping: {0} seconds".format(round(time() - start_time, 3))