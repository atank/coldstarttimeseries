import pandas as pd
import pickle
import sys
import rpy2.robjects as robjects
import scipy
import scipy.sparse

from pylab import *
from IPython import embed
from numpy import ceil, asarray


def main():

    ### BEGIN LOAD DATA ###

    args = sys.argv[1:]

    # Load the data in, don't forget about the index column
    data = pd.read_csv(args[0], index_col=0)
    # 2008-09-21 is the first date where all data beginning from there is observed (no NaNs)
    # 
    # 2003: 52 points
    # 2004: 52 points
    # 2005: 52 points
    # 2006: 53 points
    # 2007: 52 points
    # 2008: 52 points
    # 2009: 52 points
    # 2010: 52 points
    # 2011: 52 points
    # 2012: 53 points
    # 2013: 52 points
    # 2014: 52 points
    # 2015: 32 points (only until 2015-08-09)
    #
    # Conclusion: for each time series, we'll use all the data we can. 2006 and 2012 have 52 time points. For both, drop Jan 1.
    #

    data = data.ix[1:-32] # Drop first entry (2002), and 2015 entries
    data = data.drop(['2006-01-01', '2012-01-01']) # Drop these dates to preserve periodicity

    # Load the metadata in
    metadata = pickle.load(open(args[1], 'rb'))
    region_names = metadata.columns.tolist()

    ### END LOAD DATA ###




    ### BEGIN PUT IN YEARLY FORMAT ###
    print "BEGIN: Putting time series data and metadata in yearly format..."

    # Calculate starting point of full observations of time series
    starting_index = {}
    for col in data.columns:
        for i in data.index:
            if np.count_nonzero(np.isnan(data.ix[i:][col])) == 0:
                starting_index[col] = i
                break

    # Instantiate the yearly format time series dataframe
    weekly_index = ['week ' + str(i) for i in xrange(1, 53)] # week 1, week 2, ...
    yearly_format_time_series_dataframe = pd.DataFrame(index=weekly_index)
    
    # Columns of the metadata
    yearly_format_metadata_column_order = [] # Used to access the metadata columns
    yearly_format_metadata_column_names = [] # Used to rename the metadata columns

    # means = pd.DataFrame(index=['means'], columns=region_names)
    # stddevs = pd.DataFrame(index=['stddev'], columns=region_names)

    for region in region_names:

        # means[region] = data[region].mean()
        # stddevs[region] = data[region].std()

        # Preprocess
        # Zero-mean, divide by stddev
        data[region] -= data[region].mean()
        data[region] /= data[region].std()

        # Calculate starting year 
        start_year, start_month, start_day = starting_index[region].split('-')
        if start_month == '1' and 1 <= int(start_day) <= 7:
            start_year = int(start_year)
        else:
            start_year = int(start_year) + 1

        for year in range(start_year, 2015): # excluding 2015

            data_for_year = data.ix[52 * (year-2003) : 52 * (year-2003+1) , region].values
            yearly_format_time_series_dataframe[region + ' ' + str(year)] = data_for_year
            yearly_format_metadata_column_order.append(region)
            yearly_format_metadata_column_names.append(region + ' ' + str(year))

    sparse_yearly_format_metadata = metadata[yearly_format_metadata_column_order]
    sparse_yearly_format_metadata.columns = yearly_format_metadata_column_names

    # means_yearly_format = means[yearly_format_metadata_column_order]
    # stddevs_yearly_format = stddevs[yearly_format_metadata_column_order]
    # means_yearly_format.columns = yearly_format_metadata_column_names
    # stddevs_yearly_format.columns = yearly_format_metadata_column_names
    # means_yearly_format.to_csv('means_dataframe.csv')
    # stddevs_yearly_format.to_csv('stddevs_dataframe.csv')

    # Add indicators for year here
    # TO DO:
    #   create dataframe with index '2003', '2004', ..., '2014', columns: yearly_format_metadata_column_names
    #   loop through column names and assign 1's where appropriate
    #   concatenate the dataframes

    print "END: Putting time series data and metadata in yearly format..."
    ### END PUT IN YEARLY FORMAT ###




    # Check density of yearly dataframe
    print "Density of non-nans resulting dataframe: {0}".format(sum(~np.isnan(yearly_format_time_series_dataframe.values)) / float(yearly_format_time_series_dataframe.values.size))
    print "Density of resulting metadata dataframe: {0}".format(sum(np.count_nonzero(sparse_yearly_format_metadata.values)) / float(sparse_yearly_format_metadata.values.size))

    # Save dataframe as csv
    yearly_format_time_series_dataframe.to_csv('time_series_yearly_format.csv')
    # pickle.dump(sparse_yearly_format_metadata, open('preprocessed_metadata_sparse_yearly_format.pkl', 'wb'))
    sparse_yearly_format_metadata_np = scipy.sparse.coo_matrix(sparse_yearly_format_metadata)
    sparse_yearly_format_metadata_np = sparse_yearly_format_metadata_np.tocsc()
    pickle.dump(sparse_yearly_format_metadata_np, open('preprocessed_metadata_sparse_yearly_format.pkl', 'wb')) # when adding nans back in, just change file name

    print "Done!"

# To run file:
# $:~ python yearly_formatter.py <csv_file_of_raw_weekly_data> <preprocessed_metadata>
if __name__ == '__main__':
    main()