from IPython import embed
import os
import pickle
import pandas as pd
import sys
import itertools
import re
import graphlab

def isplit(iterable,splitters):
    return [list(g) for k,g in itertools.groupby(iterable, lambda x:x in splitters) if not k]

# To run file (REQUIRES GRAPHLAB-CREATE):
# $:~ source activate gl-env 
# $:~ python generate_stemmed_tokens.py <raw_metadata_pickle_file>
def main():
    # Grab raw metadata file
    args = sys.argv[1:]
    raw_metadata = pickle.load(open(args[0], 'rb'))

    # Create data frame
    tokenized_descriptions = {}

    description_text = ''

    # Loop through articles
    articles = raw_metadata.keys()
    for article in articles:

        """
        # Remove punctuation
        raw_metadata[article]['summary'] = re.sub("[^\w\s]", "", raw_metadata[article]['summary'].lower().strip())
        """

        # Remove stopwords and get an SArray of word counts
        sent_count = graphlab.text_analytics.count_words(graphlab.SArray([raw_metadata[article]['summary']])) \
                                            .dict_trim_by_keys(graphlab.text_analytics.stopwords(), exclude=True)

        # Hack the word counts back into a string
        temp = ' '.join([(k+' ') * v for k,v in sent_count[0].items()])
        raw_metadata[article]['summary'] = ' '.join(temp.split())

        # Get the description
        description_text += raw_metadata[article]['summary']
        if description_text == '':
            continue

        description_text += '\n'
        description_text += '----------'
        description_text += '\n'

    # Get rid of last "----------"
    description_text = description_text[:-11]

    # Write it to file
    with open('temp.txt', 'w') as temp_file:
        temp_file.write(description_text)
    # Call Stanford NLP tokenizer on it
    os.system('/Library/Internet\ Plug-Ins/JavaAppletPlugin.plugin/Contents/Home/bin/java -cp "/Users/ChrisXie/Desktop/stanford-corenlp-full-2015-12-09/*" edu.stanford.nlp.pipeline.StanfordCoreNLP -annotators tokenize,ssplit,pos,lemma -file temp.txt -outputFormat "conll"')
    os.system('rm -f temp.txt')

    # output is called temp.txt.conll
    temp = pd.read_csv('temp.txt.conll', sep='\t', header=None)
    temp = list(temp[2].values)

    # Split by '----------'. isplit() returns a list of lists
    temp = isplit(temp, ['----------'])
    i = 0
    for article in articles:
        if len(raw_metadata[article]['summary']) != 0:
            if article == 'Warren G. Harding': # Nan Britton is a name, and Pandas thinks Nan is Not-A-Number... so hack here
                temp[i] = map(str, temp[i])
            tokenized_descriptions[article] = map(str.lower, temp[i])
            i += 1
        else:
            print "{0} has no summary".format(article)
            tokenized_descriptions[article] = []

    # Dump this object to a file
    pickle.dump(tokenized_descriptions, open('tokenized_descriptions.pkl', 'wb'))
    os.system('rm -f temp.txt.conll')


# To run file:
# $:~ python generate_stemmed_tokens.py <raw_metadata_pickle_file>
if __name__ == '__main__':
    main()