import pandas as pd
from pylab import *
from time import time
import sys

# To run file:
# $:~ python plot_random_time_series.py <csv_file_time_series> <product 1> <product 2> ...
# Ex: python plot_random_time_series.py weekly_data_detrended.csv french_press soap kitchen_sink
def main():

    # Load in the data, don't forget about the index column. Either plot full data or averaged
    args = sys.argv[1:]
    data = pd.read_csv(args[0], index_col=0)

    # Number of plots
    k = 5

    wanted_products = []
    if len(args) > 1:
        wanted_products = args[1:]
        wanted_products = map(lambda x : x.replace('_', ' '), wanted_products)
        wanted_products = wanted_products[:5] # max at k

    # Grab a few random products and plot them
    products = list(data.columns)

    while True:

        # np.random.seed(0)
        np.random.seed(int(time()))
        indices = np.random.permutation(len(products))[ : max(k-len(wanted_products), 0) ]
        products_to_plot = wanted_products + [products[index] for index in indices]

        # Grab data in the form of numpy array
        data_to_plot = data[products_to_plot].values

        # plot on subplots
        figure(1)
        for i in range(k):
            subplot(k, 1, i+1)
            y_vals = data_to_plot[:,i]
            plot(y_vals)
            title(products_to_plot[i].decode('utf-8'))

        print ' '.join(map(lambda x : x.replace(' ', '_'), products_to_plot))

        show()

        raw_input('Hit (Enter) to continue. Hit (Ctrl-C) to stop.')


# To run file:
# $:~ python plot_random_time_series.py <csv_file_time_series> <product 1> <product 2> ...
# Ex: python plot_random_time_series.py weekly_data_detrended.csv french_press soap kitchen_sink
if __name__ == '__main__':
    main()