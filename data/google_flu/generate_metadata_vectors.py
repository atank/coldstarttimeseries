from IPython import embed
import os
import pickle
import pandas as pd
import sys
import itertools
import scipy
import scipy.sparse
from pylab import *
from sklearn.feature_extraction.text import TfidfTransformer


def isplit(iterable,splitters):
    return [list(g) for k,g in itertools.groupby(iterable, lambda x:x in splitters) if not k]

# To run file:
# $:~ python generate_metadata_vectors.py <raw_metadata_pickle_file> <tokenized_summary_file>
def main():

    # Grab raw metadata file and tokenized summary file
    args = sys.argv[1:]
    raw_metadata = pickle.load(open(args[0], 'rb'))
    tokenized_summaries = pickle.load(open(args[1], 'rb'))

    articles = raw_metadata.keys()
    if 'Main Page' in articles:
        articles.remove('Main Page') 




    ### BEGIN COORDINATES METADATA ###
    print "Processing coordinates..."
    metadata_coordinate_dataframe = pd.DataFrame(index=['coordinate x','coordinate y'])
    for article in articles:
        metadata_coordinate_dataframe[article] = raw_metadata[article]['coordinates']
    metadata_coordinate_dataframe.ix['coordinate x'] /= 90. # latitude shrunk to [-1, 1]
    metadata_coordinate_dataframe.ix['coordinate y'] /= 180. # longitude shrunk to [-1, 1]
    print "Finished processing coordinates!"
    ### END COORDINATES METADATA ###





    ### BEGIN SUMMARIES METADATA ###
    print "Processing summaries..."

    num_total_tokens = 0
    all_tokens = set()
    for article in articles:
        all_tokens = all_tokens.union(set(tokenized_summaries[article]))
        num_total_tokens += len(tokenized_summaries[article])
    print "Number of total (non-unique) tokens: {0}".format(num_total_tokens)
    print "Number of unique tokens: {0}".format(len(all_tokens))

    # Now create the vector of summaries
    summary_counts = np.zeros((len(articles), len(all_tokens))) # Each row is a article
    for idx, article in enumerate(articles):
        tf = dict.fromkeys(all_tokens, 0) # term frequencies
        for token in tokenized_summaries[article]:
            tf[token] += 1
        summary_counts[idx, :] = tf.values()

    # Remove any token where it appears only in one article (i.e. any column where only one value is nonzero)
    indices_to_delete = []
    for column in xrange(summary_counts.shape[1]):
        if np.count_nonzero(summary_counts[:, column]) == 1:
            indices_to_delete.append(column)
    summary_counts = np.delete(summary_counts, indices_to_delete, axis=1)
    print "After dropping tokens unique to one article, the number of unique tokens are {0}".format(summary_counts.shape[1])

    # Names of summaries metadata feature vector
    metadata_summary_names = np.delete(np.array(tf.keys()), indices_to_delete).tolist()

    # Compute tf-idf matrix using sklearn
    transformer = TfidfTransformer()
    tfidf = transformer.fit_transform(summary_counts)

    # Create dataframe
    metadata_summary_dataframe = pd.DataFrame(index=metadata_summary_names)
    for idx, article in enumerate(articles):
        metadata_summary_dataframe[article] = tfidf[idx, :].toarray().T # Each row is a article. Transpose it to a column vector

    print "Finished processing summaries!"

    ### END SUMMARIES METADATA ###




    # Concatenate dataframes
    metadata_dataframe = pd.concat([metadata_coordinate_dataframe, metadata_summary_dataframe])
    # metadata_summary_dataframe = pd.concat([metadata_coordinate_dataframe, metadata_summary_dataframe])

    # For summary dataframe, drop anything that has no summary
    drop_list = []
    for article in metadata_summary_dataframe.columns: # Find the articles that have no summary
        if metadata_summary_dataframe[article].values.nonzero()[0].size == 0:
            drop_list.append(article)
    for article in drop_list: # Drop them from this dataframe
        metadata_summary_dataframe = metadata_summary_dataframe.drop(article, axis=1)
        print "Dropped {0} because it has no summary...".format(article)

    # Sparsify everything before saving
    # metadata_category_dataframe = metadata_category_dataframe.to_sparse(fill_value=0)
    metadata_summary_dataframe = metadata_summary_dataframe.to_sparse(fill_value=0)
    metadata_dataframe = metadata_dataframe.to_sparse(fill_value=0)

    # Files are too large. Need to save sparse versions of them via pickling.
    pickle.dump(metadata_summary_dataframe, open('preprocessed_summary_metadata_sparse.pkl', 'wb'))
    pickle.dump(metadata_dataframe, open('preprocessed_metadata_sparse.pkl', 'wb'))

    print 'Finished!'

# To run file:
# $:~ python generate_metadata_vectors.py <raw_metadata_pickle_file> <tokenized_summary_file>
if __name__ == '__main__':
    main()