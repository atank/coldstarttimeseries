from mechanize import Browser
from bs4 import BeautifulSoup as BS
from time import sleep, time
import os

if not os.path.exists('raw_csv_files'):
    os.makedirs('raw_csv_files')

br = Browser()

# Browser options
# Ignore robots.txt. Do not do this without thought and consideration.
br.set_handle_robots(False)
 
# Don't add Referer (sic) header
br.set_handle_referer(False)
 
# Don't handle Refresh redirections
br.set_handle_refresh(False)
 
#Setting the user agent as Chrome
br.addheaders = [('User-agent', 'Chrome')]

scraping_start_time = time()

# Quick function for encoding with ascii, leaving out the non-ascii characters
def ascii_encode(string):
    return string.encode('ascii', errors='ignore')

# Debug
def write_page(soup):
    my_html = str(soup)
    f = open('try_this.html', 'w')
    f.write(my_html)
    f.close()


# Base URL
base_url = 'https://www.google.org/flutrends/about/'

# If you run into some bug, just save the pickled data
try:

    # compute time it takes
    start_time = time()

    # Open amazon.com
    br.open(base_url)

    # Grab HTML text
    soup = BS(br.response())
 
    # Grab first unordered list
    ul_of_interest = soup.find_all('ul')[0]

    # Loop over data files and download
    for country in ul_of_interest.children:
        if country == '\n':
            continue
        if country.get_text() == 'World': # Not interested in this data file
            continue

        # At this point, "country" looks like: <li><a href="data/flu/au/data.txt">Australia</a></li>
        # Now get the href and country name
        extension_for_data_url = country.next_element.get('href')
        country_name = ascii_encode(country.get_text())

        # Open the url and save the data
        data_url = base_url + extension_for_data_url
        br.open(data_url)
        data_soup = BS(br.response())
        csv_text = data_soup.find_all('p')[0].get_text()

        f = open('raw_csv_files/{0}.csv'.format(country_name), 'w')
        f.write(csv_text.encode('utf-8'))
        f.close()

        print "Downloaded csv file for: ", ascii_encode(country_name)    


except Exception as e:
    print e
    pass

total_time = time() - scraping_start_time

# Print computation time
print "Total wallclock time: {0} seconds".format(round(total_time,2))


# pickle.load(open('wiki_page_names.pkl', 'rb'))











