from IPython import embed
import os
import pickle
import pandas as pd
import sys
import itertools
from pylab import *
from sklearn.feature_extraction.text import TfidfTransformer

products_to_remove = ['ipad',
                      'iphone',
                      'iphone case',
                      'tablet',
                      'samsung galaxy']

def isplit(iterable,splitters):
    return [list(g) for k,g in itertools.groupby(iterable, lambda x:x in splitters) if not k]

# To run file:
# $:~ python generate_metadata_vectors.py <raw_metadata_pickle_file> <tokenized_description_file>
def main():

    # Grab raw metadata file and tokenized description file
    args = sys.argv[1:]
    raw_metadata = pickle.load(open(args[0], 'rb'))
    tokenized_descriptions = pickle.load(open(args[1], 'rb'))

    products = raw_metadata.keys()
    if 'Week' in products:
        # Get rid of week column
        products.remove('Week') 
    for product in products_to_remove:
        products.remove(product)

    ### BEGIN CATEGORIES METADATA ###

    # Get all categories
    categories = set()
    for product in products:
        categories = categories.union(raw_metadata[product]['categories'])

    # Now create the vector of categories
    category_metadata_vectors = {}
    for product in products:
        temp = dict.fromkeys(categories, 0)
        for category in raw_metadata[product]['categories']:
            temp[category] += 1
        category_metadata_vectors[product] = temp.values()

    # Names of category metadata feature vector
    metadata_category_names = temp.keys()

    # Create dataframe
    metadata_category_dataframe = pd.DataFrame(index=metadata_category_names)
    for product in products:
        metadata_category_dataframe[product] = category_metadata_vectors[product]

    ### END CATEGORIES METADATA ###




    ### BEGIN DESCRIPTIONS METADATA ###
    all_tokens = set()
    for product in products:
        all_tokens = all_tokens.union(set(tokenized_descriptions[product]))

    # Now create the vector of descriptions
    description_counts = np.zeros((len(products), len(all_tokens))) # Each row is a product
    for idx, product in enumerate(products):
        tf = dict.fromkeys(all_tokens, 0) # term frequencies
        for token in tokenized_descriptions[product]:
            tf[token] += 1
        description_counts[idx, :] = tf.values()

    # Names of descriptions metadata feature vector
    metadata_description_names = tf.keys()

    # Compute tf-idf matrix using sklearn
    transformer = TfidfTransformer()
    tfidf = transformer.fit_transform(description_counts)

    # Create dataframe
    metadata_description_dataframe = pd.DataFrame(index=metadata_description_names)
    for idx, product in enumerate(products):
        metadata_description_dataframe[product] = tfidf[idx, :].toarray().T # Each row is a product. Transpose it to a column vector

    ### END DESCRIPTIONS METADATA ###




    # Concatenate dataframes
    metadata_dataframe = pd.concat([metadata_category_dataframe, metadata_description_dataframe])

    # Write out to dataframe
    metadata_category_dataframe.to_csv('preprocessed_category_metadata.csv')
    metadata_description_dataframe.to_csv('preprocessed_description_metadata.csv')
    metadata_dataframe.to_csv('preprocessed_metadata.csv')

    print 'Finished!'

if __name__ == '__main__':
    # To run file:
    # $:~ python generate_metadata_vectors.py <raw_metadata_pickle_file> <tokenized_description_file>
    main()