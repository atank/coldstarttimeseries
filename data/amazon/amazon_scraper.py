from mechanize import Browser
from bs4 import BeautifulSoup as BS
from time import sleep, time
from IPython import embed
import re
import pickle

products = ['aaa batteries', 'adidas running shoes', 'air conditioner', 'air fan', 'air purifier', 'asics running shoes',
            'bathing suit', 'beach tent', 'binders', 'birkin bag', 'blueberries', 'bookcases', 'boots', 'braun shaver', 
            'brother cartridge', 'brother printer', 'burton snowboard', 'canon camera', 'canon cartridge', 'canon printer', 
            'casio calculator', 'ceramic heater', 'christmas card', 'clothes iron', 'coach bag', 'coffee grinder', 
            'colored pencils', 'computer keyboard', 'cooler', 'cranberry', 'crayons', 'dehumidifier', 'dell computer', 
            'desk fan', 'eggnog', 'epson cartridge', 'epson printer', 'fabric steamer', 'file cabinets', 'floor fan', 
            'food processor', 'fujifilm camera', 'gillette razor', 'gloves', 'golf equipment', 
            'gucci bag', 'hamilton blender', 'hand mixer', 'heating oil', 'hermes bag', 'hp calculator', 'hp cartridge', 
            'hp computer', 'hp printer', 'humidifier', 'ironing board', 'kitchen backsplash', 'kitchen cabinet', 
            'kitchen countertop', 'kitchen faucet', 'kitchen sink', 'kitchenaid mixer', 'kodak printer', 'lenovo computer', 
            'louis vuitton bag', 'lunch bags', 'markers', 'microwave', 'new balance running shoes', 'nike running shoes', 
            'nikon camera', 'notebooks', 'oakley sunglasses', 'toaster oven', 'paint brushes', 'paper shredder', 
            'philips shaver', 'portable heater', 'pressure cooker', 'ray ban sunglasses', 'remington shaver', 'rice cooker', 
            'samsung camera', 'samsung printer', 'sandals', 'sandbags', 'school backpack', 'ski jacket', 'sleds', 'slow cooker', 
            'snow shovel', 'snowmobile', 'strawberry', 'sunscreen', 'swimwear', 'texas instruments calculator', 'toaster', 
            'vacuum cleaner', 'watermelon', 'wedding centerpiece', 'wedding flowers', 'wedding gown', 'winter coat', 'plates', 
            'cups', 'spoons', 'forks', 'knives', 'spatula', 'socks', 'shirts', 'pants', 'jackets', 'dresses', 'ipad', 'iphone', 
            'iphone case', 'tablet', 'macbook', 'samsung galaxy', 'toothbrush', 'toothpaste', 'floss', 'shampoo', 'soap', 
            'nail polish', 'tweezers', 'necklace', 'jewelry', 'bracelet', 'ring', 'flashlight', 'hammer', 'lightbulbs', 
            'wrench', 'power tools', 'roomba', 'broom', 'vacuum', 'watch', 'xbox', 'playstation', 'wii', 'nintendo', 
            'dog food', 'cat food', 'cat litter', 'fish food', 'shovel', 'lawnmower', 'weeder', 'flower seeds', 
            'sharpie markers', 'dry erase markers', 'scotch tape', 'guitar picks', 'guitar stand', 
            'piano keyboard', 'drum sticks', 'recording microphone', 'cast iron skillet', 'water filter', 'earphones', 
            'headphones', 'bluetooth speaker', 'external hard drive', 'brownies', 'cupcakes', 'apple pie', 
            'creme brulee torch', 'paper towel', 'toilet paper', 'kleenex', 'shaving cream', 'blanket', 'comforter', 
            'bed sheets', 'pillow case', 'sponge', 'dish soap', 'laundry detergent', 'dryer sheets', 'wine cooler', 'wine rack', 
            'decanter', 'thermos', 'rubber bands', 'post it notes', 'shower head', 'french press', 'cookbook', 
            'calendar', 'tote bag', 'lotion', 'hairbrush', 'fish tank']

br = Browser()
 
# Browser options
# Ignore robots.txt. Do not do this without thought and consideration.
br.set_handle_robots(False)
 
# Don't add Referer (sic) header
br.set_handle_referer(False)
 
# Don't handle Refresh redirections
br.set_handle_refresh(False)
 
#Setting the user agent as Chrome
br.addheaders = [('User-agent', 'Chrome')]
 
# Dictionary of raw product metadata
raw_metadata = {}

scraping_start_time = time()
compute_times = []

# Quick function for encoding with ascii, leaving out the non-ascii characters
def ascii_enconde(string):
    return string.encode('ascii', errors='ignore')

# Debug
def write_page(soup):
    my_html = str(soup)
    f = open('try_this.html', 'w')
    f.write(my_html)
    f.close()

# If you run into some bug, just save the pickled data
try:

    for product_index, product in enumerate(products):

        print "Scraping metadata for product {0}: {1}...".format(product_index+1, product)

        # compute time it takes for this product
        product_start_time = time()

        # Add this product to the metadata dictionary
        raw_metadata[product] = {'descriptions': [], 'categories': set()}

        # Open amazon.com
        br.open('http://www.amazon.com/')

        # Search product
        br.select_form(name="site-search")
        br['field-keywords'] = product
        br.submit()

        # Grab HTML text
        soup = BS(br.response())
     
        # Grab first product
        link = soup.find_all('a', class_='a-link-normal s-access-detail-page  a-text-normal')[0]['href']

        # Go to the page
        br.open(link)
        soup = BS(br.response())

        # Get feature bullet points (like a product description)
        feature_bullet_div = soup.find_all(id="feature-bullets")
        if len(feature_bullet_div) >= 1:
            feature_bullet_div = feature_bullet_div[0]
            list_items = feature_bullet_div.find_all('li')
            for item in list_items:
                if len(item.find_all('span')) == 1: # typically only one span tag
                    raw_metadata[product]['descriptions'].append(ascii_enconde(item.text).strip())

        # Get best seller rankings
        candidates = soup.find_all('a', href=re.compile(".*/bestsellers.*/"))
        for candidate in candidates:
            if 'best' not in candidate.text.lower() and 'see top' not in candidate.text.lower(): # It's not a recommend best seller
                # Put the text (typically this should be a category)
                raw_metadata[product]['categories'].add(ascii_enconde(candidate.text.lower()))
            
                # Grab the thing next to /bestseller/ in the href
                index = candidate['href'].find('bestsellers/')
                category = candidate['href'][index + len('bestsellers/'):]
                # Search for a '/' on the right side
                if category.find('/') >= 0:
                    index = category.find('/')
                    category = category[:index]
                raw_metadata[product]['categories'].add(ascii_enconde(category))

        compute_times.append(time() - product_start_time)

        # Sleep as to not seem like a bot
        sleep(2)

    # Print the metadata at the end
    print '\n'
    for product in products:
        print "Metadata for {0}:\n".format(product.upper())
        print "DESCRIPTIONS:"
        print "\n".join(raw_metadata[product]['descriptions'])
        print "CATEGORIES:"
        print "\n".join(raw_metadata[product]['categories'])
        print "\n\n"

except Exception as e:
    print e
    pass

total_time = time() - scraping_start_time

# Print computation time
print "Time it took on average for each product: {0} seconds".format(round(sum(compute_times)/len(compute_times), 2))
print "Total wallclock time: {0} seconds".format(round(total_time,2))

# Pickle the Python objects
pickle.dump(raw_metadata, open('raw_metadata.pkl', 'wb'))

# pickle.load(open('raw_metadata.pkl', 'rb'))











