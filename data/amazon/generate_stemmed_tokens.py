from IPython import embed
import os
import pickle
import pandas as pd
import sys
import itertools

def isplit(iterable,splitters):
    return [list(g) for k,g in itertools.groupby(iterable, lambda x:x in splitters) if not k]

# To run file:
# $:~ python generate_stemmed_tokens.py <raw_metadata_pickle_file>
def main():

    # Grab raw metadata file
    args = sys.argv[1:]
    raw_metadata = pickle.load(open(args[0], 'rb'))

    # Create data frame
    tokenized_descriptions = {}

    description_text = ''

    # Loop through products
    products = raw_metadata.keys()
    for product in products:

        # Get the description
        description_text += ' '.join(raw_metadata[product]['descriptions'])
        if description_text == '':
            continue

        description_text += '\n'
        description_text += '----------'
        description_text += '\n'

    # Get rid of last "----------"
    description_text = description_text[:-11]

    # Write it to file
    with open('temp.txt', 'w') as temp_file:
        temp_file.write(description_text)
    # Call Stanford NLP tokenizer on it
    os.system('/Library/Internet\ Plug-Ins/JavaAppletPlugin.plugin/Contents/Home/bin/java -cp "/Users/ChrisXie/Desktop/stanford-corenlp-full-2015-12-09/*" edu.stanford.nlp.pipeline.StanfordCoreNLP -annotators tokenize,ssplit,pos,lemma -file temp.txt -outputFormat "conll"')
    os.system('rm -f temp.txt')

    # output is called temp.txt.conll
    temp = pd.read_csv('temp.txt.conll', sep='\t', header=None)
    temp = list(temp[2].values)

    # Split by '----------'. isplit() returns a list of lists
    temp = isplit(temp, ['----------'])
    i = 0
    for product in products:
        if len(raw_metadata[product]['descriptions']) != 0:
            tokenized_descriptions[product] = map(str.lower, temp[i])
            i += 1
        else:
            tokenized_descriptions[product] = []

    # Dump this object to a file
    pickle.dump(tokenized_descriptions, open('tokenized_descriptions.pkl', 'wb'))

if __name__ == '__main__':
    # To run file:
    # $:~ python generate_stemmed_tokens.py <raw_metadata_pickle_file>
    main()