import sys
import pandas as pd
import sklearn
from sklearn.decomposition import PCA

# To run file:
# $:~ python generate_pca_vectors.py <preprocessed_metadata_file>
def main():

    args = sys.argv[1:]

    # Full metadata
    metadata = pd.read_csv(args[0], index_col=0)

    # Each column is a vector according to a product, we want each row, so transpose it
    m = metadata.values.T

    # PCA the data
    pca_data = PCA().fit_transform(m)

    # Turn into a PANDAS dataframe
    pca_dataframe = pd.DataFrame(pca_data.T) # Each column is a product
    pca_dataframe.columns = metadata.columns

    pca_dataframe.to_csv('preprocessed_PCA_metadata.csv')



# To run file:
# $:~ python generate_pca_vectors.py <preprocessed_metadata_file>
if __name__ == '__main__':
    main()